#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <sys/time.h>
#include <mkl.h>

void print_matrix(char *desc, double *a, int m, int n);
void init(double *u, int n, double u0);
double calculate_eps(double *u, double *g, int n);

void swap_vec(double **a, int m);
void print_vec(char *desc, double *a, int m);
void free_vec(double **a, int m);

void init_a(double *a, int n, int m, double **u, double** g);
void init_b(double *b, int n, double** u, double** g);

double find_rank(double *mat_a, double *mat_b, int n,
		 int m, double *x, double& cond, double cond_param);

double calculate_next(double **u, double **g, int n,
		      int mmax, int iter, double& cond,
                        double cond_param, int& mcur);

double *anderson_acceleration(int n, int max_iter, int mmax, double eps, 
                                 double cond_param, double *init_vals, 
                                 void (calculate_g(double *, double *, int)), 
                                 double *final_residual_norm, double *runtime);
