
      subroutine dinit
c--------------------------------------------------------
c     initialize
c     1)Steps of v_x,v_y uniform mesh:
c       step_x,step_y
c     2)Creates arrays v_x,v_y
c     3)Sst rn_2d_b=0, rn_2d_c=0, rn_2d_t=0, rn_2d_anal=0
c-----------------------------------------------------------
      use One_NmlMod
      use One_NoNmlMod
      implicit none
      
      integer i,j

      
      step_x=v_x_max/Nx
      step_y=v_y_max/Ny

      do i=0,Nx
         v_x(i)=step_x*i
      enddo
      do j=0,Ny
         v_y(j)=step_y*j
      enddo

      do i=0,Nx
        do j=0,Ny
           rn_2d_b(i,j)=0.d0
           rn_2d_c(i,j)=0.d0
           rn_2d_t(i,j)=0.d0
           rn_2d_analyt(i,j)=0.d0
        enddo
      enddo

      return
      end

      subroutine ainalloc_arrays_One_NoNmlMod
c-----allocate pointers in One_NoNmlMod
      use One_NmlMod
      use One_NoNmlMod
      implicit none  

      real*8  zero
      integer istat,n
      real*4  zero_r4

      zero=0.d0 
      zero_r4=0.e0
 
      write(*,*)'dinit.f in  ainalloc_arrays_One_NoNmlMod'
   
      allocate( v_x(0:Nx),STAT=istat)
      call bcast(v_x,zero,SIZE(v_x))
   
      allocate( v_y(0:Ny),STAT=istat)
      call bcast(v_y,zero,SIZE(v_y))

      allocate( rn_2d_t(0:Nx,0:Ny),STAT=istat)
      call bcast( rn_2d_t,zero,SIZE( rn_2d_t))

      allocate( rn_2d_b(0:Nx,0:Ny),STAT=istat)
      call bcast( rn_2d_b,zero,SIZE( rn_2d_b))

      allocate( rn_2d_c(0:Nx,0:Ny),STAT=istat)
      call bcast( rn_2d_c,zero,SIZE( rn_2d_c))

      allocate( rn_2d_wx(0:Nx,0:Ny),STAT=istat)
      call bcast( rn_2d_t,zero,SIZE( rn_2d_wx))

      allocate( rn_2d_analyt(0:Nx,0:Ny),STAT=istat)
      call bcast( rn_2d_analyt,zero,SIZE(rn_2d_analyt))

      write(*,*)'befiore allocate vol_plot_ar n_vol_plot',n_vol_plot

      allocate(vol_plot_ar(0:n_vol_plot),STAT=istat)
      call bcast( vol_plot_ar,zero,SIZE( vol_plot_ar))

      allocate(particle_numb_ar(1:n_vol_plot),STAT=istat)
      call bcast( particle_numb_ar,zero,SIZE( particle_numb_ar))

      allocate(component_mass_ar(1:n_vol_plot,1:2),STAT=istat)
      call bcast( component_mass_ar,zero,SIZE( component_mass_ar))
c--------------------------------------------------------------------
c     for Monte-Carlo
c------------------------------------------------------------------
      allocate( fn_2d_mesh(1:Nx,1:Ny),STAT=istat)
      allocate( fn_2d_analyt(1:Nx,1:Ny),STAT=istat)
      allocate( fq_2d_mesh(1:Nx,1:Ny),STAT=istat)
      allocate( fq_2d_analyt(1:Nx,1:Ny),STAT=istat)

      allocate( y_ar(1:N_species,1:N_particle),STAT=istat)

      allocate( v_c_x(1:Nx),STAT=istat)
      allocate( v_c_y(1:Ny),STAT=istat)

      allocate( v_particle_min_vector(1:N_species),STAT=istat)
      allocate( v_particle_max_vector(1:N_species),STAT=istat)
      allocate( v0_vector(1:N_species),STAT=istat)

c-----------------------------------------------------------------------
c     to plot difference between numerical and analytical solutions
c----------------------------------------------------------------------
      allocate(difnorm_ar(1:n_timesteps),STAT=istat)
      call bcast( difnorm_ar,zero,SIZE( difnorm_ar))

      allocate(difc_ar(1:n_timesteps),STAT=istat)
      call bcast( difc_ar,zero,SIZE( difc_ar))

      allocate(difrc_ar(1:n_timesteps),STAT=istat)
      call bcast( difrc_ar,zero,SIZE( difrc_ar))
c------------------------------------------------------------------
c      allocate(dk_coag_ar(1:N_particle,1:N_particle),STAT=istat)
c      call bcast( dk_coag_ar,zero,SIZE( dk_coag_ar))
     
c----------------------------------------------------------------
      n_plots = t_max/h_plot
      write(*,*)'t_max,h_plot,n_plots',t_max,h_plot,n_plots

      allocate(density_time(0: n_plots,1:4),STAT=istat)
      call bcast( density_time,zero,SIZE( density_time))

     
      write(*,*)'end allocate all pointers'

      return
      end

