#include <cstring>
#include <cmath>
#include "gsmol_kernel.h"

//============================================================================//
//    Kernel initialization                                                   //
//============================================================================//
TKernel::TKernel(
    // in: dimensionality
    const int & dim,
    // in: sizes of modes
    int * & modes,
    // in: kernel name
    const char * kname
):
    TTensor(dim, modes),
    powers(NULL),
    kvalue_func_ptr(&TKernel::kconst)
{
    //===================================//
    //    Sum of squared roots kernel    //
    //===================================//
    if (!strcmp(kname, "sumsqrt"))
    {
        kvalue_func_ptr = &TKernel::ksumsqrt;
    } else
    //==================================//
    //    Generalized product kernel    //
    //==================================//
    if (!strcmp(kname, "genprod"))
    {
        powers = (double *)malloc(dimensionality * sizeof(double));

#pragma omp parallel for
        for (int d = 0; d < dimensionality; ++d)
        {
            powers[d] = 0.35 - 0.1 * (double)d;
        }

        kvalue_func_ptr = &TKernel::kgenprod;
    } else
    //==============================//
    //    Generalized sum kernel    //
    //==============================//
    if (!strcmp(kname, "gensum"))
    {
        powers = (double *)malloc(sizeof(double));

        powers[0] = 0.5;

        kvalue_func_ptr = &TKernel::kgensum;
    } else
    //====================//
    //    IBRAE kernel    //
    //====================//
    if (!strcmp(kname, "ibrae"))
    {
        kvalue_func_ptr = &TKernel::kibrae;
    }
}

//============================================================================//
//    Kernel deallocation                                                     //
//============================================================================//
TKernel::~TKernel(
) {
    if (powers)
        free(powers);
}

//============================================================================//
//    Constant kernel                                                         //
//============================================================================//
// return: const(point)
double TKernel::kconst(
    // in:
    const int * point
) {
    return 1.;
}

//============================================================================//
//    Sum of sqruared roots kernel                                            //
//============================================================================//
// return: sumsqrt(point)
double TKernel::ksumsqrt(
    // in:
    const int * point
) {
    double value = 0.;

//#pragma omp parallel for reduction(+value)
    for (int d = 0; d < dimensionality; ++d)
    {
        value += sqrt((double)(point[d] + 1));
    }

    return value;
}

//============================================================================//
//    Next permutation by one transposition algorithm                         //
//============================================================================//
// return: 0, if perm is the last permutation 
// return: 1, otherwise 
int nextperm(
    // in: size of permutation
    const int & d,
    // alt: permutation to modify
    int * perm
) {
    int i;
    int k;
    int tmp;

    // Find the largest index k such that perm[k] < perm[k + 1].
    for (k = d - 2; k >= 0; --k)
        if (perm[k] < perm[k + 1])
            break;

    // If no such index exists, the permutation
    // is the last permutation.
    if (k < 0)
        return 0;

    // Find the largest index i greater than k such
    // that perm[k] < perm[i].
    for (i = d - 1; i > k; --i)
        if (perm[k] < perm[i])
            break;

    // Swap the value of perm[k] with that of perm[i].
    tmp = perm[k];
    perm[k] = perm[i];
    perm[i] = tmp;
    
    // Reverse the sequence from perm[k + 1] up to
    // and including the final element perm[d - 1].
    for (i = k + 1, k = d - 1; i < k; ++i, --k)
    {
        tmp = perm[k];
        perm[k] = perm[i];
        perm[i] = tmp;
    }
    
    return 1;
}

//============================================================================//
//    Generalized product kernel                                              //
//============================================================================//
// return: genprod(point)
double TKernel::kgenprod(
    // in:
    const int * point
) {
    double value = 0.;
    double tmp = 1.;
    int perm[dimensionality];

#pragma omp parallel for
    for (int d = 0; d < dimensionality; ++d)
        perm[d] = d;

    while (tmp > 0)
    {
#pragma omp parallel for reduction(*:tmp)
        for (int d = 0; d < dimensionality; ++d)
        {
            tmp *= pow((double)(point[d] + 1), powers[perm[d]]);
        }

        value += tmp;
        tmp = (double)nextperm(dimensionality, perm);
    }

    return value;
}

//============================================================================//
//    Generalized sum kernel                                                  //
//============================================================================//
// return: gensum(point)
double TKernel::kgensum(
    // in:
    const int * point
) {
    double tmp = 0.;

#pragma omp parallel for reduction(+:tmp)
    for (int d = 0; d < dimensionality; ++d)
    {
        tmp += point[d] + 1;
    }

    return pow((double)tmp, powers[0]);
}

//============================================================================//
//    IBRAE kernel                                                            //
//============================================================================//
// return: basic symmetric component of IBRAE kernel in 3d
double ibrae_3dsmnd(
    // in:
    const double & i1,
    // in:
    const double & i2
) {
    double res = pow(pow(i1, 1. / 3.) + pow(i2, 1. / 3.), 3.);
    res *= sqrt(i1 * i2 / (i1 + i2));

    return res;
}

// return: ibrae(point)
double TKernel::kibrae(
    // in:
    const int * point
) {
    int d = dimensionality;
    double p = 0.;
    double q = 0.;
    double s = 0.;

    if (d == 2)
    {
//#pragma omp parallel for reduction(+:p, q)
        for (int a = 0; a < d; ++a)
        {
            p += pow((double)(point[a] + 1), 1. / 3.);
            q += 1. / (double)(point[a] + 1);
        }

        return p * p * sqrt(q) / (sqrt(2.) * 4.);
    }
    else if (d == 3)
    {
//#pragma omp parallel for reduction(+:s, p, q)
        for (int a = 0; a < d; ++a)
        {
            s += ibrae_3dsmnd(
                (double)(point[a] + 1),
                (double)(point[(a + 1 > 2)? a - 2: a + 1] + 1)
            );
            p += pow((double)(point[a] + 1), 1. / 3.);
            q += (double)(point[a] + 1);
        }

        for (int a = 0; a < d; ++a)
        {
            q /= (double)(point[a] + 1);
        }

        return s * p * p * sqrt(q) / (sqrt(1.5) * 216.);
    }

    return 1.;
}

//============================================================================//
//    Factorial                                                               //
//============================================================================//
// return: n!
int fact(
    // in:
    const int & n
) {
    if (n == 1)
        return 1;
    else
        return n * fact(n - 1);
}
