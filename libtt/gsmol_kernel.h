#ifndef GSMOL_KERNEL_H
#define GSMOL_KERNEL_H

#include <mkl/mkl.h>
#include <mkl/mkl_lapacke.h>
#include <sys/time.h>
#include "tensor.h"

//============================================================================//
//    Class for access to kernel items                                        //
//============================================================================//
class TKernel : public TTensor
{

    private:

        // kernel parameters (see implementations of kernels)
        double * powers;

        // pointer to specific function returning kernel item
        double (TKernel::*kvalue_func_ptr)(
            const int * point
        );

        double kconst(
            const int * point
        );

        double ksumsqrt(
            const int * point
        );

        double kgenprod(
            const int * point
        );

        double kgensum(
            const int * point
        );

        double kibrae(
            const int * point
        );

    public:

        TKernel():
            TTensor(),
            powers(NULL),
            kvalue_func_ptr(NULL)
        {}

        TKernel(
            // in: dimensionality
            const int & d,
            // in: sizes of modes
            int * & modes,
            // in: kernel name
            const char * kname
        );

        virtual ~TKernel();

        // return: Kernel(point)
        double operator[](
            // in: 
            int * point
        ) {
            return (this->*kvalue_func_ptr)(point);
        }

};

//============================================================================//
//    Factorial                                                               //
//============================================================================//
// return: n!
int fact(
    // in:
    const int & n
);

#endif // GSMOL_KERNEL_H

