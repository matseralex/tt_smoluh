#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include "../../libtt/tensor_train.h"
class visualisation_gnuplot
{
public:
    static void gnuplot_1d(int S, int D, double dr, int periods, double dt)  // 2d graph
    {
        FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
        fprintf(gnuplotPipe, "reset\n");
        fprintf(gnuplotPipe, "set terminal gif animate\n");
        fprintf(gnuplotPipe, "set output 'result.gif'\n");
        fprintf(gnuplotPipe, "stats 'data.txt' nooutput\n");
        fprintf(gnuplotPipe, "set xrange [0:%f]\n", dr*(S-D-1));
        fprintf(gnuplotPipe, "set yrange [0:1.0]\n");
        fprintf(gnuplotPipe, "set xlabel 'x'\n");
        fprintf(gnuplotPipe, "set ylabel 'f(x,v,t)'\n");
        fprintf(gnuplotPipe, "set title 'Solution graph'\n");
        fprintf(gnuplotPipe, "set key at 4.95, 0.95\n");
        fprintf(gnuplotPipe, "do for [i=1:int(STATS_blocks)] {\n");
        fprintf(gnuplotPipe, "plot 'data.txt' using 1:2 index(i-1) with lines lc 'red' title sprintf(\"t=%%0.2f v=0.2\", %f*i), ", periods*dt);
        fprintf(gnuplotPipe, "'data.txt' using 1:3 index(i-1) with lines lc 'green' title 'v=2.5',");
        fprintf(gnuplotPipe, "'data.txt' using 1:4 index(i-1) with lines lc 'blue' title 'v=3.5',");
        fprintf(gnuplotPipe, "}\n");
        pclose(gnuplotPipe);
    }
    static void gnuplot_2d(int periods, double dt)  //3d graph
    {
        FILE *gnuplotPIPE = popen ("gnuplot -persistent", "w");
        fprintf(gnuplotPIPE, "reset\n");
        fprintf(gnuplotPIPE, "set terminal gif animate\n");
        fprintf(gnuplotPIPE, "set output 'result2d.gif'\n");
        fprintf(gnuplotPIPE, "stats 'data2.txt' nooutput\n");
        fprintf(gnuplotPIPE, "set cbrange[0:1]\n");
        fprintf(gnuplotPIPE, "set view map\n");
        fprintf(gnuplotPIPE, "set xrange [0:5.0]\n");
        fprintf(gnuplotPIPE, "set yrange [0:5.0]\n");
        fprintf(gnuplotPIPE, "set key tc  'black'\n");
        fprintf(gnuplotPIPE, "set key at 5.53, 5.53\n");
        fprintf(gnuplotPIPE, "set xlabel 'v'\n");
        fprintf(gnuplotPIPE, "set ylabel 'x'\n");
        fprintf(gnuplotPIPE, "set cblabel 'f(x,v,t)'\n");
        fprintf(gnuplotPIPE, "set title 'Solution heatmap'\n");
        fprintf(gnuplotPIPE, "do for [i=1: int(STATS_blocks)]{\n");
        fprintf(gnuplotPIPE, "plot 'data2.txt' index (i-1) u ($1/20):($2/20):3 matrix with image title sprintf(\"t=%%0.2f \", %f*i),\n", periods*dt);
        fprintf(gnuplotPIPE, "}\n");
        pclose(gnuplotPIPE);
    }
};

void advection_init(double ** u, double dm, int Mrange, int Xrange)// initial and boundary conditions for Smoluchowski equation i - space, m - particle size
{
    for (int i = 0; i < 1; i++)
    {
        for(int m = 0; m < Mrange; m++)
            u[i][m] = exp(-pow((m) * dm, 2));
    }
    
    for (int i = 1; i < Xrange; i++)
    {
        for(int m = 0; m < Mrange; m++)
            u[i][m] = 0.0;
    }
}

double velocity(double r, double m)
{
    //return 1.0/(1.0+m);
    return 1.0;
}

double sigma(double r, int Xrange, int PML, double dr)
{
    if (r > (dr * (Xrange - PML))) 
    {
        return pow((r - (Xrange - PML) * dr) / (PML * dr), 2) * 3.0 * log(10.0) * 10.0 / (PML * dr);
    }
    else
    {
        return 0;
    }
}

void limiters(double *r_axis, double *m_axis, double dr, double dt, int Xrange, int Mrange, int PML, double ** u)
{
    double a,b;
    double ** res = new double * [Xrange];
    for (int i = 0; i < Xrange; i++)
        res[i] = new double [Mrange];
    double *vel;
    vel = (double*) malloc ((Mrange) * sizeof(double));
    for (int m = 0;m < Mrange; m++)
        vel[m] = velocity(*r_axis, *(m_axis + m));
    for (int m = 0; m < Mrange; m++)
    {
        res[0][m] = u[0][m];
        res[Xrange-1][m] = u[Xrange - 1][m];
    }
    for (int i = 1;i < Xrange - 1; i++)
    {
        for (int m = 0; m < Mrange; m++)
        {
            b = (u[i + 1][m] - u[i - 1][m]);
            if (( (u[i + 1][m]-u[i][m])*(u[i][m]-u[i-1][m]))>0)
                a = min(min(abs(b) / (2.0 * dr),
                            2.0 * abs(u[i + 1][m] - u[i][m]) / dr),
                        2.0 * abs(u[i][m] - u[i-1][m]) / dr) * (b >= 0 ? 1.0: -1.0);
            else a = 0;
            res[i][m] = u[i][m]+dr/2.0*(1.0-vel[m]*dt/dr)*a;
        }
    }
    for (int i = 1;i < Xrange - 1; i++)
    {
        for(int m = 0;m < Mrange; m++)
            u[i][m] = (vel[m] * res[i][m] - vel[m] * res[i - 1][m]) / dr +
                       vel[m] * sigma(*(r_axis+i), Xrange, PML, dr) * u[i][m];
    }
    free(vel);
    for (int i = 0; i < Xrange; i++)
        delete res[i];
    delete res;
}


class TKernel2: public TMatrix
{
/// TKernel2 approximates the matrix of solutions at time steps =0s, 1s, ... 9s
public:
    double **Sol;                             /// the matrix of solutions itself
    int S;
    TKernel2 (const int &, const int &);      /// a better function to initialize memory allocation, but I failed to use it
    ~TKernel2()
    {
        for (int i = 0; i < S; i++)
            delete [] Sol[i];
        delete [] Sol;
    }
    double value (const int & , const int &); /// get elements of matrix
};


TKernel2::TKernel2(const int &S, const int &N): TMatrix (S, N)
{
    this->S = S;
    Sol = new double * [S];
        for (int i = 0; i < S; i++)
            Sol[i] = new double [N];
}

double TKernel2::value(const int &i, const int &j)
{
        return Sol[i][j];
}

double K(const int & u, const int &v, const double h)
{
    //return 1.0;
    double u1=(u+1.0) * h;
    double v1=(v+1.0) * h;
    return pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), 2) * sqrt((1.0l / u1 + 1.0l / v1));
}

class TKernel: public TMatrix
{
public:
    double h;
    TKernel (const int &, const int &);
    double value (const int & , const int &);
};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{

}

double TKernel::value(const int &i, const int &j)
{
        return h * K(i, j, h);
}

double L1(const int &N, const int &i, const double *n, const double &h)
{
    double l1 = 0;
    for (int i1 = 1; i1 <= i; i1++)
    {
        l1 += n[i1] * n[i - i1] * K((i - i1) , i1 ,h) + n[i1 - 1] * n[i - i1 + 1] * K((i - i1 + 1) , (i1 - 1) ,h);
    }
    l1 *= h/2;
    return l1;
}

double L2(const int &N, const int &i, const double *n, const double &h)
{
    double l2 = 0;
    for (int i1 = 1; i1 < N; i1++)
    {
        l2 += n[i1] * K(i , i1,h) + n[i1 - 1] * K(i , (i1 - 1), h );
    }
    l2 *= h/2;
    return l2;
}

void Smolushowski_operator(double *inp, double *out, int N_grid_nodes)
{

    double *L1_res_vec, *L2_res_vec, *L2_correction;
    L2_correction = (double *) malloc (N_grid_nodes * sizeof(double));
}

int main(int argc, char **argv)
{
    if (argc != 13)
    {
        printf("12 parameters have been expected\n");
        printf("v_max n_steps time_step n_knots tol num_threads ifpaint\n");
        return -1;
    }

    bool if_paint = true;
    bool if_singular_value_data = true;

    int num_of_threads = 1;
    omp_set_num_threads(num_of_threads);
    double dm  = 0.025;                                  ///particle's size step (dm)
    double dr = 0.025;                                   ///space step
    double dt = 0.0005;                                  ///time step
    int N_time_steps = (int) 1000;                ///(max_time_steps)
    int N_m_grid_nodes = (int) 100;                      /// Number of grid nodes corresponging to particles' size
    // Max possible particle size = N_m_grid_nodes * dm 
    int PML = (int)(N_m_grid_nodes * 0.1);            ///width of PML layer
    int max_distance_pml = (int) N_m_grid_nodes + PML;/// max distance from origin (includes PML)
    int periods = 10;                                    /// every periodsth calculation goes to gnuplot
    int ranks_periods = 10;                              /// for every ranksperiodsth calculation we also calculate solution rank
    double tolerance = 1e-6;

    double tolerance_ranks = 1e-4;

    sscanf(argv[1], "%lf", &if_paint);
    sscanf(argv[2], "%d", &if_singular_value_data);
    sscanf(argv[3], "%lf", &num_of_threads);
    sscanf(argv[4], "%d", &dm);
    sscanf(argv[5], "%lf", &dr);
    sscanf(argv[6], "%d", &dt);
    sscanf(argv[7], "%d", &N_time_steps);
    sscanf(argv[8], "%d", &N_m_grid_nodes);
    sscanf(argv[9], "%d", &periods);
    sscanf(argv[10], "%d", &ranks_periods);
    sscanf(argv[11], "%d", &tolerance);
    sscanf(argv[12], "%d", &tolerance_ranks);



    TCross_Parallel_v1_Parameters parameters;
    parameters.tolerance = tolerance;
    parameters.maximal_iterations_number = 0;

    TCross_Parallel_v1_Parameters parameters2;
    parameters2.tolerance = tolerance_ranks;
    parameters2.maximal_iterations_number = 0;

    double *n_k;
    double *L1_res_vec, *L2_res_vec, *L2_correction;
    L2_correction = (double *) malloc (N_m_grid_nodes * sizeof(double));


    TKernel kernel(N_m_grid_nodes, N_m_grid_nodes);
    kernel.h = dm;
    TCross_Parallel_v1 crossed_kernel;
    crossed_kernel.Approximate(&kernel, parameters);
    int rank = crossed_kernel.get_rank();
    printf("the rank of the approximated coagulation kernel is %d\n", rank);

    FILE *output;                                        /// output for graph
    FILE *output2d;                                      /// output for heat map

    if (if_paint)
    {
        output = fopen("data.txt", "w");
        output2d = fopen("data2.txt", "w");
    }

    double **smoluch_operator;                           ///array for storing S(f^n,f^n) from ( f^(n+1) = f^n + dt*A(f^n)+dt*S(f^n,f^n) )
    double **initial_layer;                              ///array for storing f^n(x,v)
    double **u;
    double r[max_distance_pml];
    double mass[N_m_grid_nodes];
    for (int m = 0; m < N_m_grid_nodes; m++)
        mass[m] = dm * m;                                 ///particle size coordinate axis
    for (int i = 0; i < max_distance_pml; i++)
        r[i] = dr * i;                                   ///space coordinate axis


    ///allocate memory to arrays start
    u = new double * [max_distance_pml];
    for (int i = 0; i < max_distance_pml; i++)
        u[i] = new double [N_m_grid_nodes];


    smoluch_operator = new double * [max_distance_pml];
    for (int i = 0; i < max_distance_pml; i++)
        smoluch_operator[i] = new double [N_m_grid_nodes];

    initial_layer = new double * [max_distance_pml];
    for (int i = 0; i < max_distance_pml; i++)
        initial_layer[i] = new double [N_m_grid_nodes];

    advection_init(initial_layer, dm, N_m_grid_nodes, max_distance_pml);
    advection_init(u, dm, N_m_grid_nodes, max_distance_pml);

    n_k = (double *) malloc (N_m_grid_nodes * sizeof(double));
    ///allocate memory to arrays end

    for (int t = 0; t < N_time_steps; t += 1)
    {
    ///THE MAIN TIME LOOP START

        ///data for painting start
        if (if_paint && (t % periods == 0))
        {
            for(int i = 0; i < ((max_distance_pml - PML)); i++)
            {
                for(int m = 0; m < N_m_grid_nodes; m++) fprintf(output2d, "%f ", u[i][m]);
                fprintf(output2d, "\n");
            }
            fprintf(output2d, "\n\n");

            for(int i = 0;i < ((max_distance_pml - PML)); i++) fprintf(output, "%f\t%f\t%f\t%f\n", r[i], u[i][N_m_grid_nodes/25], u[i][N_m_grid_nodes/2], u[i][N_m_grid_nodes*7/10]);

	    fprintf(output, "\n\n");
        }
        ///data for painting end


        ///smoluchowski operator start
        for (int j = 1; j < max_distance_pml; j++)
        {
            for (int m = 0; m < N_m_grid_nodes; m++)
                n_k[m] = u[j][m];
            /// fast computations of L1 and L2
            L2_res_vec = crossed_kernel.matvec(n_k);
            L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k);
            /// compute corrections for L2 into trapecial weights
            for (int i = 0; i < N_m_grid_nodes; i++)
            {
                L2_correction[i] = ( K( i, 1 ,dm ) * n_k[0] + K(i, N_m_grid_nodes ,dm) * n_k[N_m_grid_nodes - 1] ) * dm;
            }
            cblas_daxpy(N_m_grid_nodes, -0.5, L2_correction, 1, L2_res_vec, 1);
            for (int i = 0; i < N_m_grid_nodes; i++)
            {
                smoluch_operator[j][i] = ( L1_res_vec[i] * 0.5 - n_k[i] * L2_res_vec[i] );
                if (smoluch_operator[j][i] < 0.0) smoluch_operator[j][i] = 0.0;
            }
            free (L2_res_vec);
            L2_res_vec = NULL;
            free (L1_res_vec);
            L1_res_vec = NULL;
            free(n_k);
            n_k = (double *) malloc (N_m_grid_nodes * sizeof(double));
        } ///now smoluch_operator <==> S(f^n,f^n)
        ///smoluchowski operator end

        ///advection operator start
        limiters(r, mass, dr, dt, max_distance_pml, N_m_grid_nodes, PML, u); /// now u <==> A(f^n)
        ///advection operator end

        ///calculation of next time layer start
        for (int i = 1; i < max_distance_pml; i++)
        {
            for (int m = 0; m < N_m_grid_nodes; m++)
            {
                u[i][m] = initial_layer[i][m] + dt * (smoluch_operator[i][m] - u[i][m]); ///now u <==> f^(n+1)(x,v)
            }
        }

        for (int i = 0; i < max_distance_pml; i++)
            delete [] smoluch_operator[i];
        for (int i = 0; i < max_distance_pml; i++)
            delete [] initial_layer[i];
        delete [] initial_layer;
        delete [] smoluch_operator;

        initial_layer = new double * [max_distance_pml];
        smoluch_operator = new double * [max_distance_pml];
        for (int i = 0; i < max_distance_pml; i++)
        {
            initial_layer[i] = new double [N_m_grid_nodes];
            smoluch_operator[i] = new double [N_m_grid_nodes];
        }

        for (int i = 1; i < max_distance_pml; i++)
        {
            for (int m = 0; m < N_m_grid_nodes; m++)
            {
                initial_layer[i][m] = u[i][m];
                smoluch_operator[i][m] = u[i][m];
            }
        }
        ///calculation of next time layer end

        ///calculation of rank and singular values of solution matrix start
        if (if_singular_value_data){
            if (t%(ranks_periods) == 0){

		TKernel2 kernel2(max_distance_pml-PML, N_m_grid_nodes);

                for (int i=0;i<max_distance_pml-PML;i++){
                    for (int m=0;m<N_m_grid_nodes;m++)
                        kernel2.Sol[i][m] = u[i][m];
                }

		TCross_Parallel_v1 crossed_kernel2;
                int rank2;
                crossed_kernel2.Approximate(&kernel2, parameters2);
                rank2 = crossed_kernel2.get_rank();
                printf("the rank of the solution at t= %.2f is %d\n", t*dt, rank2);
                double* a;
                a = (double *) malloc (N_m_grid_nodes * (max_distance_pml-PML)* sizeof(double));
                for (int i=0;i<max_distance_pml-PML;i++){
                    for (int m=0;m<N_m_grid_nodes;m++)
                        a[m+N_m_grid_nodes*i] = u[i][m];
                }
                double* u = (double *) malloc (sizeof(double));
                double* vt = (double *) malloc (sizeof(double));
                double* s = (double*) malloc (N_m_grid_nodes*sizeof(double));
                double* superb = (double*) malloc (N_m_grid_nodes*sizeof(double));
                int result = LAPACKE_dgesvd(LAPACK_ROW_MAJOR, 'N', 'N', N_m_grid_nodes, max_distance_pml-PML, a, N_m_grid_nodes, s, u, N_m_grid_nodes, vt, N_m_grid_nodes, superb);
                printf("result of svd is equal to %d\n", result);
                printf("here's the list the biggest 30 singular numbers\n");
                for (int i=0;i<30; i++) printf("%.4f\t", s[i]);
                printf("\n\n\n");

                free(u);
                free(vt);
                free(s);
                free(superb);
                free (a);
            }
        }
        ///calculation of rank and singular values of solution matrix end

    } /// THE MAIN TIME LOOP END

    for (int i = 0; i < max_distance_pml; i++) 
        delete [] smoluch_operator[i];
    delete [] smoluch_operator;

    for (int i = 0; i < max_distance_pml; i++) 
        delete [] initial_layer[i];
    delete [] initial_layer;

    for (int i = 0; i < max_distance_pml; i++)
        delete [] u[i];
    delete [] u;
    
    free(n_k);
    
    
    if (if_paint)
    {
        fclose(output);
        fclose(output2d);
        visualisation_gnuplot::gnuplot_1d(max_distance_pml, PML, dr, periods, dt);
        visualisation_gnuplot::gnuplot_2d(periods, dt);
    }
    return 0;
}
