      program Smoluhovsky_2d_numerical_solver
c--------------------------------------------------------------
c     It solves 2D Smoluhovski equation
c
c     d_n(vx,vy,t)/d_t=0.5*integral(d_ux[0-v1])integral(d_uy[0-v2])
c                      {K(vx-ux,vy-uy,ux,uy)n(ux,uy,t)n(vx-ux,vy-uy,t)} -
c                     -n(vx,vy,t)integral(d_ux[0-inf])integral(d_uy[0-inf)
c                      {K(vx,vy,ux,uy)n(ux,uy,t)} 
c
c     at uniform numerical mesh 
c     v_x(0:Nx)  (0= < v_x =< v_x_max) (0:Nx)
c     v_y(0:Ny)  (0= < v_y =< v_y_max) (0:Ny)
c     using predictor-corrector two steps method
c     t(0,t_max) is a time
c     tau is a time step 
c
c     Here
c     n(vx,vy,t) is determined that 
c                n*d_vx*d_vy is averaged particle's concentration.
c                The volumes of these particles are in the intervals
c                (vx.vx+dvx),(vy,vy+dvy).
c                The dimention of n is [1/v**3] = [1/m**9]
c
c     K          coagulation kernel. The dimention K is [v/t]=[m**3/sec]  
c
c     The equation was solved in normalized variables using
c     following character parameters for normalization:
c         
c     v_ch character volume  [m**3]
c     tau_ch character time  used in coagulation Kernel [sec]  
c     V_total_ch character total volume used in coagulation Kernel [m**3] 
c
c     The given character parameters were used for additional character
c     parameters:
c  
c     t_ch = tau_ch*v_ch/V_total_ch  character time of the Smoluhovski equation
c                                    [sec]
c     n_ch = 1/V_ch**3 character concentration [1/m**9]
c
c
c     Normalized variables are
c
c     v_x = v_x_dimention/v_ch           normalized concentraition x
c     v_y = v_y_dimention/v_ch           normalized concentration y
c     t = t_dimention/t_ch               normalized time
c     K = K_dimention*tau_ch/V_total_ch  normalized coagulation Kernel

c-------------------------------------------------------------
      use One_NmlMod
      use One_NoNmlMod

      implicit none
    
      real*8 
     & t_t,       !time at the next time step
     & t_c,       !time at intermediate time step  
     & t_b,        !time at the previous time step
     & time

      real*8 
     &r_integral_2d_1, !first 2D integral in RHS of of Smoluhosky equations
     &r_integral_2d_2, !second 2D integral in RHS of of Smoluhosky equations
     &difnorm,         !norm of difference between numerical and
                       !analytical solutions
     &difc,            !error in C norm (maximal difference) 
     &difrc,           !relative error in norm C
     &total_n_0,       !total number of initial particles
     &total_n,         !total number of particles
     &total_nv_0,      !total initial volume
     &total_nv,         !total volume
     &run_time_1,run_time_2,run_time_3, !for cpu time
     &t_plot            !time for plots
      integer i,j,i1,j1,ic,jc,irc,jrc,
     &n_timestep



      integer ::  N_integration
      parameter(N_integration=100) !number of integration points

      real*8 :: Q0, Q0_normalized,density,volume,
     &dk_coag_l !it  is not used 

      real :: time1,time2

c-----externals
      real*8 w_x,w_x_prime,Q_normalization_N_species,w_x_t
      external w_x,w_x_prime,w_x_t
c-----------------------------------------------------

c      call cpu_time(run_time_1)

      write(*,*)'Smoluhovsky_2d_numerical_solver'
c--------------------------------------------------------------------------
c     It creates default input data in coagulation.dat file.
c     The results of the work  are in One_NmlMod
c-----------------------------------------------------------------------    
      write(*,*)'before default_in'

      call default_in
c-----------------------------------------------------------
c     reads all namelists from coagulation.dat input file
c----------------------------------------------------------- 
      write(*,*)'beforer ead_all_namelists'
 
      call read_all_namelists 

      write(*,*)'beforer ead_all_namelists'
      write(*,*)'v0_vector',v0_vector
      write(*,*)'v_particle_min_vector',v_particle_min_vector
      write(*,*)'v_particle_max_vector',v_particle_max_vector

c------------------------------------------------------------
c     number opf time steps
c-----------------------------------------------------------
      n_timesteps=t_max/tau
c------------------------------------------------------------
c     allocate pointers in One_NoNmlMod
c------------------------------------------------------------
c     write(*,*)'before ainalloc_arrays_One_NoNmlMod'
c      call ainalloc_arrays_One_NoNmlMod


      write(*,*)'after ainalloc_arrays_One_NoNmlMod'
      write(*,*)'v0_vector',v0_vector
      write(*,*)'v_particle_min_vector',v_particle_min_vector
      write(*,*)'v_particle_max_vector',v_particle_max_vector

c-----------------------------------------------------------
c     initialization
c-----------------------------------------------------------
      call dinit 

      rN_0=1.d0
    
c----------------------------------------------------------
c     open output plot.ps file
c---------------------------------------------------------
      call plotinit
      
      write(*,*)'t_max,tau',t_max,tau
      write(*,*)'v_x_0,v_y_0',v_x_0,v_y_0
      write(*,*)'v_x_max,v_y_max',v_x_max,v_y_max
      write(*,*)'rN_0,rK_0',rN_0,rK_0
c-------------------------------------------------------------
c     plot input data to plot.ps file
c-------------------------------------------------------------
      call plot_input_data  
 
c      stop 'after plot_input_data'

c------------------------------------------------------------
c     calculate initial distribution of concentration rn_2d_b(0:Nx,0:Ny)
c-------------------------------------------------------------
c      write(*,*)'v_x_0.v_y_0',v_x_0,v_y_0
c      do i=1,Nx
c         write(*,*)'i,v_x(i)',i,v_x(i)
c      enddo
c      do j=1,Ny
c         write(*,*)'j,v_y(j)',i,v_y(j)
c      enddo

      call initial_concentation_distribution(Nx,Ny,v_x,v_y,
     &v_x_0,v_y_0,rN_0,rn_2d_b)

      call initial_concentation_distribution_w_x(Nx,Ny,v_x,v_y,
     &v_x_0,v_y_0,rN_0,rn_2d_wx)

c      do i=1,nx
c         do j=1,ny
c           write(*,*)'i,j,rn_2d_b(i,j)',i,j,rn_2d_b(i,j)
c         enddo
c      enddo

      rn_2d_c=rn_2d_b !initial distribution at t =(1/2)tau


c      do i=1,nx
c         do j=1,ny
c           write(*,*)'i,j,rn_2d_c(i,j)',i,j,rn_2d_c(i,j)
c         enddo
c      enddo

c-------------------------------------------------------------
c     calculate total number of initial particles: total_n_0
c     calculate total initial volume: total_nv_0
c-----------------------------------------------------------
      total_n_0=0.d0
      total_nv_0=0.d0
      do i=0,Nx
        do j=0,Ny
           rn_2d_b(i,j)=rN_0/(v_x_0*v_y_0)*
     &                  dexp(-v_x(i)/v_x_0-v_y(j)/v_y_0) 
           rn_2d_c(i,j)=rn_2d_b(i,j)
           total_n_0=total_n_0+rn_2d_b(i,j)
           total_nv_0=total_nv_0+rn_2d_b(i,j)*(v_x(i)+v_y(j))
        enddo
      enddo
 
      total_n_0=total_n_0*step_x*step_y
      total_nv_0=total_nv_0*step_x*step_y

      write(*,*)'total_n_0,total_nv_0',total_n_0,total_nv_0

c      stop 'total_n_0,total_nv_0'

      call number_of_particle_total_volume(Nx,Ny,v_x,v_y,rn_2d_b,
     &total_n_0,total_nv_0)

      write(*,*)'total_n_0,total_nv_0',total_n_0,total_nv_0
c-----------------------------------------------------------
c     Plot ten contours of initial distribution n(v_x,v_y)=const_i (i=1:10)
c     This plot will be in output file plot.ps
c-----------------------------------------------------------
 
c-      call plot_2d_contours_of_rn_2d(Nx,Ny,rn_2d_b,
c-     &v_x,v_y,0.d0)

c      stop 'after plot_2d_contours_of_rn_2d'

c-----------------------------------------------------------------
c     Calulates  Output volume mesh  vol_plot_ar(1:n_vol_plot) 
c     was set inside this subtroutine.
c     The mesh is close to the article
c     G.Palaniswaamy, S.K. Loyalka, Direct simulation
c     Monte-Calo aerosol dynamics: coagulation and collisional sampling,
c     Nuclear Technology, Vol. 156, Oct. 2006, pp.20-38
c-----------------------------------------------------------------
      write(*,*) 'before create_vol_plot_aray'

      call create_vol_plot_aray
c------------------------------------------------------------------
c     initialize times
c----------------------------------------------------------------
      t_b=0.d0   !t_bottom
      t_t=0.d0   !t_top
      t_c=0.d0   !t_center=(t_bottom+t_top)/2
c-------------------------------------------------------------------
c     plot sectional distribution versus volume from initial n
c-------------------------------------------------------------------
      write(*,*)'befor
     &plot_distribution_particle_number_mass_on_volume'

c-      call plot_distribution_particle_number_mass_on_volume
c-     &(v_x_0,v_y_0,Nx,Ny,rn_2d_b,v_x,v_y,t_t,'initial',
c-     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

c      stop 'after lot_distribution_particle_number_mass_on_volume ini'

c-      call plot_distribution_particle_number_mass_on_volume
c-     &(v_x_0,v_y_0,Nx,Ny,rn_2d_wx,v_x,v_y,t_t,'initial wx',
c-     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
 
c       write(*,*)'aft plot_distribution_particle_number_mass_on_volume'

c       stop 'af lot_distribution_particle_number_mass_on_volume iniwx'

c       write(*,*)'v_x',v_x
c       write(*,*)'v_y',v_y
c       write(*,*)'rn_2d_wx',rn_2d_wx

c       write(*,*)'v0_vector',v0_vector
c       write(*,*)'v_particle_max_vector',v_particle_max_vector
c       write(*,*)'v_particle_min_vector',v_particle_min_vector
c       write(*,*)'v_x',v_x
c       write(*,*)'v_y',v_y

c      call create_plot_distribution_particle_number_mass_on_vol_t(
c     &Nx,Ny,N_particle,N_particle,N_species,
c     &v0_vector,v_particle_max_vector,v_particle_min_vector,y_ar,t_b,
c     &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar) 

      do i=0,Nx
         v_x(i)=v_particle_min_vector(1)+
     &   ((v_particle_max_vector(1)-v_particle_min_vector(1))/(Nx+1))*i
      enddo  

      do i=0,Ny
         v_y(i)=v_particle_min_vector(2)+
     &   ((v_particle_max_vector(2)-v_particle_min_vector(2))/(Ny+1))*i
      enddo

c-----Smoluhovski code -distribution at MC mesh
c-      call  initial_concentation_distribution(Nx,Ny,v_x,v_y,
c-     &v_x_0,v_y_0,rN_0,rn_2d_b)

c-      call plot_distribution_particle_number_mass_on_volume
c-     &(v_x_0,v_y_0,Nx,Ny,rn_2d_b,v_x,v_y,t_t,'initial mesh',
c-     &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

c      stop 'plot_distribution_particle_number_mass_on_volume in mesh'
c-----distribution from w_x_t function at MC mesh
c-      call initial_concentation_distribution_w_x_t(Nx,Ny,v_x,v_y,
c-     &v_x_0,v_y_0,rN_0,rn_2d_wx)

c-      call plot_distribution_particle_number_mass_on_volume
c-     &(v_x_0,v_y_0,Nx,Ny,rn_2d_wx,v_x,v_y,t_t,'initial wx_t',
c-    &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

c      stop 'plot_distribution_particle_number_mass_on_volume in_wx_t'

c      stop 'smoluhovsky after
c     &plot_distribution_particle_number_mass_on_volume 2'
    

c-      call create_plot_distribution_particle_number_mass_on_vol_t(
c-     &Nx,Ny,N_particle,N_particle,N_species,
c-     &v0_vector,v_particle_max_vector,v_particle_min_vector,y_ar,t_b,
c-     &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar,
c-     &v_x,v_y,v_x_0,v_y_0,rN_0,rn_2d_wx)

c      write(*,*)'after
c     &create_plot_distribution_particle_number_mass_on_vol_t'

c      stop 'before MC initializaion'

c---------------------------------------------------------------------
c     Monte-Carlo initialization
c-----------------------------------------------------------------------      
      
c------------------------------------------------------------
c     analytical formula Q0 for q~v_sum*exp(-v/v0)
c-----------------------------------------------------------      
      write(*,*)'befor analytical_Q0_for_exp_distribution'
      write(*,*)'N_species',N_species
      write(*,*)'v_particle_min_vector',v_particle_min_vector
      write(*,*)'v_particle_max_vector',v_particle_max_vector
      write(*,*)'v0_vector',v0_vector
      write(*,*)'rN_0',rN_0

      call analytical_Q0_for_exp_distribution(N_species,
     &v_particle_min_vector,v_particle_max_vector,v0_vector,
     &rN_0,Q0)

      write(*,*)'mass_flow analytical Q0=',Q0
c----------------------------------------------------------
c     numerical calculation of Q0_normalized for 1D and 2D cases
c     for test it should be Q0_normalized=1
c----------------------------------------------------- 

      if(N_species.eq.2)then

         write(*,*)'before q0_calculation_2d'
         write(*,*)'N_species',N_species 
         write(*,*)'V_particle_min_vector',V_particle_min_vector
         write(*,*)'V_particle_max_vector',V_particle_max_vector
         write(*,*)'N_integration',N_integration
         write(*,*)'v0_vector',v0_vector

         call q0_calculation_2d( N_species, 
     &   V_particle_min_vector,V_particle_max_vector,
     &   N_integration,v0_vector,w_x,Q0_normalized)
        
         write(*,*)'mass_flow after call q0_calculation_2d'
         write(*,*)'Q0_normalized=',Q0_normalized

      endif   
c-------------------------------------------------------------
c     initialization y_ar
c--------------------------------------------------------------
      time=0.d0
      call  rejected_accepted(N_particle,N_species,
     &w_x,w_x_prime,v0_vector,
     &v_particle_max_vector,v_particle_min_vector,y_ar)

      write(*,*)'mass_flow after rejected_accepted'
c      stop 'mass_flow after rejected_accepted'    
      call calculate_total_density_from_particles(N_species,
     &N_particle,y_ar,
     &v_particle_min_vector,v_particle_max_vector,v0_vector,
     &density,volume)

c      write(*,*)'time',time,'density=',density
c      write(*,*)'volume',volume,'volume/N_particle',volume/N_particle
c      write(*,*)'density/N_particle',density/N_particle
c      write(*,*)'1.d0/(1.d0+0.5d0*time)',1.d0/(1.d0+0.5d0*time)

      n_timestep=0
      density_time(n_timestep,1)=n_timestep
      density_time(n_timestep,2)=time
      density_time(n_timestep,3)=density/N_particle
      density_time(n_timestep,4)=1.d0/(1.d0+0.5d0*time)

      call calculate_integral_from_w_x(N_species,v0_vector,
     &v_particle_min_vector,
     &v_particle_max_vector)


c      stop 'mass_flow after calculate_integral_from_w_x' 

c-      call  create_plot_distribution_particle_number_mass_on_vol(
c-     &Nx,Ny,N_particle,N_particle,N_species,
c-     &v0_vector,v_particle_max_vector,v_particle_min_vector,y_ar,t_b,
c-     &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

c      stop 'mass_flow after
c     &create_plot_distribution_particle_number_mass_on_vol' 
     
      time=0.d0
      t_plot=h_plot

      call cpu_time(time1)

      do while (time.lt.t_max) 
        
c        call coagulation_time_step(N_particle,N_particles_ar_dim,

c        write(*,*)'bef coagulation_time_step'
c        write(*,*)'N_particle',N_particle
c        write(*,*)'rK_0,i_coag_type,Q0',rK_0,i_coag_type,Q0
         
         call coagulation_time_step(N_species,N_particle,N_particle,
     &        y_ar,rK_0,i_coag_type,dk_coag_l,
     &        Q0,
     &        tau) 
c         write(*,*)'aft coagulation_time_step tau',tau
            
c         write(*,*)'t_plot,time=',t_plot,time

c         stop'aft coagulation_time_step tau_coag'


         tau=tau*0.1
c         write(*,*)'tau',tau

         time=time+tau
c         write(*,*)'time,tau',time,tau 

c         write(*,*)'t_plot,time=',t_plot,time
c-------------------------------------------------------
c       integration of coagulation
c       y_ar(i),k -> y_ar(i),k+1
c            
c-------------------------------------------------------
c        write(*,*)'mass_flow before coagulation_step'
          

c        call cpu_time(time_1)

        call coagulation_step(N_species,N_particle,N_particle,
     &  tau,q0,
     &  rK_0,i_coag_type,dk_coag_l,y_ar)

c        write(*,*)'mass_flow after coagulation_step'
         if (t_plot .lt.time) then

            write(*,*)'print h_plot,t_plot,time',
     &                       h_plot,t_plot,time

            call  create_plot_distribution_particle_number_mass_on_vol(
     &        Nx,Ny,N_particle,N_particle,N_species,
     &  v0_vector,v_particle_max_vector,v_particle_min_vector,y_ar,time,
     &        n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
              
            t_plot=t_plot+h_plot

            call calculate_total_density_from_particles(N_species,
     &      N_particle,y_ar,
     &      v_particle_min_vector,v_particle_max_vector,v0_vector,
     &      density,volume)


            call calculate_total_density_from_particles(N_species,
     &      N_particle,y_ar,
     &      v_particle_min_vector,
     &      v_particle_max_vector,
     &      v0_vector,
     &      density,volume)

c             write(*,*)'//'
c             write(*,*)'time',time,'density=',density 
c             write(*,*)'volume',volume,
c     &                 'volume/N_particle',volume/N_particle
c             write(*,*)'density/N_particle',density/N_particle
c             write(*,*)'1.d0/(1.d0+0.5d0*time)',1.d0/(1.d0+0.5d0*time)

             n_timestep=n_timestep+1
      
             density_time(n_timestep,1)=n_timestep
             density_time(n_timestep,2)=time
             density_time(n_timestep,3)=density/N_particle
             density_time(n_timestep,4)=1.d0/(1.d0+0.5d0*time)


         endif
      enddo !while time.lt.t_max
      call cpu_time(time2)

      write(*,*)'**********Monter-Carlo cpu_time',time2-time1
 
c      write(*,*)'n_plots', n_plots  
c      write(*,*)'n_timestep',n_timestep
c      do i=0,n_timestep
c         write(*,*)'i,time,density_MC,density_analitical',
c     &   density_time(i,1),density_time(i,2),density_time(i,3),
c     &   density_time(i,4)
c      enddo
c----------------------------------------------------------------
c     write output data to density_time.dat file
c     n_timestep,time,density_MC,density_analytical
c----------------------------------------------------------------
      call write_density_at_time(density_time,n_plots)
c----------------------------------------------------------------

      call calculate_total_density_from_particles(N_species,
     &N_particle,y_ar,
     &v_particle_min_vector,
     &v_particle_max_vector,
     &v0_vector,
     &density,volume)

  
c      write(*,*)'time',time,'density=',density 
c      write(*,*)'volume',volume,'volume/N_particle',volume/N_particle
c      write(*,*)'density/N_particle',density/N_particle
c      write(*,*)'1.d0/(1.d0+0.5d0*time)',1.d0/(1.d0+0.5d0*time)

      stop 'MC'
c------------------------------------------------------------------
c     Main time's loop to calulate n(vx,vt,time) in time
c     0< t =<t_max
c-----------------------------------------------------------------
      n_timestep=0
      do while(t_t.le.t_max)
        n_timestep=n_timestep+1
       
        t_b=t_t
c----------------------------------------------------
c       two steps predictor-corretor solution
c       of Smoluhvosky equation 
c-----------------------------------------------------
c-------first 1/2 time step
        write(*,*)'1/2 time step t_b=',t_b
c-------------------------------------------------------
c       calculate 2D integrals in RHS of Smoluhosky equations
c       and n=rn_2d_c(0:Nx,0:Ny) at 1/2 time step
c
c       (n_1/2 - n) / (0.5tau) = Integral_1-n*Integral_2
c
c------------------------------------------------------
        do i=0,Nx
          do j=0,Ny
c-----------2d integral 1 
            call calculate_integral_2d_1(Nx,Ny,i,j,rn_2d_b,v_x,v_y,
     &                                   r_integral_2d_1)
c-----------2d integral 2 
            call calculate_integral_2d_2(Nx,Ny,i,j,rn_2d_b,v_x,v_y,
     &                                   r_integral_2d_2)
c-----------calculate n at 1/2 time step 
            rn_2d_c(i,j)=rn_2d_b(i,j)+  
     &        (r_integral_2d_1-rn_2d_b(i,j)*r_integral_2d_2)*0.5d0*tau

c            write(*,*)'i,j,rn_2d_t(i,j)',i,j,rn_2d_t(i,j)


c            write(*,*)'r_integral_2d_1,r_integral_2d_2,rn_2d_b(i,j)',
c     &                 r_integral_2d_1,r_integral_2d_2,rn_2d_b(i,j)
          enddo
        enddo
c        stop 'end of 1/2 step'
c-------second full time step
        write(*,*)'1 time step t_b=',t_b
     
c-------------------------------------------------------
c       calculate 2D integrals in RHS of Smoluhosky equations
c       and n=rn_2d_c(0:Nx,0:Ny) at 1/2 time step
c
c       (n_1-n)/(tau)=Integral_1-n*Integral_2
c----------------------------------------------------------
        do i=0,Nx
          do j=0,Ny
c-----------2d integral 1 
            call calculate_integral_2d_1(Nx,Ny,i,j,rn_2d_c,v_x,v_y,
     &                                   r_integral_2d_1)
c-----------2d integral 2
            call calculate_integral_2d_2(Nx,Ny,i,j,rn_2d_c,v_x,v_y,
     &                                   r_integral_2d_2)
c-----------calculate n at full time step 
            rn_2d_t(i,j)=rn_2d_b(i,j)+  
     &        (r_integral_2d_1-rn_2d_b(i,j)*r_integral_2d_2)*tau

c           write(*,*)'i,j,rn_2d_t(i,j)',i,j,rn_2d_t(i,j)

c           write(*,*)'r_integral_2d_1,r_integral_2d_2,rn_2d_b(i,j)',
c     &                r_integral_2d_1,r_integral_2d_2,rn_2d_b(i,j)

          enddo !j
        enddo   !i


        t_t=t_b+tau

        write(*,*)'after time step t_t,t_b,tau',t_t,t_b,tau
c------------------------------------------------------------------
c       calculate
c       total number of particles: total_n
c       and total volume: total_nv)
c-----------------------------------------------------------------
        call number_of_particle_total_volume(Nx,Ny,v_x,v_y,rn_2d_t,
     &  total_n,total_nv)
        write(*,*)'2 total_n_0,total_n',total_n_0,total_n
        write(*,*)'2 total_nv_0,total_nv',total_nv_0,total_nv

c        stop 'end of 1 step'
c-----------------------------------------------------------
c       calculate analytical solution for comparison
c---------------------------------------------------
c       Calculate analytical solution rn_2d_analyt(i,j) (i=0:Nx,j=0:Ny)
c       at constant kernal K=K0=const and the initial concentration
c       n(v1,v2,t-0)=N_0/(v1_0*v2_0)*exp(-(v1/v10+v2/v20)))
c
c       Calculate total number of particles:  total_n
c       and total volume: total_nv=
c       using numerical integration of the analytical concentration 
c  
c       Result
c       rn_2d_analyt(0:Nx,0:Ny) !analytical concentation
c                               !It will be in One_NoNmlMod
c----------------------------------------------------------- 
        call analytical_solution(t_t,total_n,total_nv)

        write(*,*)'after analytical solutiont'
        write(*,*)'total_n_0,total_n',total_n_0,total_n
        write(*,*)'total_nv_0,total_nv',total_nv_0,total_nv
c-------------------------------------------------------------
c       Calculate and plot sectional disributions versus volume to plot.ps file
c       Distributions were calculted from analytical solution n at time t_t
c--------------------------------------------------------------
c        call plot_distribution_particle_number_mass_on_volume
c      & (v_x_0,v_y_0,Nx,Ny,rn_2d_analyt,v_x,v_y,t_t,'analytical',
c      & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
c-------------------------------------------------------------
c       Calculate and plot sectional disributions versus volume  to plot.ps file
c       Distributions were calculted from numerical n at time t_t 
c--------------------------------------------------------------
c        call plot_distribution_particle_number_mass_on_volume
c     &  (v_x_0,v_y_0,Nx,Ny,rn_2d_t,v_x,v_y,t_t,'numerical',
c     &  n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
c------------------------------------------------------------------
c       Calculate and plot sectional distributions 
c       (from numerical n=rn_2d_numer and analytical n=rn_2d_anal)
c       versus volume 
c       (at special output volume mesh) at given time to plot.ps file.
c-------------------------------------------------------------
        call
     &  plot_num_anal_distribution_particle_number_mass_on_volume 
     &  (v_x_0,v_y_0,Nx,Ny,rn_2d_t,rn_2d_analyt,v_x,v_y,t_t,
     &  n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
c-----------------------------------------------------------------
c       Update n at bottom and intermidium steps 
c----------------------------------------------------------------
        do i=0,Nx
          do j=0,Ny 
            rn_2d_b(i,j)=rn_2d_t(i,j)
            rn_2d_c(i,j)=rn_2d_b(i,j)
          enddo
        enddo
c----------------------------------------------------------------------
c       Calculate norms of difference between numerical and
c       analytical solutions
c
c       difnorm=Sum(i[0,Nx])Sumj[0;Ny]
c             |rn_2d_analyt(i,j)-rn_2d_t(i,j)|/(Nx*Ny)
c
c       Calculate point (ic,jc) where the difference
c       difc=|rn_2d_analyt(i,j)-rn_2d_t(i,j)| has maximal value
c       difc it will be error in C norm (maximal difference) 
c
c       Calculte point (icr,jcr) where ratio  has maximal value
c       difrc=|rn_2d_analyt(i,j)-rn_2d_t(i,j)|/|rn_2d_t(i,j)|
c       difcr will be relative error 
c
c       It will print out:
c        difnorm/(Nx*Ny)
c        ic,jc,difc,rn_2d_analyt(ic,jc),rn_2d_t(ic,jc)
c        irc,jrc,difrc,rn_2d_analyt(irc,jrc),rn_2d_t(irc,jrc)
c---------------------------------------------------------------------       
        call difference_numerical_analytical(Nx,Ny,
     &  rn_2d_t,rn_2d_analyt,
     &  difnorm,difc,difrc,ic,jc,irc,jrc)

        if (n_timestep.le.n_timesteps) then
          difnorm_ar(n_timestep)=difnorm
          difc_ar(N_timestep)=difc
          difrc_ar(n_timestep)=difrc
        endif
c-----------------------------------------------------------------
        !one time step is finished      
      enddo
c---------------------------------------------------------------------
c     solution of the Smoluhovsky equation was finished
c-----------------------------------------------------------------
c     plot contours of numerical n(vx.vy)=const_i ,i=1:10 
c-----------------------------------------------------------------
      call plot_2d_contours_of_rn_2d(Nx,Ny,rn_2d_t,
     &v_x,v_y,t_b)
c-----------------------------------------------------------------
c     plot contours of analytical n(vx.vy)=const_i ,i=1:10 
c-----------------------------------------------------------------
      call plot_2d_contours_of_rn_2d(Nx,Ny,rn_2d_analyt,
     &v_x,v_y,t_b)
c------------------------------------------------------------
c     plot difference between analytical abnd numerical solutions
c     versus timesteps
c----------------------------------------------------------------
      call plot_diference_numer_analyt_solutions
c--------------------------------------------------------
c     close output file plot.ps
c--------------------------------------------------------
      call plotend

      call cpu_time(run_time_2)
      run_time_3=run_time_2-run_time_1

      write(*,100) run_time_3
 100  format('smoluhovski_2d_num_sol.f runtime [sec] ',1pd14.5)

      write(*,*)'Normal end of program'

      stop
      end
      

      subroutine calculate_integral_2d_1(Nx,Ny,i,j,rn_2d,v_x,v_y,
     &r_integral_2d_1)
c--------------------------------------------------------------------
c     Calculate first 2D integral r_integral_2d_1
c     in RHS of the Smolyhovsky equation at a point v_x(i),v),v_y(j)
c--------------------------------------------------------------------
      implicit none
c-----input
      integer 
     &Nx,Ny, !total numbers of 2d mesh points
     &i,j    !indexes  of current point point [v_x(i),v_y(j)]
             !where the integral will be calculated

      real*8 
     &rn_2d(0:Nx,0:Ny), !2D array of numerical concentration n(x,y)    
     &v_x(0:Nx),        !uniform volume mesh  
     &v_y(0:Ny)         !uniform volume mesh
c-----output      
      real*8
     &r_integral_2d_1
c-----externals
      real*8 rK_f   !coagulation kernel

c-----locals
      integer i1,j1
      real*8
     &step_v_x,step_v_y,     !steps of v_x,v_y uniform meshes
     &v_x_l,v_x_l_m,
     &v_y_l,v_y_l_m,
     &v_x_i,v_y_j,
     &delta_v_x,delta_v_x_m, 
     &delta_v_y,delta_v_y_m
   
      step_v_x=v_x(2)-v_x(1)
      step_v_y=v_y(2)-v_y(1)

      v_x_i=v_x(i)
      v_y_j=v_y(j)

c      write(*,*)'in calculate_integral_2d_1'
c      write(*,*)'i,j,v_x_i,v_y_j',i,j,v_x_i,v_y_j
c      write(*,*)'step_v_x,step_v_y',step_v_x,step_v_y

      r_integral_2d_1=0.d0
      if ((i.gt.0).and.(j.gt.0))then
         do i1=1,i 
            v_x_l = v_x(i1)
            delta_v_x = v_x_i-v_x_l 
            v_x_l_m = v_x_l-step_v_x
            delta_v_x_m = v_x_i-v_x_l_m
 
c            write(*,*)'i1,v_x_l,v_x_l_m,delta_v_x,delta_v_x_m',
c     &                 i1,v_x_l,v_x_l_m,delta_v_x,delta_v_x_m

            do j1=1,j
              v_y_l = v_y(j1)
              delta_v_y = v_y_j-v_y_l 
              v_y_l_m = v_y_l-step_v_y
              delta_v_y_m = v_y_j-v_y_l_m 
  
c              write(*,*)'i1,j1',i1,j1
c              write(*,*)'v_y_l,v_y_l_m,delta_v_y,delta_v_y_m',
c     &                   v_y_l,v_y_l_m,delta_v_y,delta_v_y_m
c              write(*,*)'rK_f(delta_v_x,delta_v_y,v_x_l,v_y_l)',
c     &                   rK_f(delta_v_x,delta_v_y,v_x_l,v_y_l)
c              write(*,*)'rK_f(delta_v_x_m,delta_v_y,v_x_l_m,v_y_l)',
c     &                   rK_f(delta_v_x_m,delta_v_y,v_x_l_m,v_y_l)
c              write(*,*)'rK_f(delta_v_x,delta_v_y_m,v_x_l,v_y_l_m)',
c     &                   rK_f(delta_v_x,delta_v_y_m,v_x_l,v_y_l_m)
c             write(*,*)'rK_f(delta_v_x_m,delta_v_y_m,v_x_l_m,v_y_l_m)'
c     &                 ,rK_f(delta_v_x_m,delta_v_y_m,v_x_l_m,v_y_l_m)

              r_integral_2d_1 = r_integral_2d_1+
     &        0.25d0*
     &        (rn_2d(i1,j1)*rn_2d(i-i1,j-j1)*
     &                  rK_f(delta_v_x,delta_v_y,v_x_l,v_y_l)+
     &         rn_2d(i1-1,j1)*rn_2d(i-(i1-1),j-j1)*
     &                  rK_f(delta_v_x_m,delta_v_y,v_x_l_m,v_y_l)+             
     &         rn_2d(i1,j1-1)*rn_2d(i-i1,j-(j1-1))*
     &                  rK_f(delta_v_x,delta_v_y_m,v_x_l,v_y_l_m)+
     &         rn_2d(i1-1,j1-1)*rn_2d(i-(i1-1),j-(j1-1))*
     &                  rK_f(delta_v_x_m,delta_v_y_m,v_x_l_m,v_y_l_m))*
     &        0.5d0*step_v_x*step_v_y     
          
c             write(*,*)'r_integral_2d_1',r_integral_2d_1

             enddo
         enddo
      endif

      return
      end

      subroutine calculate_integral_2d_2(Nx,Ny,i,j,rn_2d,v_x,v_y,
     &r_integral_2d_2)
c--------------------------------------------------------------------
c     Calculate second 2D integral r_integral_2d_2
c     in RHS of the Smolyhovsky equation at a point v_x(i),v),v_y(j)
c--------------------------------------------------------------------
      implicit none
c-----input
      integer
     &Nx,Ny, !total numbers of 2d mesh points
     &i,j    !indexes  of current point point [v_x(i),v_y(j)]
             !where the integral will be calculated
      real*8 
     &rn_2d(0:Nx,0:Ny),  !2D array of numerical concentration n(x,y)
     &v_x(0:Nx),         !uniform volume mesh  
     &v_y(0:Ny)          !uniform volume mesh
 
c-----output      
      real*8
     &r_integral_2d_2
c-----externals
      real*8 rK_f   !coagulation kernel
c-----locals
      integer i1,j1
      real*8
     &step_v_x,step_v_y,     !steps of v_x,v_y uniform meshes
     &v_x_l,v_x_l_m,
     &v_y_l,v_y_l_m,
     &v_x_i,v_y_j

      step_v_x=v_x(2)-v_x(1)
      step_v_y=v_y(2)-v_y(1)
      v_x_i=v_x(i)
      v_y_j=v_y(j)

c      write(*,*)'in calculate_integral_2d_2'
c      write(*,*)'i,j,v_x_i,v_y_j',i,j,v_x_i,v_y_j
c      write(*,*)'step_v_x,step_v_y',step_v_x,step_v_y

      r_integral_2d_2=0.d0
        
      do i1=1,Nx
         v_x_l = v_x(i1) 
         v_x_l_m = v_x_l-step_v_x

c         write(*,*)'i1,v_x_l,v_x_l_m',
c     &              i1,v_x_l,v_x_l_m

         do j1=1,Ny 
            v_y_l = v_y(j1)
            v_y_l_m = v_y_l-step_v_y

c            write(*,*)'i1,j1',i1,j1
c            write(*,*)'v_y_l,v_y_l_m',
c     &                 v_y_l,v_y_l_m
c            write(*,*)'rK_f(v_x_i,v_y_j,v_x_l,v_y_l)',
c     &                 rK_f(v_x_i,v_y_j,v_x_l,v_y_l)
c            write(*,*)'rK_f(v_x_i,v_y_j,v_x_l_m,v_y_l)',
c     &                 rK_f(v_x_i,v_y_j,v_x_l_m,v_y_l)
c            write(*,*)'rK_f(v_x_i,v_y_j,v_x_l,v_y_l_m)',
c     &                 rK_f(v_x_i,v_y_j,v_x_l,v_y_l_m)
c            write(*,*)'rK_f(v_x_i,v_y_j,v_x_l_m,v_y_l_m)',
c     &                 rK_f(v_x_i,v_y_j,v_x_l_m,v_y_l_m)

            r_integral_2d_2=r_integral_2d_2+
     &      0.25d0*(rn_2d(i1,j1)*rK_f(v_x_i,v_y_j,v_x_l,v_y_l)+
     &              rn_2d(i1-1,j1)*rK_f(v_x_i,v_y_j,v_x_l_m,v_y_l)+
     &              rn_2d(i1,j1-1)*rK_f(v_x_i,v_y_j,v_x_l,v_y_l_m)+
     &              rn_2d(i1-1,j1-1)*rK_f(v_x_i,v_y_j,v_x_l_m,v_y_l_m))
     &        *step_v_x*step_v_y

c           write(*,*)'r_integral_2d_2',r_integral_2d_2        

          enddo
      enddo
     
      return
      end

      real*8 function rK_f(v_x1,v_y1,v_x2,v_y2)
c-----------------------------------------------------------------
c     coagulation kernel
c---------------------------------------------------------------- 
c-----------------------------------------------------------
c     declaration and storage of all namelists variables
c-----------------------------------
      use One_NmlMod
      implicit none 
c-----input 
      real*8 
     &v_x1,v_y1, !first  pair of arguments
     &v_x2,v_y2  !second pair of arguments
c-----locals

c-----constant kernel     
      rK_f=rK_0

      return
      end


      subroutine number_of_particle_total_volume(Nx,Ny,v_x,v_y,rn_2d,
     &total_n,total_v)
c---------------------------------------------------------------
c     Calculate total number of particles: total_n,
c     Calculate total volum: total_v,
c--------------------------------------------------------------
      implicit none
c-----input
      integer Nx,Ny !dimensions of volume mesh

      real*8, dimension(0:Ny,0:Ny) :: rn_2d          ! concentration n(vx,vy)
      real*8, dimension(0:Nx) :: v_x                 !v_x mesh
      real*8, dimension(0:Ny) :: v_y                 !v_x mesh
c-----output
      real*8
     &total_n,            !total number of partcles
     &total_v             !total volume

c-----locals
      integer i,j
      real*8 step_x,step_y

      step_x=v_x(1)-v_x(0)
      step_y=v_y(1)-v_y(0)

      total_n=0.d0
      total_v=0.d0

      do i=0,Nx
         do j=0,Ny        
           total_n=total_n+rn_2d(i,j)
           total_v=total_v+rn_2d(i,j)*(v_x(i)+v_y(j))
        enddo
      enddo

      total_n=total_n*step_x*step_y
      total_v=total_v*step_x*step_y

      return
      end


      subroutine initial_concentation_distribution(Nx,Ny,v_x,v_y,
     &v_x_0,v_y_0,rN_0,rn_2d)
c---------------------------------------------------------------
c     Calculate array of initial distribution of concentrsation 
c     rn_2d(0:Nx,0:Ny)=n(nx,vy)
c     at uniform mesh v_x,v_y
c
c     n(vx,vy)=(rN/(vx_0*vy*))*exp(-v_x/v_x_0-v_y/v_y_0)     
c--------------------------------------------------------------
      implicit none
c-----input
      integer Nx,Ny !dimensions of volume mesh
      real*8  v_x_0,v_y_0,rN_0
      real*8, dimension(0:Nx) :: v_x                 !v_x mesh
      real*8, dimension(0:Ny) :: v_y                 !v_x mesh
c-----output
      real*8, dimension(0:Ny,0:Ny) :: rn_2d          ! concentration n(vx,vy)
c-----locals
      integer i,j

c      write(*,*)'initial_concentation_distribution'

c      write(*,*)'Nx,Ny,v_x_0,v_y_0,rN_0',Nx,Ny,v_x_0,v_y_0,rN_0

      do i=0,Nx
        do j=0,Ny
c           write(*,*)'i,j,v_x(i),v_y(j)',i,j,v_x(i),v_y(j)
           rn_2d(i,j)=rN_0/(v_x_0*v_y_0)*
     &                dexp(-v_x(i)/v_x_0-v_y(j)/v_y_0)
    
c           write(*,*)' rn_2d(i,j)',rn_2d(i,j)
        enddo
      enddo

      return
      end


      subroutine difference_numerical_analytical(Nx,Ny,
     &rn_2d_numer,rn_2d_analyt,
     &difnorm,difc,difrc,
     &ic,jc,irc,jrc)
c-----------------------------------------------------------
c     Calculate norms of difference between numerical and
c     analytical solutions
c
c     difnorm=Sum(i[0,Nx])Sumj[0;Ny]
c             |rn_2d_analyt(i,j)-rn_2d_t(i,j)|/(Nx*Ny)
c
c     Calculate point (ic,jc) where the difference
c     difc=|rn_2d_analyt(i,j)-rn_2d_t(i,j)| has maximal value
c     difc it will be error in C norm (maximal difference) 
c
c     Calculte point (icr,jcr) where ratio  has maximal value
c     difrc=|rn_2d_analyt(i,j)-rn_2d_t(i,j)|/|rn_2d_t(i,j)|
c     difcr will be relative error 
c
c     It will print out:
c        t_t,difnorm/(Nx*Ny)
c        ic,jc,difc,rn_2d_analyt(ic,jc),rn_2d_t(ic,jc)
c        irc,jrc,difrc,rn_2d_analyt(irc,jrc),rn_2d_t(irc,jrc)
c---------------------------------------------------------------------
      implicit none     
c-----input
      integer Nx,Ny !dimensions of volume mesh
      real*8, dimension(0:Ny,0:Ny) :: rn_2d_numer  !numerical solution
      real*8, dimension(0:Ny,0:Ny) :: rn_2d_analyt !analytical solution\

c-----output
      real*8
     &difnorm, 
     &difc,
     &difrc
      integer
     &ic,jc,irc,jrc
c-----locals 
      integer i,j

      difnorm=0.d0
      difc=0.d0
      difrc=0.d0

      ic=-1
      jc=-1 
      irc=-1
      jrc=-1

      do i=0,Nx
         do j=0,Ny 
           difnorm=difnorm+dabs(rn_2d_analyt(i,j)-rn_2d_numer(i,j))

           if(dabs(rn_2d_analyt(i,j)-rn_2d_numer(i,j)).gt.difc) then
              difc=dabs(rn_2d_analyt(i,j)-rn_2d_numer(i,j))
              ic=i
              jc=j
           endif

           if(dabs(rn_2d_analyt(i,j)-rn_2d_numer(i,j))/
     &        dabs(rn_2d_numer(i,j)).gt.difrc) then

              difrc=dabs(rn_2d_analyt(i,j)-rn_2d_numer(i,j))/
     &              dabs(rn_2d_numer(i,j))
              irc=i
              jrc=j
           endif
             enddo
      enddo  

      difnorm =difnorm/(Nx*Ny)

      write(*,*)'difnorm',difnorm
      write(*,*)'ic,jc,difc,rn_2d_analyt(ic,jc),rn_2d_numer(ic,jc)',
     &           ic,jc,difc,rn_2d_analyt(ic,jc),rn_2d_numer(ic,jc)

      write(*,*)'irc,jrc,difrc',irc,jrc,difrc
      write(*,*)'rn_2d_analyt(irc,jrc),rn_2d_numer(irc,jrc)',
     &           rn_2d_analyt(irc,jrc),rn_2d_numer(irc,jrc)
      return
      end

      subroutine initial_concentation_distribution_w_x(Nx,Ny,v_x,v_y,
     &v_x_0,v_y_0,rN_0,rn_2d)
c---------------------------------------------------------------
c     Calculate array of initial distribution of concentrsation 
c     rn_2d(0:Nx,0:Ny)=n(nx,vy)
c     at uniform mesh v_x,v_y
c
c     n(vx,vy)=(rN/(vx_0*vy*))*exp(-v_x/v_x_0-v_y/v_y_0)     
c--------------------------------------------------------------
      implicit none
c-----input
      integer Nx,Ny !dimensions of volume mesh
      real*8  v_x_0,v_y_0,rN_0
      real*8, dimension(0:Nx) :: v_x                 !v_x mesh
      real*8, dimension(0:Ny) :: v_y                 !v_x mesh
c-----output
      real*8, dimension(0:Ny,0:Ny) :: rn_2d          ! concentration n(vx,vy)
c-----locals
      integer i,j

      real*8 v_vector(1:2),v0_vector(1:2),v_max_vector(1:2),
     &v_min_vector(1:2) 
      real*8, dimension(0:Ny,0:Ny) :: rn_2d_wx     
c-----externals
      real*8 w_x
     
c      write(*,*)'initial_concentation_distribution_w_x'

c      write(*,*)'Nx,Ny,v_x_0,v_y_0,rN_0',Nx,Ny,v_x_0,v_y_0,rN_0
      v0_vector(1)=v_x_0
      v0_vector(2)=v_y_0

      v_min_vector(1)=0.01d0
      v_min_vector(2)=0.01d0
 
      v_max_vector(1)=15.d0
      v_max_vector(2)=15.d0

      do i=0,Nx
        do j=0,Ny

c           write(*,*)'i,j,v_x(i),v_y(j)',i,j,v_x(i),v_y(j)

           rn_2d(i,j)=0.5d0*rN_0/(v_x_0*v_y_0)*
     &                dexp(-v_x(i)/v_x_0-v_y(j)/v_y_0)
    
           v_vector(1)=v_x(i)
           v_vector(2)=v_y(j)
           if((i.eq.0).and.(j.eq.0)) then 
             rn_2d_wx(i,j)=rn_2d(i,j)
           else
             rn_2d_wx(i,j)=w_x(2,v0_vector, 
     &       v_min_vector,v_max_vector,v_vector)/
     &       (v_vector(1)+v_vector(2))
            endif

c           write(*,*)'rn_2d(i,j),rn_2d_wx(i,j)',
c     &                rn_2d(i,j),rn_2d_wx(i,j)

        enddo
      enddo

c      stop 'in initial_concentation_distribution_w_x'

      return
      end


      subroutine initial_concentation_distribution_w_x_t(Nx,Ny,v_x,v_y,
     &v_x_0,v_y_0,rN_0,rn_2d_wx)
c---------------------------------------------------------------
c     Calculate array of initial distribution of concentrsation 
c     rn_2d(0:Nx,0:Ny)=n(nx,vy)
c     at uniform mesh v_x,v_y
c
c     n(vx,vy)=(rN/(vx_0*vy*))*exp(-v_x/v_x_0-v_y/v_y_0)     
c--------------------------------------------------------------
      implicit none
c-----input
      integer Nx,Ny !dimensions of volume mesh
      real*8  v_x_0,v_y_0,rN_0
      real*8, dimension(0:Nx) :: v_x                 !v_x mesh
      real*8, dimension(0:Ny) :: v_y                 !v_x mesh
c-----output
      real*8, dimension(0:Ny,0:Ny) :: rn_2d          ! concentration n(vx,vy)
c-----locals
      integer i,j

      real*8 v_vector(1:2),v0_vector(1:2),v_max_vector(1:2),
     &v_min_vector(1:2) 
      real*8, dimension(0:Ny,0:Ny) :: rn_2d_wx     
c-----externals
      real*8 w_x_t
     
c      write(*,*)'initial_concentation_distribution_w_x_t'

c      write(*,*)'Nx,Ny,v_x_0,v_y_0,rN_0',Nx,Ny,v_x_0,v_y_0,rN_0
      v0_vector(1)=v_x_0
      v0_vector(2)=v_y_0

      v_min_vector(1)=0.01d0
      v_min_vector(2)=0.01d0
 
      v_max_vector(1)=15.d0
      v_max_vector(2)=15.d0

      do i=0,Nx
        do j=0,Ny

           write(*,*)'i,j,v_x(i),v_y(j)',i,j,v_x(i),v_y(j)

           rn_2d(i,j)=0.5d0*rN_0/(v_x_0*v_y_0)*
     &                dexp(-v_x(i)/v_x_0-v_y(j)/v_y_0)
    
           v_vector(1)=v_x(i)
           v_vector(2)=v_y(j)
           rn_2d_wx(i,j)=w_x_t(2,v0_vector, 
     &     v_min_vector,v_max_vector,v_vector)/
     &     (v_vector(1)+v_vector(2))

c           if((i.eq.0).and.(j.eq.0)) then 
c             rn_2d_wx_t(i,j)=rn_2d(i,j)
c           else
c             rn_2d_wx(i,j)=w_x_t(2,v0_vector, 
c     &       v_min_vector,v_max_vector,v_vector)/
c     &       (v_vector(1)+v_vector(2))
c            endif

           write(*,*)'rn_2d(i,j),rn_2d_wx(i,j)',
     &                rn_2d(i,j),rn_2d_wx(i,j)

        enddo
      enddo

c      stop 'in initial_concentation_distribution_w_x'

      return
      end

      subroutine dif_norm_l2(Nx,Ny,
     &rn_2d_numer,rn_2d_analyt,v_c_1_2d_mesh,v_c_2_2d_mesh,
     &difnorm_l2)
c----------------------------------------------------------
c     Calculate l2 norm difnorm_l2 of difference between numerical and
c     analytical solution
c---------------------------------------------------------- 
      implicit none     
c-----input      
      integer  ::
     & Nx,Ny      !dimensions of volume mesh
      real*8, dimension(0:Nx,0:Ny) :: rn_2d_numer  !numerical solution
      real*8, dimension(0:Nx,0:Ny) :: rn_2d_analyt !analytical solution
      real*8, dimension(0:Nx) :: v_c_1_2d_mesh  !v mesh in first dimention
      real*8, dimension(0:Ny) :: v_c_2_2d_mesh  !v mesh in second dimention
c-----locals
      integer :: k1,k2,j
      real*8  ::
     &difnorm_l2,stepx,stepy

      stepx=v_c_1_2d_mesh(2)-v_c_1_2d_mesh(1)
      stepy=v_c_2_2d_mesh(2)-v_c_2_2d_mesh(1)

      difnorm_l2 =0.d0
      do k1=0,Nx
         do k2=0,Ny
            difnorm_l2 = difnorm_l2 +  
     &      (rn_2d_numer(k1,k2)-rn_2d_numer(k1,k2))**2
         enddo
      enddo
      
      difnorm_l2=dsqrt(difnorm_l2*stepx*stepy)

      write(*,*)' difnorm_l2', difnorm_l2


      return
      end

