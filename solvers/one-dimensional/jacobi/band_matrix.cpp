#include "band_matrix.hpp"
#include <mkl.h>

BandMatrix::BandMatrix(const double *vals, size_t N, size_t width)
  : a(vals, vals + width * N), n(N), k(width - 1) {}

void BandMatrix::apply(double *x, bool inverse) const {
  if (inverse) {
    cblas_dtbsv(CblasColMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, k,
                a.data(), k + 1, x, 1);
  } else {
    cblas_dtbmv(CblasColMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, k,
                a.data(), k + 1, x, 1);
  }
}

