      MODULE NamelistMod

      use One_NmlMod
c-------------------------------------------------------------
c     namelist /numericle_nml/
c------------------------------------------------------------- 
      namelist /numericle_nml/
     &t_max,                       !maximal time
     &tau,                         !time step
     &v_x_max,v_y_max,             !Maximum particle volumes 
     &h_plot,                      !time step for plots-
     &Nx,                          !number of volume mesh points
     &Ny,                          !number of volume mesh points
     &n_vol_plot,                  !number of mesh points 
                                   !for sectional distributions (0:n_vol_plot)
     &mnemonic,                    !it is used for naming the data output files:
                                   ! mnemonic.txt
     &N_particle                   !number of MC particles
c    -------------------------------------------
c     namelist /physical_nml/
c------------------------------------------------------------- 
      namelist /physical_nml/
     &v_x_0,v_y_0,                 !Overage initial volumes
     &rK_0,                        !Kernal
     &rN_0,                        !Number of particles 
     &N_species,                    !number of species
     &i_coag_type                  !type of kernel 1 const, 2 brounian

c-------------------------------------------------------------------------
      namelist /species_v0_vector_nml/
     &prof                              !v0_vector(N_species)        
c-------------------------------------------------------------------------
      namelist /species_v_particle_min_vector_nml/
     &prof                              !v_particle_min_vector(N_species)    
c-------------------------------------------------------------------------
      namelist /species_v_particle_max_vector_nml/
     &prof                              !v_particle_max_vector(N_species)    

      END MODULE NamelistMod
