#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<ctime>
#include<complex>

#include"../include/gsmol_cp_blas.h"
#include"../include/gsmol_cp_kernel.h"

//============================================================================//
//    Main                                                                    //
//============================================================================//
int main(
    int argc,
    char ** argv
) {
    if (argc < 9)
    {
        printf(
            "================================================================================\n"
        );

        printf(
            "Required arguments:\n (1) kernel_name\n (2) collisions_type\n (3) dimensionality\n (4) mode_size\n"
        );

        printf(
            " (5) number_iterations\n (6) dt\n (7) filename_time\n (8) filename_result\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: kernel_name == {const, sumsqrt, genprod}\n"
        );

        printf(
            "Tips: kernel_name := const, otherwise\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: collisions_type == all   --   all collisions till dimensionality\n"
        );

        printf(
            "Tips: collisions_type != all   --   only dimensionality-ary collisions\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: './log' file to be appended if present, to be generated otherwise\n"
        );

        printf(
            "================================================================================\n"
        );

        return 1;
    }

    int D;
    int N;
    double dt;
    int T;

    // argv parsing
    sscanf(argv[3], "%d", &D);
    sscanf(argv[4], "%d", &N);
    sscanf(argv[5], "%d", &T);
    sscanf(argv[6], "%lf", &dt);

    if (D < 2 || N < 1 || T < 1)
        return 1;

    // default weights
    double alpha[D - 1];

    for (int a = 0; a < D - 1; ++a)
        alpha[a] = 1.;

    // kernel initialization
    TKernel_CP * kernel = new TKernel_CP(argv[1], argv[2], D, N, alpha);

    const int size = N << 1;

    // auxiliary allocation
    double * u = (double *)malloc(size * sizeof(double));
    double * v = (double *)malloc(size * sizeof(double));

    //======================================//
    //    Monodisperse initial condition    //
    //======================================//
    double * n = (double *)calloc(size, sizeof(double));
    n[0] = 1.;

    //===========================//
    //    Predictor-corrector    //
    //===========================//
    clock_t t = clock();
    for (int k = 0; k < T; ++k) 
    {
        kernel->compute(n, u);
        
        // u *= 0.5 * dt
        scal(N, 0.5 * dt, u, 1);
        // u += n
        axpy(N, 1., n, 1, u, 1);

        kernel->compute(u, v);

        // n += dt * v
        axpy(N, dt, v, 1, n, 1);
    }
    t = clock() - t;

    //==========================//
    //    Write times >> log    //
    //==========================//
    FILE * ft = fopen("log", "a");

    fprintf(
        ft, "\ns %s %d-%s %d\nt: %d %.8f %d\ntime: %.8f\n", argv[1], D,
        (strcmp(argv[2], "all"))? "only": "all", N, (int)rint(dt * T), dt, T,
        ((double)t)/CLOCKS_PER_SEC
    );

    fclose(ft);

    //=============================//
    //    Write times > argv[7]    //
    //=============================//
    ft = fopen(argv[7], "a");

    fprintf(ft, "\n%d-%s %d\ntime: %.8f\n", D,
        (strcmp(argv[2], "all"))? "only": "all", N, ((double)t)/CLOCKS_PER_SEC
    );

    fclose(ft);

    //===============================//
    //    Write results > argv[8]    //
    //===============================//
    ft = fopen(argv[8], "w");

    for (int i = 0; i < N; ++i)
        fprintf(ft, "%d %.15f\n", i, n[i]);

    fclose(ft);

    //=========================//
    //    Deallocate memory    //
    //=========================//
    delete kernel;

    free(n);
    free(u);
    free(v);

    return 0;
}
 
