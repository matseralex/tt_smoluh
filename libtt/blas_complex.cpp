#include "blas.h"
#include <iostream>
using namespace std;

void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const complex<double> alpha, const complex<double> *A, const int lda, const complex<double> *X, const int incX, const complex<double> beta, complex<double> *Y, const int incY)
{
    cblas_zgemv(order, TransA, M, N, &alpha, A, lda, X, incX, &beta, Y, incY);
    return;
}
