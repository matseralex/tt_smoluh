#include <omp.h>
#include <cstring>
#include <cmath>
#include <sys/time.h>

#include "gsmol_kernel.h"
#include "gsmol_tt_kernel.h"

//============================================================================//
//    Default constructor                                                     //
//============================================================================//
TKernel_TT::TKernel_TT(
    void
):
    D(0),
    A(0),
    N(0),
    size(0),
    R(0),
    mult(NULL),
    koffs(NULL),
    coffs(NULL),
    loads(NULL),
    K(0),
    kers_tt(NULL),
    kdata(NULL),
    H(0),
    kbounds(NULL),
    mbounds(NULL),
    fdescs(NULL),
    bdesc(NULL),
    X(0),
    aux(NULL),
    faux(NULL),
    ranks_appr(NULL),
    t_appr(0),
    t_compress(0)
{}

//============================================================================//
//    Kernels initialization                                                  //
//============================================================================//
TKernel_TT::TKernel_TT(
    const char * kname,
    // in: type of collisions
    const char * ktype,
    // in: kernels maximal dimension
    const int & kdim,
    // in: quantity of equations
    const int & kmode,
    // in: weights in front of kernels
    const double * kw,
    // in: tolerance of approximation
    const double & ktol
):
    D(kdim),
    A((!strcmp(ktype, "all"))? kdim - 1: 1),
    N(kmode),
    size(2 * kmode),
    R(1),
    K(0),
    kdata(NULL)
{
    kers_tt = (TTensorTrain **)malloc(A * sizeof(TTensorTrain *));
    mult = (double *)malloc(A * sizeof(double));
    koffs = (int *)malloc(A * sizeof(int));
    coffs = (int **)malloc(A * sizeof(int *));
    loads = (int **)malloc(A * sizeof(int *));
    ranks_appr = (int *)malloc((D + 1) * sizeof(int)); 

    const double dzero = 0.;

    // initialize kernels multipliers 
    double factor = 0.5;

    mult[0] = (A == 1)? kw[0] / (double)fact(D): factor * kw[0];
   
    for (int a = 1; a < A; ++a)
    {
        factor /= (double)(a + 2);
        mult[a] = factor * kw[a];
    }

    koffs[0] = 0;

    int dim;
    int * ranks;
    double ** cars;
    int ksize;

    //=================================//
    //    TT-kernels initialization    //
    //=================================//
    for (int a = 0; a < A; ++a)
    {
        dim = (a < A - 1)? a + 2: D;

        kers_tt[a] = new TTensorTrain();

        int * modes = (int *)malloc(dim * sizeof(int));
        modes[0] = N;
        copy(dim, modes, 0, modes, 1); 

        // construct (plain) kernel
        TKernel * ker = new TKernel(dim, modes, kname);

        // initialize parameters for TT-cross approximation 
        TTensorTrainParameters pars;

        // convergence tolerance
        pars.tolerance = ktol;
        // number of iterations
        // if zero then iterates until convergence
        pars.maximal_iterations_number = 0;

        // TT-cross approximation
        t_appr = omp_get_wtime();
        (kers_tt[a])->Approximate(ker, pars);
        t_appr = omp_get_wtime() - t_appr;

        printf(
            "Approximation finished N = %d, d = %d, t = %.2f\n", N, dim, t_appr
        );

        // save approximation ranks of the highest TT-kernel
        if (a == A - 1)
        {
            copy(D + 1, (kers_tt[A - 1])->ranks, 1, ranks_appr, 1);
        }

        // SVD-compression
        // unused for now
        t_compress = 0;
        // t_compress = omp_get_wtime();
        // (kers_tt[a])->SVDCompress(parameters.tolerance / 1.);
        // t_compress = omp_get_wtime() - t_compress;

        // delete (plain) kernel
        delete ker;

        coffs[a] = (int *)malloc(dim * sizeof(int));
        loads[a] = (int *)malloc(dim * sizeof(int));

        ranks = (kers_tt[a])->ranks;
        cars = (kers_tt[a])->carriages;
        ksize = 0;

        for (int d = 0; d < dim; ++d)
        {
            // update max rank
            if (R < ranks[d])
            {
                R = ranks[d];
            }

            coffs[a][d] = ksize;
            loads[a][d] = ranks[d] * ranks[d + 1];
            ksize += loads[a][d];
        }

        kdata = (double *)realloc(kdata, (K + ksize) * N * sizeof(double));

        for (int d = 0; d < dim; ++d)
        {
#pragma omp parallel for collapse(2)
            for (int r = 0; r < loads[a][d]; ++r)
            {
                for (int i = 0; i < N; ++i)
                {
                    kdata[(K + r) * N + i] = cars[d][r + i * loads[a][d]];
                }
            }

            free(cars[d]);
            (kers_tt[a])->carriages[d] = NULL;//kdata + K * N;

            K += loads[a][d];
        }

        if (a < A - 1)
        {
            koffs[a + 1] = koffs[a] + ksize;
        }
    }

    H = omp_get_max_threads();
    //if (H > K)
    //{
    //    H = K;
    //}

    kbounds = (int *)malloc((H + 1) * sizeof(int));
    kbounds[H] = K;

    mbounds = (int *)malloc((H + 1) * sizeof(int));
    mbounds[H] = N + 1;

    fdescs = (DFTI_DESCRIPTOR_HANDLE *)malloc(
        H * sizeof(DFTI_DESCRIPTOR_HANDLE)
    );

    X = K + 2 * (R + H * N) + size;

    aux = (double *)malloc((X + N) * sizeof(double));

    faux = (std::complex<double> *)mkl_malloc(
        ((K + 1) * (N + 1) + 2 * H * R) * sizeof(std::complex<double>), 64
    );

    MKL_LONG status;
    MKL_LONG is[2];
    MKL_LONG os[2];
    is[0] = 0;
    is[1] = 1;
    os[0] = 0;
    os[1] = K;
    
    //printf("H = %d\n", H);

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        kbounds[h] = ((K - 1) / omp_get_max_threads() + 1) * h;

        if (kbounds[h] > K)
        {
            kbounds[h] = K;
        }

        mbounds[h] = (N / omp_get_max_threads() + 1) * h;

        if (mbounds[h] > N + 1)
        {
            mbounds[h] = N + 1;
        }

        //printf("kbounds[%d] = %d\n", h, kbounds[h]);

        copy(N, &dzero, 0, aux + K + 2 * R + (2 * h + 1) * N, 1);

        // DFTI descriptor handler initialization
        status = DftiCreateDescriptor(
            fdescs + h, DFTI_DOUBLE, DFTI_REAL, 1, size
        );
        status = DftiSetValue(
            fdescs[h], DFTI_CONJUGATE_EVEN_STORAGE, DFTI_COMPLEX_COMPLEX
        );
        status = DftiSetValue(fdescs[h], DFTI_PLACEMENT, DFTI_NOT_INPLACE);
        status = DftiSetValue(fdescs[h], DFTI_INPUT_STRIDES, is);
        status = DftiSetValue(fdescs[h], DFTI_OUTPUT_STRIDES, os);
        status = DftiSetValue(
            fdescs[h], DFTI_ORDERING, DFTI_BACKWARD_SCRAMBLED
        );
        status = DftiSetValue(
            fdescs[h], DFTI_BACKWARD_SCALE, 1. / (double)size
        );

        status = DftiCommitDescriptor(fdescs[h]);

        status = DftiComputeForward(fdescs[h], aux + h * size, faux + h);
        // printf("after h = %d, thread = %d\n", h, omp_get_thread_num());
    }

    status = DftiCopyDescriptor(fdescs[0], &bdesc);
    status = DftiSetValue(bdesc, DFTI_OUTPUT_STRIDES, is);
    status = DftiCommitDescriptor(bdesc);
    //std::cout << status << std::endl << DftiErrorMessage(status) << std::endl;

    status = DftiComputeBackward(bdesc, faux, aux + X - size);
    //std::cout << status << std::endl << DftiErrorMessage(status) << std::endl;

    // printf("BEGIN\n");
    return;
}

//============================================================================//
//    Kernels deallocation                                                    //
//============================================================================//
TKernel_TT::~TKernel_TT(
    void
) {
    if (mult)
    {
        free(mult);
    }

#pragma omp parallel for 
    for (int a = 0; a < A; ++a)
    {
        delete kers_tt[a];

        free(coffs[a]);
        free(loads[a]);
    }

    if (coffs)
    {
        free(coffs);
    }

    if (loads)
    {
        free(loads);
    }

    if (koffs)
    {
        free(koffs);
    }

    if (kers_tt)
    {
        free(kers_tt);
    }

    if (kbounds)
    {
        free(kbounds);
    }

    if (mbounds)
    {
        free(mbounds);
    }

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        DftiFreeDescriptor(fdescs + h);
    }

    // printf("~\n");
    DftiFreeDescriptor(&bdesc);

    if (fdescs)
    {
        free(fdescs);
    }

    if (kdata)
    {
        free(kdata);
    }

    if (aux)
    {
        free(aux);
    }

    if (faux)
    {
        mkl_free(faux);
    }

    if (ranks_appr)
    {
        free(ranks_appr);
    }

    return;
}

//============================================================================//
//    Income part of Smoluchowski operator                                    //
//============================================================================//
void TKernel_TT::gconv(
    // in:
    const double * arg,
    // out: income part of S(arg)
    double * res
) {
    int status;
    const double dzero = 0.;
    const std::complex<double> zzero(dzero, dzero);
    const std::complex<double> zone(1., dzero);

    int dim;
    int * ranks;

    double * ukdata = aux + K + 2 * R;
    std::complex<double> * tmp = faux + K * (N + 1) + 2 * H * R;

    copy(N, &dzero, 0, res, 1);

    //========================//
    //    Carriages update    //
    //========================//
#pragma omp parallel
    {
        int h = omp_get_thread_num();

        //printf("h = %d, l = %d, r = %d\n", h, kbounds[h], kbounds[h + 1]);

        for (int k = kbounds[h]; k < kbounds[h + 1]; ++k)
        {
            // int addr = (r % ranks[d + 1]) * ranks[d] + r / ranks[d + 1];

            for (int i = 0; i < N; ++i)
            {
                ukdata[h * size + i] = kdata[k * N + i] * arg[i];
            }
             
            status = DftiComputeForward(
                fdescs[h], ukdata + h * size, faux + k
            );
        }
    }
    //printf("START\n");

    for (int a = 0; a < A; ++a)
    {
        dim = (kers_tt[a])->dimensionality;
        ranks = (kers_tt[a])->ranks;

#pragma omp parallel
        {
            int h = omp_get_thread_num();

            std::complex<double> * prod1 = faux + K * (N + 1) + R * h;
            std::complex<double> * prod2 = prod1 + R * H;
            std::complex<double> * rucar;

            for (int i = mbounds[h]; i < mbounds[h + 1]; ++i)
            {
                rucar = faux + koffs[a] + coffs[a][dim - 1] + i * K;

                for (int d = dim - 2; d >= 0; --d)
                {
                //printf("BBBBBBBBBB\n");
                //printf("h = %d\n", h);

                    //==========================================//
                    //    Product along right rank dimension    //
                    //==========================================//
                    // prod = lucar · rucar
                    gemv(
                        CblasColMajor, CblasTrans,
                        ranks[d + 1], ranks[d], zone,
                        faux + koffs[a] + coffs[a][d] + i * K, ranks[d + 1],
                        rucar, 1,
                        zzero, prod1, 1
                    );

                    rucar = prod1;
                    prod1 = prod2;
                    prod2 = rucar;
                }

                tmp[i] = rucar[0];
                //if (!i)
                //{
                //    std::cout << faux[koffs[a] + coffs[a][dim - 1] + i * K]
                //        << endl << faux[koffs[a] + coffs[a][0] + i * K]
                //        << endl
                //        << tmp[i]
                //        << endl;
                //}
                //printf("AAAAAAAAAA\n");
            }
        }

        status = DftiComputeBackward(bdesc, tmp, aux + X - size);

        //std::cout << DftiErrorMessage(status) << endl;
        //    std::cout << "i = " << i << tmp[i] << endl << aux[i] << endl;

        axpy(N - dim + 1, mult[a], aux + X - size, 1, res + dim - 1, 1);

        //for (int k = 0; k < 10; ++k)
        //    printf("res[%d] = %.8f\n", k, res[k]);
        //printf("\n");
    }

    // printf("FINISH\n");
    return;
}

//============================================================================//
//    Outcome part of Smoluchowski operator                                   //
//============================================================================//
void TKernel_TT::gmatvec(
    // in:
    const double * arg,
    // out: outcome part of S(arg)
    double * res
) {
    const double dzero = 0.;
    const double done = 1.;

    int dim;
    int * ranks;

    // aux ~ K
    // prod1 ~ R * N
    // prod2 ~ R * N
    double * prod1 = aux + K;
    double * prod2 = prod1 + R;
    double * rucar;

    copy(N, &dzero, 0, res, 1);

    for (int a = 0; a < A; ++a)
    {
        dim = (kers_tt[a])->dimensionality;
        ranks = (kers_tt[a])->ranks;

        //printf("a = %d, ranks[1] = %d, coffs[a][1] = %d\n", a, ranks[1], coffs[a][1]);

        //========================//
        //    Carriages update    //
        //========================//
        // aux = kdata^T · arg

        //printf("a = %d, length = %d, K = %d, koffs[a] = %d\n", a, coffs[a][dim - 1], K, koffs[a]);
        //printf("a = %d, coffs[a][dim - 1] - ranks[1] + ranks[dim - 1] = %d\n", a, coffs[a][dim - 1] - ranks[1] + ranks[dim - 1]);
        gemv(
            CblasColMajor, CblasTrans,
            N, coffs[a][dim - 1] - ranks[1] + ranks[dim - 1], done,
            kdata + (koffs[a] + ranks[1]) * N, N,
            arg, 1,
            dzero, aux + koffs[a] + ranks[1], 1
        );

        rucar = aux + koffs[a] + coffs[a][dim - 1];

        for (int d = dim - 2; d > 0; --d)
        {
            //printf("AAAAAAAAAA\n");
            //==========================================//
            //    Product along right rank dimension    //
            //==========================================//
            // prod = lucar · rucar
            gemv(
                CblasColMajor, CblasTrans,
                ranks[d + 1], ranks[d], done,
                aux + koffs[a] + coffs[a][d], ranks[d + 1],
                rucar, 1,
                dzero, prod1, 1
            );

            rucar = prod1;
            prod1 = prod2;
            prod2 = rucar;
        }

        gemv(
            CblasColMajor, CblasNoTrans,
            N, ranks[1], -dim * mult[a],
            kdata + koffs[a] * N, N,
            rucar, 1,
            done, res, 1
        );

        //printf("-dim * mult[a] = %.3f\n", -dim * mult[a]);
    }

#pragma omp parallel for
    for (int i = 0; i < N; ++i)
    {
        res[i] *= arg[i];
    }
    //printf("FINISH\n");

    return;
}

//============================================================================//
//    Smoluchowski operator                                                   //
//============================================================================//
void TKernel_TT::compute(
    // in:
    const double * arg,
    // out: S(arg)
    double * res
) {
    const double done = 1.;
    double * tmp = aux + X;

    this->gconv(arg, res);
    this->gmatvec(arg, tmp);
    //this->gmatvec(arg, res);

    axpy(N, done, tmp, 1, res, 1);
    /// debug /// copy(N, arg, 1, res, 1);

    // printf("COMPUTE\n");
    return;
}
