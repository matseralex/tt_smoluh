#include "parallel_cross.h"

MPI_Datatype Send_Info_Type;
void TCross_Parallel_v1::get_diff_column(const int &j, const TCross_Parallel_v1_Work_Data & work_data, double *&result)
{
    int i;
    #pragma omp parallel for
    for (i = ((int) 0) ; i < mpi_column_num; i++)
    {
        if (!work_data.I[i])
        {
            result[i] = work_data.matrix->value(mpi_column_start + i, j);
        }
        else
        {
            result[i] = ((double) 0.0);
        }
    }
    #pragma omp barrier 
    #pragma omp parallel for
    for (i = ((int) 0) ; i < work_data.omp_column_threads_num; i++)
    {
        copy(work_data.omp_column_num[i], result + work_data.omp_column_start[i], ((int) 1), C + this->rank*mpi_column_num + work_data.omp_column_start[i], ((int) 1));
    }
    #pragma omp barrier 
    #pragma omp parallel for
    for (i = ((int) 0) ; i < work_data.omp_column_threads_num; i++)
    {
        gemv(CblasColMajor, CblasNoTrans, work_data.omp_column_num[i], this->rank, - ((double) 1.0), U + work_data.omp_column_start[i], mpi_column_num, work_data.vec, ((int) 1), ((double) 1.0), result + work_data.omp_column_start[i], ((int) 1));
    }
    #pragma omp barrier 
    return;
}

void TCross_Parallel_v1::get_diff_row(const int &i, const TCross_Parallel_v1_Work_Data  & work_data, double *&result)
{
    int j;
    #pragma omp parallel for
    for (j = ((int) 0) ; j < mpi_row_num; j++)
    {
        if (!work_data.J[j])
        {
            result[j] = work_data.matrix->value(i, j + mpi_row_start);
        }
        else
        {
            result[j] = ((double) 0.0);
        }
    }
    #pragma omp barrier 
    #pragma omp parallel for
    for (j = ((int) 0) ; j < work_data.omp_row_threads_num; j++)
    {
        copy(work_data.omp_row_num[j], result + work_data.omp_row_start[j], ((int) 1), RT + this->rank*mpi_row_num+ work_data.omp_row_start[j], ((int) 1));
    }
    #pragma omp barrier 
    #pragma omp parallel for
    for (j = ((int) 0) ; j < work_data.omp_row_threads_num; j++)
    {
        gemv(CblasColMajor, CblasNoTrans, work_data.omp_row_num[j], this->rank, - ((double) 1.0), V + work_data.omp_row_start[j], mpi_row_num, work_data.vec, ((int) 1), ((double) 1.0), result + work_data.omp_row_start[j], ((int) 1));
    }
    #pragma omp barrier
    return;
}


TCross_Parallel_v1_Work_Data::TCross_Parallel_v1_Work_Data(TMatrix *original_matrix, TMatrix *approximated_matrix): max_volume(((double) 1.0)), work_matrix(original_matrix, approximated_matrix)
{
    I = NULL;
    J = NULL;
    global_max = ((double) 0.0);
    matrix = original_matrix;
    int len[3] = {1, 1, 1};
    MPI_Aint pos[3] = {offsetof(Send_Info, number), offsetof(Send_Info, volume), sizeof(Send_Info)};
    MPI_Datatype typ[3] = {MPI_INT, MPI_DOUBLE, MPI_UB};
    MPI_Type_struct( 3, len, pos, typ, &Send_Info_Type );
    MPI_Type_commit( &Send_Info_Type );
}

TCross_Parallel_v1_Work_Data::~TCross_Parallel_v1_Work_Data()
{
    free(I);
    free(J);
    free(current_row);
    free(current_column);
    free(omp_column_start);
    free(omp_column_num);
    free(omp_row_start);
    free(omp_row_num);
    free(vec);
}

void TCross_Parallel_v1 ::Prepare_Data(TCross_Parallel_v1_Work_Data  &work_data,  const TCross_Parallel_v1_Parameters  & parameters)
{
    norm = 0;
    MPI_Comm_size(parameters.communicator, &row_comm_size);
    column_comm_size = row_comm_size;
    MPI_Comm_rank(parameters.communicator, &processor_rank);
    mpi_avg_column = (this->get_rows_number() - ((int) 1)) / column_comm_size + ((int) 1);
    mpi_avg_row = (this->get_columns_number() - ((int) 1)) / row_comm_size + ((int) 1);
    row_comm_size = (this->get_columns_number() - ((int) 1)) / mpi_avg_row + ((int) 1);
    MPI_Comm_split(parameters.communicator, processor_rank < row_comm_size, processor_rank, &row_comm);
    column_comm_size = (this->get_rows_number() - ((int) 1)) / mpi_avg_column + ((int) 1);
    MPI_Comm_split(parameters.communicator, processor_rank < column_comm_size, processor_rank, &column_comm);
    if (processor_rank < column_comm_size - 1)
    {
        mpi_column_start = mpi_avg_column*processor_rank;
        mpi_column_num = mpi_avg_column;
    }
    else if (processor_rank == column_comm_size - 1)
    {
        mpi_column_start = mpi_avg_column*processor_rank;
        mpi_column_num = this->get_rows_number() - mpi_column_start;
    }
    else
    {
        mpi_column_start = ((int) 0) ;
        mpi_column_num = ((int) 0) ;
    }
    if (processor_rank < row_comm_size - 1)
    {
        mpi_row_start = mpi_avg_row*processor_rank;
        mpi_row_num = mpi_avg_row;
    }
    else if (processor_rank == row_comm_size - 1)
    {
        mpi_row_start = mpi_avg_row*processor_rank;
        mpi_row_num = this->get_columns_number() - mpi_row_start;
    }
    else
    {
        mpi_row_start = ((int) 0) ;
        mpi_row_num = ((int) 0) ;
    }
    if (processor_rank != 0)
    {
        work_data.I = (bool *) malloc(mpi_column_num*sizeof(bool));
        #pragma omp parallel for
        for (int i = ((int) 0) ; i < mpi_column_num; i++) work_data.I[i] = false;
        work_data.J = (bool *) malloc(mpi_row_num*sizeof(bool));
        #pragma omp parallel for
        for (int j = ((int) 0) ; j < mpi_row_num; j++) work_data.J[j] = false;
    }
    else
    {
        work_data.I = (bool *) malloc(this->get_rows_number() * sizeof(bool));
        #pragma omp parallel for
        for (int i = ((int) 0) ; i < this->get_rows_number(); i++) work_data.I[i] = false;
        work_data.J = (bool *) malloc(this->get_columns_number() * sizeof(bool));
        #pragma omp parallel for
        for (int j = ((int) 0) ; j < this->get_columns_number(); j++) work_data.J[j] = false;
    }
    work_data.omp_column_threads_num = omp_get_max_threads();
    work_data.omp_row_threads_num = work_data.omp_column_threads_num;
    int avg_column = (mpi_column_num - ((int) 1)) / work_data.omp_column_threads_num + ((int) 1);
    int avg_row = (mpi_row_num - ((int) 1)) / work_data.omp_row_threads_num + ((int) 1);
    work_data.omp_column_threads_num = (mpi_column_num - ((int) 1)) / avg_column + ((int) 1);
    work_data.omp_row_threads_num = (mpi_row_num - ((int) 1)) / avg_row + ((int) 1);
    work_data.omp_column_start = (int *) malloc(work_data.omp_column_threads_num * sizeof(int));
    work_data.omp_column_num = (int *) malloc(work_data.omp_column_threads_num * sizeof(int));
    work_data.omp_row_start = (int *) malloc(work_data.omp_row_threads_num * sizeof(int));
    work_data.omp_row_num = (int *) malloc(work_data.omp_row_threads_num * sizeof(int));
    work_data.omp_column_start[((int) 0) ] = ((int) 0) ;
    work_data.omp_row_start[((int) 0) ] = ((int) 0) ;
    for (int i = ((int) 1); i < work_data.omp_column_threads_num; i++)
    {
        work_data.omp_column_start[i] = work_data.omp_column_start[i - ((int) 1)] + avg_column;
        work_data.omp_column_num[i - ((int) 1)] = avg_column;
    }
    work_data.omp_column_num[work_data.omp_column_threads_num - ((int) 1)] = mpi_column_num - work_data.omp_column_start[work_data.omp_column_threads_num - ((int) 1)];
    for (int i = ((int) 1); i < work_data.omp_row_threads_num; i++)
    {
        work_data.omp_row_start[i] = work_data.omp_row_start[i - ((int) 1)] + avg_row;
        work_data.omp_row_num[i - ((int) 1)] = avg_row;
    }
    work_data.omp_row_num[work_data.omp_row_threads_num - ((int) 1)] = mpi_row_num - work_data.omp_row_start[work_data.omp_row_threads_num - ((int) 1)];
    free(U);
    free(V);
    free(C);
    free(hat_A_inv);
    free(RT);
    U = NULL;
    V = NULL;
    C = NULL;
    hat_A_inv = NULL;
    RT = NULL;
    free(rows_numbers);
    free(columns_numbers);
    rows_numbers = NULL;
    columns_numbers = NULL;
    tolerance = parameters.tolerance;
    work_data.parameters = parameters;
    if (work_data.parameters.max_rank <= ((int) 0) )
    {
        work_data.parameters.max_rank = ((int) 1);
    }
    if ((work_data.parameters.rank_increase) <= ((int) 0)  && (work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = ((int) 1);
    }
    else if ((work_data.parameters.rank_increase) <= ((int) 1) && (!work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = ((int) 1) + ((int) 1);
    }
    #pragma omp parallel sections
    {
        #pragma omp section
            U = (double *) malloc(work_data.parameters.max_rank * mpi_column_num * sizeof(double));
        #pragma omp section
            C = (double *) malloc(work_data.parameters.max_rank * mpi_column_num * sizeof(double));
        #pragma omp section
            V = (double *) malloc(work_data.parameters.max_rank * mpi_row_num * sizeof(double));
        #pragma omp section
            RT = (double *) malloc(work_data.parameters.max_rank * mpi_row_num * sizeof(double));
        #pragma omp section
            rows_numbers = (int *) malloc(work_data.parameters.max_rank*sizeof(int));
        #pragma omp section
            columns_numbers = (int *) malloc(work_data.parameters.max_rank*sizeof(int));
        #pragma omp section
            work_data.vec = (double *) malloc(work_data.parameters.max_rank*sizeof(double));
    }
    this->rank = ((int) 0) ;
}

TCross_Parallel_v1 ::TCross_Parallel_v1(): TCross_Base<TCross_Parallel_v1_Work_Data , TCross_Parallel_v1_Parameters>()
{
    U = NULL;
    V = NULL;
    C = NULL;
    hat_A_inv = NULL;
    RT = NULL;
    rows_numbers = NULL;
    columns_numbers = NULL;
    this->rank = ((int) 0) ;
};

TCross_Parallel_v1 ::~TCross_Parallel_v1()
{
    free(U);
    free(V);
    free(C);
    free(hat_A_inv);
    free(RT);
    free(rows_numbers);
    free(columns_numbers);
};

double TCross_Parallel_v1 ::value(const int &i, const int &j)
{
    return dot_u(this->rank, U + i, this->get_rows_number(), V + j, this->get_columns_number());
}

void TCross_Parallel_v1 ::Search_Max_Volume(TCross_Parallel_v1_Work_Data  &work_data)
{
    if (this->rank == min(this->get_rows_number(), this->get_columns_number())) return;
    if (this->rank >= work_data.parameters.max_rank)
    {
        if (work_data.parameters.memory_strategy)
        {
            work_data.parameters.max_rank += work_data.parameters.rank_increase;
        }
        else
        {
            work_data.parameters.max_rank *= work_data.parameters.rank_increase;
        }
        #pragma omp parallel sections
        {
            #pragma omp section
                U = (double *) realloc(U, work_data.parameters.max_rank * mpi_column_num * sizeof(double));
            #pragma omp section
                C = (double *) realloc(C, work_data.parameters.max_rank * mpi_column_num * sizeof(double));
            #pragma omp section
                V = (double *) realloc(V, work_data.parameters.max_rank * mpi_row_num * sizeof(double));
            #pragma omp section
                RT = (double *) realloc(RT, work_data.parameters.max_rank * mpi_row_num * sizeof(double));
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
            #pragma omp section
                work_data.vec = (double *) realloc(work_data.vec, work_data.parameters.max_rank*sizeof(double));
        }
        #pragma omp barrier	
    }
    work_data.current_column = U + this->rank * mpi_column_num;
    work_data.current_row = V + this->rank * mpi_row_num;
    int i(((int) 0) ), j(((int) 0) );
    if (processor_rank == 0)
    {
        while (work_data.J[j]) j++;
    }
    MPI_Bcast(&j, 1, MPI_INT, 0, column_comm);
    work_data.max_volume.set(((double) 0.0), &i, &j);
    int iters = ((int) 0) , prev_column(j), prev_row(i);
    do
    {
        if ((work_data.max_volume.get_column_position() != prev_column) || (iters == ((int) 0) ))
        {
            j = work_data.max_volume.get_column_position();
            if (j / mpi_avg_row == processor_rank)
            {
                #pragma omp parallel for
                for (int i = ((int) 0) ; i < this->rank; i++)
                {
                    work_data.vec[i] = V[j % mpi_avg_row + i * mpi_row_num];
                }
                #pragma omp barrier
            }
            MPI_Bcast(work_data.vec, this->rank, MPI_DOUBLE, j / mpi_avg_row, column_comm);
            get_diff_column(j, work_data, work_data.current_column);
            int ind[work_data.omp_column_threads_num];
            #pragma omp parallel for
            for (int o = ((int) 0) ; o < work_data.omp_column_threads_num; o++)
            {
                ind[o] = work_data.omp_column_start[o];
                while((work_data.I[ind[o]]) && (ind[o] < work_data.omp_column_start[o] + work_data.omp_column_num[o] - 1)) ind[o] ++;
                for (int k = ind[o] + 1; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; k++)
                {
                    if (!work_data.I[k])
                    {
                        if (abs(work_data.current_column[ind[o]]) < abs(work_data.current_column[k]))
                        {
                            ind[o] = k;
                        }
                    }
                }
            }
            #pragma omp barrier
            int k = 0;
            while ((work_data.I[ind[k]]) && (k < work_data.omp_column_threads_num - 1)) k++;
            i = ind[k];
            for (int o = k + 1; o < work_data.omp_column_threads_num; o++)
            {
                if (!work_data.I[ind[o]])
                {
                    if (abs(work_data.current_column[i]) < abs(work_data.current_column[ind[o]]))
                    {
                        i = ind[o];
                    }
                }
            }
            Send_Info a;
            a.number = i + mpi_column_start;
            a.volume = work_data.current_column[i];
            Send_Info *tmp = NULL;
            if (processor_rank == 0)
            {
                tmp = (Send_Info *) malloc(column_comm_size*sizeof(Send_Info));
            }
            MPI_Gather(&a, 1, Send_Info_Type, tmp, 1, Send_Info_Type, 0, column_comm);
            if (processor_rank == 0)
            {
                int s = 0;
                while (work_data.I[tmp[s].number]) s++;
                a.volume = tmp[s].volume;
                a.number = tmp[s].number; 
                for (int k = s + 1; k < column_comm_size; k++)
                {
                    if (!work_data.I[tmp[k].number])
                    {
                        if (abs(a.volume) < abs(tmp[k].volume))
                        {
                            a.volume = tmp[k].volume;
                            a.number = tmp[k].number;
                        }
                    }
                }
                free(tmp);
            }
            MPI_Bcast(&a, 1, Send_Info_Type, 0, column_comm);
            work_data.max_volume.set(a.volume, &a.number, &j);
            prev_column = work_data.max_volume.get_column_position();
        }
        if ((work_data.max_volume.get_row_position() != prev_row) || (iters == ((int) 0) ))
        {
            i = work_data.max_volume.get_row_position();
            if (i / mpi_avg_column == processor_rank)
            {
                #pragma omp parallel for
                for (int j = ((int) 0) ; j < this->rank; j++)
                {
                    work_data.vec[j] = U[i % mpi_avg_column + j * mpi_column_num];
                }
                #pragma omp barrier
            }
            MPI_Bcast(work_data.vec, this->rank, MPI_DOUBLE, i / mpi_avg_column, row_comm);
            get_diff_row(i, work_data, work_data.current_row);
            int ind[work_data.omp_row_threads_num];
            #pragma omp parallel for
            for (int o = ((int) 0) ; o < work_data.omp_row_threads_num; o++)
            {
                ind[o] = work_data.omp_row_start[o];
                while((work_data.J[ind[o]]) && (ind[o] < work_data.omp_row_start[o] + work_data.omp_row_num[o] - 1)) ind[o] ++;
                for (int k = ind[o] + 1; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; k++)
                {
                    if (!work_data.J[k])
                    {
                        if (abs(work_data.current_row[ind[o]]) < abs(work_data.current_row[k]))
                        {
                            ind[o] = k;
                        }
                    }
                }
            }
            #pragma omp barrier
            int k = 0;
            while ((work_data.J[ind[k]]) && (k < work_data.omp_row_threads_num  - 1)) k++;
            j = ind[k];
            for (int o = k + 1; o < work_data.omp_row_threads_num; o++)
            {
                if (!work_data.J[ind[o]])
                {
                    if (abs(work_data.current_row[j]) < abs(work_data.current_row[ind[o]]))
                    {
                        j = ind[o];
                    }
                }
            }
            Send_Info a;
            a.number = j + mpi_row_start;
            a.volume = work_data.current_row[j];
            Send_Info *tmp = NULL;
            if (processor_rank == 0)
            {
                tmp = (Send_Info *) malloc(row_comm_size*sizeof(Send_Info));
            }
            MPI_Gather(&a, 1, Send_Info_Type, tmp, 1, Send_Info_Type, 0, row_comm);
            if (processor_rank == 0)
            {
                int s = 0;
                while (work_data.J[tmp[s].number]) s++;
                a.volume = tmp[s].volume;
                a.number = tmp[s].number; 
                for (int k = s + 1; k < row_comm_size; k++)
                {
                    if (!work_data.J[tmp[k].number])
                    {
                        if (abs(a.volume) < abs(tmp[k].volume))
                        {
                            a.volume = tmp[k].volume;
                            a.number = tmp[k].number;
                        }
                    }
                }
                free(tmp);
            }
            MPI_Bcast(&a, 1, Send_Info_Type, 0, row_comm);
            work_data.max_volume.set(a.volume, &i, &a.number);
            prev_row = work_data.max_volume.get_row_position();
        }
        iters++;
    } while (iters < work_data.parameters.maximal_iterations_number);
    if (prev_column != work_data.max_volume.get_column_position())
    {
        j = work_data.max_volume.get_column_position();
        if (j / mpi_avg_row == processor_rank)
        {
            #pragma omp parallel for
            for (int i = ((int) 0) ; i < this->rank; i++)
            {
                work_data.vec[i] = V[j % mpi_avg_row + i*mpi_row_num];
            }
            #pragma omp barrier
        }
        MPI_Bcast(work_data.vec, this->rank, MPI_DOUBLE, j / mpi_avg_row, column_comm);
        get_diff_column(j, work_data, work_data.current_column);
    }
    if (prev_row != work_data.max_volume.get_row_position())
    {
        i = work_data.max_volume.get_row_position();
        if (i / mpi_avg_column == processor_rank)
        {
            #pragma omp parallel for
            for (int j = ((int) 0) ; j < this->rank; j++)
            {
                work_data.vec[j] = U[i % mpi_avg_column + j * mpi_column_num];
            }
            #pragma omp barrier
        }
        MPI_Bcast(work_data.vec, this->rank, MPI_DOUBLE, i / mpi_avg_column, row_comm);
        get_diff_row(i, work_data, work_data.current_row);
    }
}

bool TCross_Parallel_v1 ::Stopping_Criteria(TCross_Parallel_v1_Work_Data  &work_data){
    if (processor_rank >= max(row_comm_size, column_comm_size)) return true;
    if (abs(work_data.max_volume.get_volume()) > work_data.global_max)
    {
        work_data.global_max = abs(work_data.max_volume.get_volume());
    }
    if (sqrt(real(norm))*abs(tolerance) >= abs(work_data.max_volume.get_volume())*sqrt(this->get_columns_number()-this->rank)*sqrt(this->get_rows_number()-this->rank))
    {
        #pragma omp parallel sections
        {
            #pragma omp section
                U = (double *) realloc(U, this->rank * mpi_column_num * sizeof(double));
            #pragma omp section
                V = (double *) realloc(V, this->rank * mpi_row_num * sizeof(double));
            #pragma omp section
                C = (double *) realloc(C, this->rank * mpi_column_num * sizeof(double));
            #pragma omp section
                RT = (double *) realloc(RT, this->rank * mpi_row_num * sizeof(double));
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers ,this->rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers,this->rank*sizeof(int));
        }
        #pragma omp barrier
        double *hat_A = (double *) malloc(this->rank * this->rank * sizeof(double));
        #pragma omp parallel for
        for (int k = ((int) 0) ; k < this->rank; k++)
        {
            if (columns_numbers[k] / mpi_column_num == processor_rank)
            {
                copy(this->rank, RT + columns_numbers[k] % mpi_row_num, mpi_row_num, hat_A + k*this->rank, ((int) 1));
            }
            MPI_Bcast(hat_A + k*this->rank, this->rank, MPI_DOUBLE, columns_numbers[k] / mpi_row_num, row_comm);
        }
        hat_A_inv = pseudoinverse(1e-8l, this->rank, this->rank, hat_A);
        free(hat_A);
        work_data.current_row = NULL;
        work_data.current_column = NULL;
        return true;
    }
    else
    {
        return false;
    }
}

void TCross_Parallel_v1 ::Update_Cross(TCross_Parallel_v1_Work_Data  &work_data)
{
    double column_factor(((double) 1.0)/sqrt(abs(work_data.max_volume.get_volume()))), row_factor = ((double) 1.0) / (work_data.max_volume.get_volume()*column_factor);
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            #pragma omp parallel for
            for (int i = ((int) 0) ; i < mpi_column_num; i++)
            {
                if (!work_data.I[i])
                {
                    work_data.current_column[i] *= column_factor;
                }
                else
                {
                    C[this->rank*mpi_column_num + i] = -work_data.current_column[i];
                    work_data.current_column[i] = ((double) 0.0);
                }
            }
        }
        #pragma omp section
        {
            #pragma omp parallel for
            for (int j = ((int) 0) ; j < mpi_row_num; j++)
            {
                if (!work_data.J[j])
                {
                    work_data.current_row[j] *= row_factor;
                }
                else
                {
                    RT[this->rank*mpi_row_num + j] = -work_data.current_row[j];
                    work_data.current_row[j] = ((double) 0.0);
                }
            }
        }
    }
    if (processor_rank != 0)
    {
        if (work_data.max_volume.get_row_position() / mpi_avg_column == processor_rank)
        {
            work_data.I[work_data.max_volume.get_row_position() % mpi_avg_column] = true;
        }
        if (work_data.max_volume.get_column_position() / mpi_avg_row == processor_rank)
        {
            work_data.J[work_data.max_volume.get_column_position() % mpi_avg_row] = true;
        }
    }
    else
    {
        work_data.I[work_data.max_volume.get_row_position()] = true;
        work_data.J[work_data.max_volume.get_column_position()] = true;
    }
    rows_numbers[this->rank] = work_data.max_volume.get_row_position();
    columns_numbers[this->rank] = work_data.max_volume.get_column_position();
    double *b = (double *) malloc (2*(this->rank + ((int) 1)) * sizeof(double)), *x = (double *) malloc (2*(this->rank + ((int) 1)) * sizeof(double));
    gemv(CblasColMajor, CblasConjTrans, mpi_column_num, this->rank + ((int) 1), ((double) 1.0), U, mpi_column_num, work_data.current_column, ((int) 1), ((double) 0.0), b, ((int) 1));
    gemv(CblasColMajor, CblasConjTrans, mpi_row_num, this->rank + ((int) 1), ((double) 1.0), V, mpi_row_num, work_data.current_row, ((int) 1), ((double) 0.0), b + this->rank + 1, ((int) 1));
    MPI_Allreduce(b, x, 2*(this->rank + 1), MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    norm += ((double) 2.0)*real(dot_c(this->rank, x, ((int) 1), x + this->rank + 1, ((int) 1))) + x[this->rank]*x[2*this->rank + 1];
    free(b);
    free(x);
    this->rank++;
    work_data.current_column = NULL;
    work_data.current_row = NULL;
}

int TCross_Parallel_v1 ::get_row_number(const int & k) const
{
    return rows_numbers[k];
}

int TCross_Parallel_v1 ::get_column_number(const int & k) const
{
    return columns_numbers[k];
}

double * TCross_Parallel_v1::matvec(double *&x)
{
	int R = this->get_rank();
	int M = this->mpi_row_num;
	int N = this->mpi_column_num;
	
	double* x_hat  = (double *) malloc ( R * sizeof(double));
	double *result = (double *) malloc ( M * sizeof(double));

	double* x_hat_buff  = (double *) malloc ( R * sizeof(double));
	int col_size, row_size, rank;
        double reduction_var;

        gemv(CblasColMajor, CblasTrans, N, R, 1.0, V, N, x, 1, 0.0, x_hat, 1);
	/*for (int i = 0; i < R; i++)
	{
		x_hat[i] = 0.0;
                reduction_var = 0.0;
	        #pragma omp parallel for reduction (+:reduction_var)
		for (int j = 0; j < N; j++)
		{
			reduction_var += V[ N * i + j] * x[j];
		}
                x_hat[i] = reduction_var;
	}*/
	//MPI_Barrier(this->column_comm);
	MPI_Allreduce(x_hat, x_hat_buff, R, MPI_DOUBLE, MPI_SUM, this->column_comm);
        
        gemv(CblasColMajor, CblasNoTrans, M, R, 1.0, V, M, x_hat_buff, 1, 0.0, result, 1);

        /*
        #pragma omp parallel for
	for(int i = 0; i < M; i++)
	{
		result[i] = 0.0;
		for(int j = 0; j < R; j++)
		{
			result[i] += U[M * j + i] * x_hat_buff[j];
		}
	}*/
	free(x_hat);
	free(x_hat_buff);
	return result;
}

void TCross_Parallel_v1::init_DFTI_desc()
{
// create FFT plan once 
	int R = this->get_rank();
	int M = this->mpi_row_num;
	int N = this->mpi_column_num;
	int full_N = this->rows_number;
	int s_out, p;
	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	DftiCreateDescriptorDM(MPI_COMM_WORLD, &desc, DFTI_DOUBLE, DFTI_COMPLEX, 1, full_N);
	DftiGetValueDM(desc, CDFT_LOCAL_SIZE, &FFTv);
	DftiGetValueDM(desc, CDFT_LOCAL_X_START, &FFTs);
        DftiSetValueDM(desc, DFTI_ORDERING, DFTI_BACKWARD_SCRAMBLED);

	Vbuff    = (double _Complex *) calloc (N , sizeof(double _Complex));
	Ubuff    = (double _Complex *) calloc (N , sizeof(double _Complex));
	work     = (double _Complex *) calloc (N , sizeof(double _Complex));
        Voutbuff = (double _Complex *) calloc (N , sizeof(double _Complex));
        Uoutbuff = (double _Complex *) calloc (N , sizeof(double _Complex));

	DftiSetValueDM(desc, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
	DftiSetValueDM(desc, CDFT_WORKSPACE, work);
	DftiCommitDescriptorDM(desc);

	if (M != N)
	{
		if (p == 0)
			printf("Matrix is not square, cannot do anything!Return NULL\n");
                MPI_Abort(MPI_COMM_WORLD, 1);
	}

	if (FFTv != N)
	{
		if (p == 0)
			printf("Something is wrong with chunks in cluster FFT; they are not equal to chunks in skeleton format! Return NULL\n");
		printf("v= %d, N = %d\n", FFTv, N);
                MPI_Abort(MPI_COMM_WORLD, 1);
	}
	DftiComputeForwardDM(desc, Ubuff, Uoutbuff);
	DftiComputeBackwardDM(desc, Ubuff, Uoutbuff);
}

void TCross_Parallel_v1::destroy_DFTI_desc()
{
	DftiFreeDescriptorDM(&desc);
        free(Vbuff);
        free(Ubuff);
        free(Voutbuff);
        free(Uoutbuff);
        free(work);
}

double * TCross_Parallel_v1::smol_conv(double *&x)
{
	int R = this->get_rank();
	int M = this->mpi_row_num;
	int N = this->mpi_column_num;
	int full_N = this->rows_number;
	int s_out, p;
	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	double *result = (double*) calloc (N , sizeof(double));
	//firstly, we multiply regular circulant part of toeplitz matrix by vector
//	#pragma omp parallel for
//	for (int i = 0; i < N; i++)
//		result[i] = 0;

        //double time1, time2;
        //time1 = MPI_Wtime();
	for (int i = 0; i < R; i++)
	{
//		#pragma omp parallel for
		for(int j = 0; j < N; j++)
		{
			__real__ Ubuff[j] = U[N * i + j] * x[j];
			__imag__ Ubuff[j] = 0;//no renormalization
			__real__ Vbuff[j] = V[N * i + j] * x[j];
			__imag__ Vbuff[j] = 0;//no renormalization
		}
		//MPI_Barrier(MPI_COMM_WORLD);
	
		DftiComputeForwardDM(desc, Ubuff, Uoutbuff);
		DftiComputeForwardDM(desc, Vbuff, Voutbuff);
		//after FFT snd before the backward FFT we do elementwise product of two vectors
//		#pragma omp parallel for
		for (int j = 0; j < N; j++)
			Ubuff[j] = Uoutbuff[j] * Voutbuff[j];
		//MPI_Barrier(MPI_COMM_WORLD);

		DftiComputeBackwardDM(desc, Ubuff, Uoutbuff);
//		#pragma omp parallel for
		for(int j = 0; j < N; j++)
			result[j] += __real__(Uoutbuff[j] / (2.0 * full_N));


	}
        //time2 = MPI_Wtime();
        //if ( p == 0)
        //    printf("Circulant time = %lf \n", time2 - time1);
	//secondly, we multiply -1-circulant part of toeplitx matrix by vector
	
	for (int i = 0; i < R; i++)
	{
//		#pragma omp parallel for
		for(int j = 0; j < N; j++)
		{
			//here we do renormalization because of structire of  -1-circulant
			__real__ Ubuff[j] = U[N * i + j] * x[j] * cos(-M_PI * (j + FFTs) / full_N);
			__imag__ Ubuff[j] = U[N * i + j] * x[j] * sin(-M_PI * (j + FFTs) / full_N);
			__real__ Vbuff[j] = V[N * i + j] * x[j] * cos(-M_PI * (j + FFTs) / full_N);
			__imag__ Vbuff[j] = V[N * i + j] * x[j] * sin(-M_PI * (j + FFTs) / full_N);
		}
		//MPI_Barrier(MPI_COMM_WORLD);

		DftiComputeForwardDM(desc, Ubuff, Uoutbuff);
		DftiComputeForwardDM(desc, Vbuff, Voutbuff);
		//after FFT and before the backward FFT we do elementwise product of two vectors
//		#pragma omp parallel for
		for (int j = 0; j < N; j++)
			Ubuff[j] = Uoutbuff[j] * Voutbuff[j];

		//MPI_Barrier(MPI_COMM_WORLD);

		DftiComputeBackwardDM(desc, Ubuff, Uoutbuff);

//		#pragma omp parallel for
		for(int j = 0; j < N; j++)
			result[j] += (cos(M_PI * (j + FFTs) / full_N) * (__real__ Uoutbuff[j]) - sin(M_PI * (j + FFTs) / full_N) * (__imag__ Uoutbuff[j]) )/ (2.0 * full_N) ;//cos and sin here comes from "backward" renormalization
	}        
        //time1 = MPI_Wtime();
        //if ( p == 0)
        //    printf("Skew Circulant time = %lf \n", time1 - time2);
	//MPI_Barrier(MPI_COMM_WORLD);
	return result;
}

double *TCross_Parallel_v1::smol_conv_discrete(double *&x)
{
	double *result;

	int p, np;
	result = this->smol_conv(x);
	MPI_Request request;
	MPI_Status status;
//	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	int N = this->mpi_column_num;

        double *final_result = (double *) malloc (N * sizeof(double));
	double bubble_send,bubble_recv, bubble1, bubble2;

	bubble_send = result[N - 1];
	// Exchange of chunks' boundaries 
	if ( p != (np - 1) )
		MPI_Isend(&bubble_send, 1, MPI_DOUBLE, p + 1, 1, MPI_COMM_WORLD, &request);
	if ( p != 0)
		MPI_Recv(&bubble_recv, 1, MPI_DOUBLE, p - 1, 1, MPI_COMM_WORLD, &status);
	// shift of the local chunk
	bubble1 = result[0];

	// correct the boundaries
        //#pragma omp parallel for
	for (int i = 0; i < (N - 1); i++)
	{
		final_result[i+1] = result[i];
	}

	if ( p == 0)
		final_result[0] = 0;
	else
		final_result[0] = bubble_recv;
        free(result);
	return final_result;
}
