#!/bin/bash

for kname in "const";
do
    for ktype in "all";
    do
        for D in 9;
        do
            for time in 50;
            do
                for iter_per_one in 100;
                do
                    T=$(( $time * $iter_per_one ))
                    dt=$( echo 0$( bc <<< "scale=4; 1. / $iter_per_one" | sed -e 's/[0]*$//g' ) )
                    #dt=0.01

                    for N in 131072;
                    do
                        echo bash singex.sh $kname $ktype $D $N $T $dt\
                            "times/s_"$kname"_"$D"-"$ktype"_"$time"_"$dt"_"$T "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T
                        bash singex.sh $kname $ktype $D $N $T $dt\
                            "times/s_"$kname"_"$D"-"$ktype"_"$time"_"$dt"_"$T "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T
                        #echo bash dsingex.sh $kname $ktype $T $dt $N\
                        #    "times/d_"$kname$ktype"_"$time"_"$dt"_"$T "results/d_"$kname$ktype"_"$N"_"$time"_"$dt"_"$T
                        #bash dsingex.sh $kname $ktype $T $dt $N\
                        #    "times/d_"$kname$ktype"_"$time"_"$dt"_"$T "results/d_"$kname$ktype"_"$N"_"$time"_"$dt"_"$T
                    done
                done
            done
        done
    done
done

for kname in "const";
do
    for ktype in "all";
    do
        for D in 9;
        do
            for time in 100;
            do
                for iter_per_one in 100;
                do
                    T=$(( $time * $iter_per_one ))
                    dt=$( echo 0$( bc <<< "scale=4; 1. / $iter_per_one" | sed -e 's/[0]*$//g' ) )
                    #dt=0.01

                    for N in 16384 32768 65536 131072;
                    do
                        echo bash singex.sh $kname $ktype $D $N $T $dt\
                            "times/s_"$kname"_"$D"-"$ktype"_"$time"_"$dt"_"$T "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T
                        bash singex.sh $kname $ktype $D $N $T $dt\
                            "times/s_"$kname"_"$D"-"$ktype"_"$time"_"$dt"_"$T "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T
                        #echo bash dsingex.sh $kname $ktype $T $dt $N\
                        #    "times/d_"$kname$ktype"_"$time"_"$dt"_"$T "results/d_"$kname$ktype"_"$N"_"$time"_"$dt"_"$T
                        #bash dsingex.sh $kname $ktype $T $dt $N\
                        #    "times/d_"$kname$ktype"_"$time"_"$dt"_"$T "results/d_"$kname$ktype"_"$N"_"$time"_"$dt"_"$T
                    done
                done
            done
        done
    done
done

for kname in "const";
do
    for ktype in "all";
    do
        for D in 10;
        do
            for time in 1 5 10 50 100;
            do
                for iter_per_one in 100;
                do
                    T=$(( $time * $iter_per_one ))
                    dt=$( echo 0$( bc <<< "scale=4; 1. / $iter_per_one" | sed -e 's/[0]*$//g' ) )
                    #dt=0.01

                    for N in 16384 32768 65536 131072;
                    do
                        echo bash singex.sh $kname $ktype $D $N $T $dt\
                            "times/s_"$kname"_"$D"-"$ktype"_"$time"_"$dt"_"$T "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T
                        bash singex.sh $kname $ktype $D $N $T $dt\
                            "times/s_"$kname"_"$D"-"$ktype"_"$time"_"$dt"_"$T "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T
                        #echo bash dsingex.sh $kname $ktype $T $dt $N\
                        #    "times/d_"$kname$ktype"_"$time"_"$dt"_"$T "results/d_"$kname$ktype"_"$N"_"$time"_"$dt"_"$T
                        #bash dsingex.sh $kname $ktype $T $dt $N\
                        #    "times/d_"$kname$ktype"_"$time"_"$dt"_"$T "results/d_"$kname$ktype"_"$N"_"$time"_"$dt"_"$T
                    done
                done
            done
        done
    done
done
