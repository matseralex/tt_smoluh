#include <iostream>
#include <stdio.h>
#include <gsl/gsl_sf_bessel.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <time.h>
#include <sys/time.h>
using namespace std;
double K(const double & u1, const double & u2, const double &v1, const double &v2)
{
    double u=u1+u2;
    double v=v1+v2;

//    if (u==0||v==0)
//	return 0.0;
//    else
//	return M_PI*pow(0.75/M_PI, 2.0l/3)*pow(pow(u,1.0l/3)+pow(v,1.0l/3),2)*sqrt(8.0/M_PI * (1.0l/v + 1.0l/u));

    return 1.0;
}


double L1(const int &N, const int &i,const int &j, const double *n, const double &h)
{
    double l1 = 0;
    for (int j1 = 1; j1 <= j; j1++)
    {
        for (int i1 = 1; i1 <= i; i1++)
        {
            l1 += n[j1 * N + i1] * n[(j - j1) * N + i - i1] * K((i - i1) * h, (j - j1) * h, i1 * h, j1 * h) + n[j1 * N + i1 - 1] * n[(j - j1) * N + i - i1 + 1] * K((i - i1 + 1) * h, (j - j1) * h, (i1 - 1) * h, j1 * h) + n[(j1 - 1) * N + i1] * n[(j - j1 + 1) * N + i - i1] * K((i - i1) * h, (j - j1 + 1) * h, i1 * h, (j1 - 1) * h) + n[(j1 - 1) * N + i1 - 1] * n[(j - j1 + 1) * N + i - i1 + 1] * K((i - i1 + 1) * h, (j - j1 + 1) * h, (i1 - 1) * h, (j1 - 1) * h);
        }
    }
    l1 *= h*h/4;
    return l1;
}

double L2(const int &N, const int &i,const int &j, const double *n, const double &h)
{
    double l2 = 0;
    for (int j1 = 1; j1 < N; j1++)
    {
        for (int i1 = 1; i1 < N; i1++)
        {
            l2 += n[j1 * N + i1] * K(i * h, j * h, i1 * h, j1 * h) + n[j1 * N + i1 - 1] * K(i * h, j * h, (i1 - 1) * h, j1 * h) + n[(j1 - 1) * N + i1] * K(i * h, j * h, i1 * h, (j1 - 1) * h) + n[(j1 - 1) * N + i1 - 1] * K(i * h, j * h, (i1 - 1) * h, (j1 - 1) * h);
        }
    }
    l2 *= h*h/4;
    return l2;
}


double L_for_vol(const int &N,  const double *n, const double &h)
{
// For computation of Int (Int ( (v_1+v_2)*n(v_1,v_2)dv_1) dv_2 ) v_1, v_2 from [0, v_max]x [0, v_max]
    double l_for_vol = 0;
    for (int j1 = 1; j1 < N; j1++)
    {
        for (int i1 = 1; i1 < N; i1++)
        {
            l_for_vol += (i1*h+j1*h)*n[j1 * N + i1]  + (j1*h+(i1-1)*h)*n[j1 * N + i1 - 1] + ((j1-1)*h+i1*h)*n[(j1 - 1) * N + i1] +((j1-1)*h+(i1-1)*h)* n[(j1 - 1) * N + i1 - 1] ;
        }
    }
    l_for_vol *= h*h/4;
    return l_for_vol;
}

double start_cond(const double &v1, const double &v2)
{
    return exp(-v1 - v2);
}

double analitycal_solution(const double &v1, const double &v2, const double & t)
{
    return exp(-v1 - v2) / ((1.0 + t / 2) * (1.0 + t / 2)) * gsl_sf_bessel_I0(2.0 * sqrt(t * v1 * v2 / (2.0 + t)));
}

int main(int argc, char** argv)
{
    if (argc != 5)
    {
        cout << "4 parameters have been expected" << endl;
	cout << "v_max steps time_step number_of_knots" << endl;
        return -1;
    }
    double v_max = 10.0;
    int N = 101, steps = 100;
    double time_step=0.01;
    struct timeval start, end;
    double r_time;
    sscanf(argv[1], "%lf", &v_max);
    sscanf(argv[2], "%d", &steps);
    sscanf(argv[3], "%lf", &time_step);
    sscanf(argv[4], "%d", &N);
    double *n_k, *n_k_half, *n_k_one, h, *n_analit;
    n_k = (double *) malloc(N*N*sizeof(double));
    n_k_half = (double *) malloc(N*N*sizeof(double));
    n_k_one = (double *) malloc(N*N*sizeof(double));
    n_analit = (double *) malloc(N*N*sizeof(double));
    h = v_max / (N - 1);
    for (int j = 0; j < N; j++)
    {
        for (int i = 0; i < N; i++)
        {
            n_k[j*N + i] = start_cond(h*i,h*j);
            n_analit[j*N + i] = analitycal_solution(h*i,h*j, 0.0);
        }
    }
    cblas_daxpy(N*N, -1.0, n_k, 1, n_analit, 1);
    cout << "Relevant error in starting condition = " << cblas_dnrm2(N*N, n_analit, 1) / cblas_dnrm2(N*N, n_k, 1) << ", norm = " << cblas_dnrm2(N*N, n_k, 1) << endl;
    cout << "Starting coagulation volume=" << L_for_vol(N, n_k, h) << endl;

    gettimeofday(&start, NULL);
    for (int k = 0; k < steps; k++)
    {
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                double l1, l2;
                l1 = 0.5 * L1(N, i, j, n_k, h);
                l2 = L2(N, i, j, n_k, h);
                /*l2 = (L2(N, i, j, n_k, h) + L2(N, i + 1, j, n_k, h) + L2(N, i, j + 1, n_k, h) + L2(N, i + 1, j + 1, n_k, h)) / 4;*/
                n_k_half[j*N + i] = (l1 - n_k[j * N + i] * l2) * time_step * 0.5 + n_k[j * N + i];
                n_analit[j*N + i] = analitycal_solution(h*i,h*j, (k + 0.5) * time_step);
            }
        }
        double norm = cblas_dnrm2(N*N, n_analit, 1);
        cblas_daxpy(N*N, -1.0, n_k_half, 1, n_analit, 1);
        cout << "Step " << k + 1 << ", absolute error after predictor = "/*", relevant error after predictor = "*/ << cblas_dnrm2(N*N, n_analit, 1) /*/ norm */<< ", norm = " << norm << endl;
	cout << "Step " << k + 1 << ", relevant error after predictor = " << cblas_dnrm2(N*N, n_analit, 1) / norm<< ", norm = " << norm << endl;
	
     for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                double l1, l2;
                l1 = 0.5 * L1(N, i, j, n_k_half, h);
                l2 = L2(N, i, j, n_k_half, h);
                n_k_one[j*N + i] =  (l1 - n_k_half[j * N + i] * l2) * time_step + n_k[j * N + i];
                n_analit[j*N + i] = analitycal_solution(h*i,h*j, (k + 1.0) * time_step);
            }
        }
        norm = cblas_dnrm2(N*N, n_analit, 1);
        cblas_daxpy(N*N, -1.0, n_k_one, 1, n_analit, 1);
        cout << "Step " << k + 1 << ",absolute error after corrector = " /*", relevant error after corrector = "*/ << cblas_dnrm2(N*N, n_analit, 1) /* / norm */<< ", norm = " << norm << endl;
        cout << "Step " << k + 1 << ", relevant error after corrector = " << cblas_dnrm2(N*N, n_analit, 1) / norm << ", norm = " << norm << endl;
	cout << "Coagulation volume on step" << k+1 <<" = " << L_for_vol(N, n_k, h) << endl;
        double * tmp = n_k_one;
        n_k_one = n_k;
        n_k = tmp;
        tmp = NULL;
    }

    gettimeofday(&end,NULL);
    r_time=end.tv_sec - start.tv_sec +((double)(end.tv_usec-start.tv_usec))/1000000;
    cout << "Average time for step = " << r_time/steps << " seconds"<<endl;
    cout << "Full time = " << r_time << endl;
    free(n_k);
    free(n_k_half);
    free(n_k_one);
    free(n_analit);
}
