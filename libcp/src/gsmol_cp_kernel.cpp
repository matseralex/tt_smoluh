#include<cstring>
#include<cstdlib>
#include<cmath>
#include<iostream>

#include"../include/gsmol_cp_blas.h"
#include"../include/gsmol_cp_utility.h"
#include"../include/gsmol_cp_kernel.h"

//============================================================================//
//    Kernel deallocation                                                     //
//============================================================================//
TKernel_CP::~TKernel_CP()
{
    if (R)
        free(R);

    if (mult)
        free(mult);

    if (intrarg)
        free(intrarg);

    if (asgn)
        free(asgn);

    if (kdata)
        free(kdata);

    if (updaux)
        free(updaux);

    if (fftaux)
        mkl_free(fftaux);

    if (ffthand)
        DftiFreeDescriptor(&ffthand);
}

//============================================================================//
//    Kernel initialization                                                   //
//============================================================================//
TKernel_CP::TKernel_CP(
    // in: kernel name
    const char * kname,
    // in: collisions types
    const char * ktype,
    // in: kernels maximal dimension
    const int & dim,
    // in: number of equations
    const int & mode,
    // in: weights of kernels
    const double * alpha
):
    D(dim),
    N(mode),
    kdata(NULL),
    ffthand(NULL)
{
    const int ione = 1;
    const int itwo = 2;

    R = (int *)calloc(D - 1, sizeof(int));
    mult = (double *)calloc(D - 1, sizeof(double));

    double coeff[D];
    coeff[0] = 1.;

    for (int d = 1; d < D; ++d)
        coeff[d] = coeff[d - 1] / (double)(d + 1);

    const int size = N << 1;
    MKL_LONG status;
    MKL_LONG rs[2];
    rs[0] = 0;
    rs[1] = 1;

    fftaux = (std::complex<double> *)mkl_malloc(
            2 * (size + 2) * sizeof(std::complex<double>), 64
        );
    status = DftiCreateDescriptor(&ffthand, DFTI_DOUBLE, DFTI_REAL, 1, size);
    status = DftiSetValue(ffthand, DFTI_CONJUGATE_EVEN_STORAGE, DFTI_COMPLEX_COMPLEX);
    status = DftiSetValue(ffthand, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
    status = DftiSetValue(ffthand, DFTI_INPUT_STRIDES, rs);
    status = DftiCommitDescriptor(ffthand);

    //====================================//
    //    Sum of squared roots kernels    //
    //====================================//
    if (!strcmp(kname, "sumsqrt"))
    {
        // d-all collisions 
        if (!strcmp(ktype, "all"))
        {
            copy(D - 1, &ione, 0, R, 1);
            asgn = (int *)calloc(D - 1, sizeof(int));
            copy(D - 1, &itwo, 0, asgn, 1);
            intrarg = (int *)calloc(D - 1, sizeof(int));
            copy(D - 1, &ione, 0, intrarg, 1);

#pragma omp parallel for
            for (int a = 0; a < D - 1; ++a)
                mult[a] = coeff[a] * alpha[a];

            int id = dot(D - 1, R, 1, asgn, 1); 

            kdata = (double *)malloc(id * N * sizeof(double));
            updaux = (double *)malloc(id * size * sizeof(double));

#pragma omp parallel for collapse(2)
            for (int a = 0; a < D - 1; ++a)
                for (int i = 0; i < N; ++i)
                    kdata[i + a * N] = sqrt(i + 1);
        }
        // d-only collisions 
        else
        {
            R[D - 2] = 1;
            mult[D - 2] = coeff[D - 2] * alpha[D - 2];
            asgn = (int *)calloc(1, sizeof(int));
            asgn[0] = 2;
            intrarg = (int *)calloc(1, sizeof(int));
            intrarg[0] = 1;

            kdata = (double *)malloc(N * sizeof(double));
            updaux = (double *)malloc(asgn[0] * size * sizeof(double));

#pragma omp parallel for
            for (int i = 0; i < N; ++i)
                kdata[i] = sqrt(i + 1);
        }
    }
    else
    //===================================//
    //    Generalized product kernels    //
    //===================================//
    if (!strcmp(kname, "genprod"))
    {
        // d-all collisions 
        if (!strcmp(ktype, "all"))
        {
            copy(D - 1, &ione, 0, R, 1);
            asgn = (int *)calloc(D - 1, sizeof(int));
            intrarg = (int *)calloc(D - 1, sizeof(int));

#pragma omp parallel for
            for (int a = 0; a < D - 1; ++a)
            {
                asgn[a] = a + 2;
                mult[a] = alpha[a];
            }

            int id = dot(D - 1, R, 1, asgn, 1); 

            kdata = (double *)malloc(id * N * sizeof(double));
            updaux = (double *)malloc(id * size * sizeof(double));

            int k = 0;
            double m0[id];

            for (int a = 2; a <= D; k += a, ++a)
#pragma omp parallel for
                for (int d = 0; d < a; ++d)
                    m0[k + d] = 0.35 - 0.1 * (double)d;

#pragma omp parallel for collapse(2)
            for (int a = 0; a < id; ++a)
                for (int i = 0; i < N; ++i)
                    kdata[i + a * N] = pow(i + 1, m0[a]);
        }
        // d-only collisions 
        else
        {
            R[D - 2] = 1;
            mult[D - 2] = alpha[D - 2];
            asgn = (int *)malloc(sizeof(int));
            asgn[0] = D;
            intrarg = (int *)calloc(1, sizeof(int));

            kdata = (double *)malloc(D * N * sizeof(double));
            updaux = (double *)malloc(D * size * sizeof(double));

            double m0[D];

#pragma omp parallel for
            for (int d = 0; d < D; ++d)
                m0[d] = 0.35 - 0.1 * (double)d;

#pragma omp parallel for collapse(2)
            for (int d = 0; d < D; ++d)
                for (int i = 0; i < N; ++i)
                    kdata[i + d * N] = pow(i + 1, m0[d]);
        }
    }
    else
    //========================//
    //    Constant kernels    //
    //========================//
    {
        // d-all collisions 
        if (!strcmp(ktype, "all"))
        {
            copy(D - 1, &ione, 0, R, 1);
            asgn = (int *)calloc(D - 1, sizeof(int));
            copy(D - 1, &ione, 0, asgn, 1);
            intrarg = (int *)calloc(D - 1, sizeof(int));
            copy(D - 1, &ione, 0, intrarg, 1);

#pragma omp parallel for
            for (int a = 0; a < D - 1; ++a)
                mult[a] = coeff[a + 1] * alpha[a];

            updaux = (double *)malloc((D - 1) * size * sizeof(double));
        }
        // d-only collisions 
        else
        {
            R[D - 2] = 1;
            mult[D - 2] = coeff[D - 1] * alpha[D - 2];
            asgn = (int *)calloc(1, sizeof(int));
            asgn[0] = 1;
            intrarg = (int *)calloc(1, sizeof(int));
            intrarg[0] = 1;

            updaux = (double *)malloc(size * sizeof(double));
        }
    }

    return;
}

//============================================================================//
//    Smoluchowski updates of argument by kernel for r-th convolution         //
//============================================================================//
// alt: updaux[] = kernel-updates of argument by kdata
void TKernel_CP::update(
    // in: index of convolution summand to handle
    const int & r,
    // in: argument to update
    const double * arg,
    // out: arity of convolution
    int & d,
    // out: moments of updated arguments
    double * w
) {
    const double dzero = 0.;
    const double dmone = -1.;
    const int size = N << 1;

    // head = index of kdata entry corresponding to r-th convolution
    int head = 0;

#pragma omp parallel for reduction(+:head)
    for (int i = 0; i < r; ++i)
        head += asgn[i] - intrarg[i];

    // tail = index of last necessary update
    const int tail = asgn[r] - 1;

    //=======================================//
    //    Arity of convolution computation   //
    //=======================================//
    d = 2;

    for (int a = 0; (R[d - 2] == 0 || r > a) && d <= D; a += R[d - 2], ++d)
        ;

    //=====================================================//
    //    Updated arguments & their moments computation    //
    //=====================================================//
    double * uarg[d];
    const double * data[d];
    double m[d];

    // m = 0 
    copy(d, &dzero, 0, m, 1);

    // updaux[k] = nontrivial updated argument
    // m[k] = its moment
#pragma omp parallel for
    for (int k = 0; k < asgn[r] - intrarg[r]; ++k)
    {
        data[k] = kdata + ((head + k) * N); 
        uarg[k] = updaux + (k * size); 

        // uarg[k] = arg 
        copy(size, arg, 1, uarg[k], 1);

        // uarg[k] update
        // m[k] = moment of uarg[k] 
        for (int i = 0; i < N; ++i)
        {
            uarg[k][i] *= data[k][i];
            m[k] += uarg[k][i];
        }
    }

    // updaux[tail] = trivial argument
    // m[tail] = its moment
    if (intrarg[r])
    {
        // trivial last argument 
        copy(size, arg, 1, updaux + (tail * size), 1);

        double mtail = 0.; 
        // trivial moment computation
#pragma omp parallel for reduction(+:mtail)
        for (int i = 0; i < N; ++i)
            mtail += arg[i];

        // set remaining moments to trivial
#pragma omp parallel for
        for (int k = tail; k < d; ++k)
            m[k] = mtail;
    }

    //===========================//
    //    Weights computation    //
    //===========================//
    if (d == 2)
    {
        w[0] = -m[1];
        w[1] = -m[0];
    }
    else
    {
        copy(d, &dmone, 0, w, 1);

#pragma omp parallel for
        for (int k = 0; k < asgn[r]; ++k)
            for (int i = 0; i < d; ++i)
                if (k != i)
                    w[k] *= m[i];
    }

    scal((double)d, mult[d - 2], w, 1);

    // counting multiple summands on tail
    w[tail] *= (double)(d - tail);

    return;
}

//============================================================================//
//    Smoluchowski operator                                                   //
//============================================================================//
void TKernel_CP::compute(
    // in:
    const double * arg,
    // out: S(arg)
    double * res
) {
    // constants
    const double dzero = 0.;
    const int size = N << 1;

    int d;
    double w[D];
    double * aux = (double *)fftaux;
    int rsum = 0;

    // rsum = quantity of summands 
#pragma omp parallel for reduction(+:rsum)
    for (int a = 0; a < D - 1; ++a)
        rsum += R[a];

    // res = 0
    copy(N, &dzero, 0, res, 1);

    //========================================//
    //    Convolution summands computation    //
    //========================================//
    for (int r = 0; r < rsum; ++r)
    {
        // updaux[] = kernel-updated arguments
        // w[] = their weights 
        // d = type of collision 
        update(r, arg, d, w);

        // d-ary convolution using updaux[] as arguments
        // aux = gconv(updaux)
        gconv(d, N, asgn[r], updaux, aux, fftaux, ffthand);

        // res += income
        axpy(N - d + 1, mult[d - 2], aux, 1, res + (d - 1), 1);

        // res += outcome
        gemv(
            CblasColMajor, CblasNoTrans,
            N, asgn[r],
            1., updaux, size,
            w, 1,
            1., res, 1
        );
    }

    // res + N = 0
    copy(N, &dzero, 0, res + N, 1); 

    return;
}

