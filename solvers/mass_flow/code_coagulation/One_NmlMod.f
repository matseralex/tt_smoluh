      MODULE One_NmlMod

      include 'param.i'

c---------------------------------------------------------------
c     Contains description of all input data given in all namesits
c----------------------------------------------------------------
      real*8
c-----from namelist/numericle_nml/
     &t_max,                       !maximal time
     &tau,                         !time step
     &v_x_max,v_y_max,             !Maximum particle volumes
     &h_plot,                      !time step for plots
c-----from namelist/physical_nml    
     &v_x_0,v_y_0,                 !Overage initial volumes
     &rK_0,                        !Kernal
     &rN_0,                        !for Kernal
c-----will be used in namelists for v0_vector ------------------------
     &prof(1:N_species_a)          !for v0_vector
  
      integer 
c-----from namelist/physical_nml
     &N_species,                    !number of species
     &i_coag_type,                  !type of kernel 1 const, 2 brounian
c-----from namelist /numericle_nml/
     &Nx,Ny,                       !mesh dimentions n(0:N_x),(0:Ny)
     &n_vol_plot,                   !number of mesh points 
                                   !for sectional distributions (0:n_vol_plot)
     &N_particle                   !number of MC particles


      character
c-----from namelist /numericle_nml/  
     &mnemonic*128               ! it is used for naming the data output files:
                                 ! mnemonic.txt
     
      END MODULE One_NmlMod
