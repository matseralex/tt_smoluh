#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<ctime>
#include<complex>

#include"blas.h"
#include"../libcp/include/gsmol_cp_utility.h"

//============================================================================//
//    Main                                                                    //
//============================================================================//
int main(
    int argc,
    char ** argv
) {
    if (argc < 10)
    {
        printf(
            "================================================================================\n"
        );

        printf(
            "Required arguments:\n (1) kernel_name\n (2) collisions_type\n (3) dimensionality\n (4) mode_size\n"
        );

        printf(
            " (5) number_iterations\n (6) dt\n (7) tolerance\n (8) path_to_cp\n (9) path_to_tt\n"
        );

        printf(
            "Optional arguments:\n(10) use_analytical\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: kernel_name == {const, sumsqrt, genprod}\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: collisions_type == all   --   all collisions till dimensionality\n"
        );

        printf(
            "Tips: collisions_type != all   --   only dimensionality-ary collisions\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: path_to_{cp, tt} == <path>/\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: if use_analytical presents, then compute relative error of cp & tt\n"
        );

        printf(
            "================================================================================\n"
        );

        return 1;
    }

    int D;
    int N;
    int T;
    double dt;
    double tol;

    // argv parsing
    sscanf(argv[3], "%d", &D);
    sscanf(argv[4], "%d", &N);
    sscanf(argv[5], "%d", &T);
    sscanf(argv[6], "%lf", &dt);
    sscanf(argv[7], "%lf", &tol);

    if (D < 2 || N < 1 || T < 1)
        return 1;

    int tmp;
    int time = (int)rint(dt * T);
    char filename_cp[256];
    char filename_tt[256];

    sprintf(
        filename_cp, "%ss_%s_%d-%s_%d_%d_%s_%d",
        argv[8], argv[1], D,
        (strcmp(argv[2], "all"))? "only": "all",
        N, time, argv[6], T
    );

    sprintf(
        filename_tt, "%ss_%s_%d-%s_%d_%d_%s_%d_%s",
        argv[9], argv[1], D,
        (strcmp(argv[2], "all"))? "only": "all",
        N, time, argv[6], T, argv[7]
    );

    // solutions
    double * rescp = (double *)malloc(N * sizeof(double));
    double * restt = (double *)malloc(N * sizeof(double));
    double mcp = 0.; 
    double mtt = 0.; 

    //=======================================================//
    //    Read CP numerical solution & compute its moment    //
    //=======================================================//
    FILE * fres = fopen(filename_cp, "r");

    for (int i = 0; i < N; ++i)
    {
        fscanf(fres, "%d %lf", &tmp, rescp + i);
        mcp += rescp[i];
    }

    fclose(fres);

    //=======================================================//
    //    Read TT numerical solution & compute its moment    //
    //=======================================================//
    fres = fopen(filename_tt, "r");

    for (int i = 0; i < N; ++i)
    {
        fscanf(fres, "%d %lf", &tmp, restt + i);
        mtt += restt[i];
    }

    fclose(fres);

    printf(
        "================================================================================\n"
    );

    //==================//
    //    Comparison    //
    //==================//
    printf("Relative difference = %.14f\n", (mcp - mtt) / mcp);
    printf("Euclid metrics = %.14f\n", euclid_metrics(N, rescp, restt));

    //====================================================//
    //    Analytical solution & its moment computation    //
    //====================================================//
    if (argc >= 11)
    {
        double * resan = (double *)malloc(N * sizeof(double));
        double man = trivial_compute(time, D, N, resan); 

        printf("Relative error cp = %.14f\n", (man - mcp) / man);
        printf("Euclid metrics = %.14f\n", euclid_metrics(N, resan, rescp));

        printf("Relative error tt = %.14f\n", (man - mtt) / man);
        printf("Euclid metrics = %.14f\n", euclid_metrics(N, resan, restt));

        free(resan);
    }

    printf(
        "================================================================================\n"
    );

    //=========================//
    //    Deallocate memory    //
    //=========================//
    free(rescp);
    free(restt);

    return 0;
}
 
