      Subroutine coagulation_kernel(N_species,
     &u_vector,v_vector,coag_Kernel_0,i_coag_type,
     & dK_coag_l)
c-------------------------------------------------------
c     calculate coagulation kernel for Brownian motion
c-------------------------------------------------------
      implicit none
c-----input
      integer :: N_species
      real*8 ::
     &coag_Kernel_0      !m**3/sec

      real*8, dimension(1:N_species) :: u_vector,v_vector  !volumes [m**3

      integer ::
     &i_coag_type              ! =1 constant coagulation kernel
                               ! =2 brownian coagulation kernel 
c-----output
      real*8 ::
     &dK_coag_l ![m**3/sec]
c-----locals
      real*8 :: u_sum,v_sum, !volumes [m**3]
     &p

      integer :: i
 
c      dK_coag=dK0*(2.d0+p+1.d0/d0/p)

c      write(*,*)'in coagulation_kernel'
c      write(*,*)'N_species',N_species
c      write(*,*)'coag_Kernel_0',coag_Kernel_0
c      write(*,*)'i_coag_type',i_coag_type
c      write(*,*)'u_vector',u_vector
c      write(*,*)'v_vector',v_vector


      if (i_coag_type.eq.1) then
c--------1) Constant kernel
         dK_coag_l=coag_Kernel_0
       elseif(i_coag_type.eq.2) then
c--------2) Brownian kernel
         p=1.d0/3.d0
         u_sum=0.d0
         v_sum=0.d0
         do i=1,N_species
            u_sum=u_sum+u_vector(i)
            v_sum=v_sum+v_vector(i)
         enddo
         dK_coag_l=coag_Kernel_0*(v_sum**p+u_sum**p)**2*
     &           dsqrt(1.d0/v_sum+1.d0/u_sum)
       endif


c      write(*,*)'in coagulation_kernel coag_Kernel_0,dK_coag_l',
c     &                                 coag_Kernel_0,dK_coag_l

c-----3) Linear coagulation
c      dk_coag_l=coag_Kernel_0(u+v)
c-----4) dK_coag_l=coag_Kernel_0(h(v)*g(u)+h(u)g(u))
            
      return
      end      

      subroutine calculate_lamda_k(N_species,
     &N_particle,N_particles_ar_dim,
     &dk_coag_l,y_ar,
     &coag_Kernel_0,i_coag_type,Q0,
     &dlabmda_k)
c------------------------------------------------
c     calculate dlabmda_k overestimate max
c     dlabmda_k=Q0
c-----------------------------------------------
      implicit none
c-----input
      integer :: 
     &N_species,          !number of species
     &N_particle,         !number of particles  
     &N_particles_ar_dim, !dimension of arrays: y_ar,omega_ar,dk_coag_ar
     &i_coag_type              ! =1 constant coagulation kernel
                               ! =2 brownian coagulation kernel 
      real*8  :: 
     &coag_Kernel_0,      !kernel amplitude
     &Q0
           
      real*8, dimension(1:N_species,1:N_particle) :: y_ar   !volume
c      real*8, dimension(1:N_particles_ar_dim,1:N_particles_ar_dim) ::
c     & dk_coag_ar !coagulation kernel
      real*8  ::
     & dk_coag_l !coagulation kernel

c-----output
      real*8 :: dlabmda_k !lambda_k=Q_0*max{omega_ar(j)*K(y_ar(i),y_ar(j))/y_ar(j)}

c-----locals
      real*8 :: y_min,dk_max,y_sum,
c------for Brownian kernel
     &y_max

      integer :: i,j

c      write(*,*)'in calculate_lamda_k coag_Kernel_0', coag_Kernel_0
 
c      omega_max=maxval(omega_ar)
c      y_min=minval(y_ar)

      y_min=1.d10
      y_max=0.d0
      do i=1,N_particle
        y_sum=0.d0
        do j=1,N_species
          y_sum=y_sum+y_ar(j,i)
        enddo 

        if(y_sum.lt.y_min) y_min=y_sum
        if(y_sum.gt.y_max) y_max=y_sum
      enddo     

c      write(*,*)'in calculate_lamda_k,y_min,y_max',
c     &                                y_min,y_max

c      write(*,*)'in calculate_lamda_k i_coag_type',i_coag_type

      if (i_coag_type.eq.1) then
c--------1) Constant kernel K=const=coag_Kernel_0 
         dk_max=coag_Kernel_0
      elseif(i_coag_type.eq.2) then
c--------2) Brownian kernel
         dk_max=4.*sqrt(2.0)*coag_Kernel_0*y_max**(2.d0/3.d0)/
     &          sqrt(y_min)
      endif

c-----for K=(0.5*(g(u)*h(v)+h(u)*g(v))
c     maxK{i,j}=max(h(y(j)))*max(g(y(j)))

      dlabmda_k=Q0*dk_max/y_min

c       write(*,*)'in calculate_lamda coag_Kernel_0,Q0,dk_max,dlabmda_k'
c     &                              ,coag_Kernel_0,Q0,dk_max,dlabmda_k

      return
      end


     
   
     
