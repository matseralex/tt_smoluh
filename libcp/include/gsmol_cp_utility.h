#ifndef GSMOL_CP_UTILITY_H
#define GSMOL_CP_UTILITY_H

#include<complex>
#include<mkl_dfti.h>

//============================================================================//
//    Positive integer power of complex number                                //
//============================================================================//
// return: base ^ degree
std::complex<double> pow(
    // in:
    const std::complex<double> & base,
    // in:
    const int & degree
);

//============================================================================//
//    Factorial                                                               //
//============================================================================//
// return: n!
int fact(
    // in:
    const int & n
);

//============================================================================//
//    General convolution                                                     //
//============================================================================//
void gconv(
    // in: convolution arity
    const int & d,
    // in: logical size of argument
    const int & N,
    // in: number of significant arguments
    const int & argc,
    // in: significant arguments
    double * argv,
    // out: result, aux can be used as res
    double * res,
    // alt: FFT auxiliary memory
    std::complex<double> * aux,
    // alt: FFT descriptor
    DFTI_DESCRIPTOR_HANDLE & hand
);

//============================================================================//
//    Analytical solution for Cauchy-Smoluchowski problem                     //
//    with constant kernel and monodisperse initial condition                 //
//============================================================================//
// return: full concentration of solution
double trivial_compute(
    // in: moment of time
    const double & time,
    // in: kernel dimension
    const int & D,
    // in: number of equations
    const int & N,
    // out: concentrations(time)
    double * res
);

//============================================================================//
//    Euclid metrics                                                          //
//============================================================================//
// return: ||arg1 - arg2||_2
double euclid_metrics(
    // in: size of vectors 
    const int & N,
    // in: 
    const double * arg1,
    // in: 
    const double * arg2
);

#endif // GSMOL_CP_UTILITY_H

