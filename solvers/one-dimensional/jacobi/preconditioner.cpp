#include "preconditioner.hpp"
#include <mkl.h>
#include <iostream>

namespace Jacobi {

void Preconditioner::apply(double *x) const {
  x[0] *= D;
  band.apply(x + 1, true);

  std::vector<double> tmp(rank);
  cblas_dgemv(CblasColMajor, CblasTrans, n, rank, 1.0, Y.data(), n, x, 1, 0.0,
              tmp.data(), 1);
  LAPACKE_dgetrs(LAPACK_COL_MAJOR, 'N', rank, 1, C.data(), rank, ipiv.data(),
                 tmp.data(), rank);
  cblas_dgemv(CblasColMajor, CblasNoTrans, n, rank, -1.0, X.data(), n,
              tmp.data(), 1, 1.0, x, 1);
}

}
