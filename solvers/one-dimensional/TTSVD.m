function [cores] = TTSVD(T, tolerance, max_rank)
% TT-SVD for dense tensor T
    sizes = size(T);
    dimensionality = numel(sizes);
    nrm = norm(T(:))^2;
    cores = cell(dimensionality, 1);
    prev_rank = 1;
    sum = 0;
    for d = 1:dimensionality-1
        T = reshape(T, prev_rank * sizes(d), prod(sizes(d + 1:end)));
        m = size(T, 1);
        n = size(T, 2);
        [U, S, T] = svd(T, 'econ');
        rank = min(m, n);
        if (tolerance > 0)
            tmp_sum = 0;
            while tmp_sum + S(rank, rank)^2 < (tolerance^2 * nrm - sum) / (dimensionality - d)
                tmp_sum = tmp_sum + S(rank, rank)^2;
                rank = rank - 1;
            end
        end
        if (max_rank > 0)
            rank = min(rank, max_rank);
        end
        cores{d} = reshape(U(:, 1:rank), [prev_rank, sizes(d), rank]);
        T = S(1:rank, 1:rank) * T(:, 1:rank)';
        sum = sum + norm(S(rank + 1:end, rank + 1:end), 'fro')^2;
        prev_rank = rank;
    end
    cores{dimensionality} = reshape(T, [prev_rank, sizes(dimensionality), 1]);
end

