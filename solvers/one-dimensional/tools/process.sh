#!/bin/bash

_m="0"
_a="0.0 0.1"
_lambda="5e-2"
_cond="1e-2"

for m in $_m
do
    for a in $_a
    do
        for lambda in $_lambda
        do
            for cond in $_cond
            do
		DATAFILE="converg_"$m"_"$a"_"$lambda"_"$cond".dat"
		if [ -f "$DATAFILE" ]
		then
		    PROCESSEDFILE="converg_"$m"_"$a"_"$lambda"_"$cond".log"
		    awk '{if ($1 == "iter") {print $3" "$6}}' $DATAFILE >$PROCESSEDFILE
		fi
	    done
        done
    done
done
