import numpy as np
from math import *
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
def colm_analyt(a, _lambda, data):
    Moment_a = 0.0
    Moment_minus_a = 0.0
    N = data.shape[0]
    for i in xrange(N):
        Moment_a = Moment_a + (i + 1)**a * data[i]
        Moment_minus_a = Moment_minus_a + (i + 1)**(-a) * data[i]
    analyt = np.zeros(N)
    CONST = sqrt ( Moment_a * Moment_minus_a / (4.0 * pi) * (1.0 - 4.0 * a * a) * cos(pi * a) )
    beta = 2.0 / (1.0 + 2.0 * a)
    for i in xrange(N):
        analyt[i] = (i + 1)**(-1.5) * exp(-_lambda**beta * (i ))
    analyt = analyt * CONST
    return analyt;


def our_analyt(a, _lambda, data):
    Moment_a = 0.0
    Moment_minus_a = 0.0
    N = data.shape[0]
    for i in xrange(N):
        Moment_a = Moment_a + (i + 1)**a * data[i]
        Moment_minus_a = Moment_minus_a + (i + 1)**(-a) * data[i]
    analyt = np.zeros(N)
    CONST = sqrt ( Moment_a * Moment_minus_a / (4.0 * pi) * (1.0 - a * a) * cos(pi * a / 2.0) )
    beta = 2.0 / (1.0 + a)
    for i in xrange(N):
        analyt[i] = (i + 1)**(-1.5) * exp(-_lambda**beta * (i ))
    analyt = analyt * CONST
    return analyt;


a_list = ["0.0", "0.1"]#, "0-1", "0-2", "0-3", "0-4"]#,"0-5" , "0-6" ,"0-7"]
lambda_list = ["5e-2"]#, "4e-2", "3e-2", "2e-2", "1e-2", "5e-3", "4e-3", "3e-3", "2e-3", "1e-3", "5e-4", "3e-4", "2e-4"]
i=1
_a=0.0
m="0"
for a in a_list:
    pp = PdfPages('multipage_'+a+'.pdf')
#    if a == "0-3":
#        lambda_list = ["2e-2", "1e-2", "5e-3", "4e-3", "3e-3", "2e-3", "1e-3", "5e-4"]#"3e-4", "2e-4"]
#    if a=="0-4":
#        lambda_list = ["2e-4"]
    for _lambda in lambda_list:
        plt.figure()
        plt.ylim(1e-12,1.0)
        plt.yscale('log')
        plt.xlabel('k')
        plt.ylabel('n_k')
        plt.xscale('log')
        plt.title('a='+str(_a)+', lambda = '+_lambda)
        data_str = "data_"+m+"_"+a+"_"+_lambda+"_1e-2"
        DATA = np.loadtxt(data_str+"/final.dat").transpose(1,0)
        print data_str, DATA.shape
        analytical = colm_analyt(_a, float(_lambda), DATA[1])
        upd_analytical = our_analyt(_a, float(_lambda), DATA[1])
        #DATA[1][0] = DATA[1][0]/2.0
        plt.plot(DATA[0], DATA[1], DATA[0], analytical , DATA[0], upd_analytical)
        plt.legend(['num', 'colm', 'our', 'm=1'], loc='upper right')
        i = i+1
        pp.savefig()
        plt.clf()
    pp.close()
    _a += 0.1
