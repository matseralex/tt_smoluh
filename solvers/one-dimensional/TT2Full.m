function [T] = TT2Full(TT)
% TT-cores to full tensor
    sizes = zeros(1, numel(TT));
    dimensionality = numel(sizes);
    T = reshape(TT{1}, size(TT{1}, 2), size(TT{1}, 3));
    sizes(1) = size(T, 1);
    prod = sizes(1);
    for d = 2:dimensionality
        sizes(d) = size(TT{d}, 2);
        prod = prod * sizes(d);
        T = reshape(T * reshape(TT{d}, [size(TT{d}, 1), size(TT{d}, 2) * size(TT{d}, 3)]), [prod, size(TT{d}, 3)]);
    end
    T = reshape(T, sizes);
end

