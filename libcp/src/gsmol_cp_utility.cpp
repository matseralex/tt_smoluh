#include<cmath>
#define MKL_Complex16 std::complex<double>
#include<mkl_types.h>

#include"../include/gsmol_cp_blas.h"
#include"../include/gsmol_cp_utility.h"

//============================================================================//
//    Positive integer power of complex number                                //
//============================================================================//
// return: base ^ degree
std::complex<double> pow(
    // in:
    const std::complex<double> & base,
    // in:
    const int & degree
) {
    if (degree == 0)
        return 1.;
    else if (degree == 1)
        return base;
    else
        return pow(base, degree / 2) * pow(base, (degree + 1) / 2);
}

//============================================================================//
//    Factorial                                                               //
//============================================================================//
// return: n!
int fact(
    // in:
    const int & n
) {
    if (n == 1)
        return 1;
    else
        return n * fact(n - 1);
}

//============================================================================//
//    General convolution                                                     //
//============================================================================//
void gconv(
    // in: convolution arity
    const int & d,
    // in: logical size of argument
    const int & N,
    // in: number of significant arguments
    const int & argc,
    // in: significant arguments
    double * argv,
    // out: result, aux can be used as res
    double * res,
    // alt: FFT auxiliary memory
    std::complex<double> * aux,
    // alt: FFT descriptor
    DFTI_DESCRIPTOR_HANDLE & hand
) {
    MKL_LONG status;

    const int size = N << 1;
    std::complex<double> * img = aux + (N + 2);

    //======================================//
    //    FFT for nontrivial multipliers    //
    //======================================//
    // img = fft(arg[1])
    status = DftiComputeForward(hand, argv, img);

    for (int k = 1; k < argc - 1; ++k)
    {
        // aux = fft(arg[k])
        status = DftiComputeForward(hand, argv + (k * size), aux);

        // img x= aux
#pragma omp parallel for
        for (int i = 0; i < N + 2; ++i)
            img[i] *= aux[i];
    }

    //===================================//
    //    FFT for trivial multipliers    //
    //===================================//
    if (argc == 1)
    {
        // img = img ^x d
#pragma omp parallel for
        for (int i = 0; i < N + 2; ++i)
            img[i] = pow(img[i], d);
    }
    else
    {
        // aux = fft(arg[argc - 1])
        status = DftiComputeForward(hand, argv + ((argc - 1) * size), aux);

        // img x= aux ^x (d - argc + 1)
#pragma omp parallel for
        for (int i = 0; i < N + 2; ++i)
            img[i] *= pow(aux[i], d - argc + 1);
    }

    //============//
    //    IFFT    //
    //============//
    // res = ifft(img)
    status = DftiComputeBackward(hand, img, res);
    
    // normalize res
    scal(N, 1. / size, res, 1);

    return;
}

//============================================================================//
//    Analytical solution for Cauchy-Smoluchowski problem                     //
//    with constant kernel and monodisperse initial condition                 //
//============================================================================//
// return: full concentration of solution
double trivial_compute(
    // in: moment of time
    const double & time,
    // in: kernel dimension
    const int & D,
    // in: number of equations
    const int & N,
    // out: concentrations(time)
    double * res
) {
    // res = 0
    res[0] = 0.;
    copy(N, res, 0, res, 1);

    int incn = D - 1; 
    double freqn = 1. / (double)incn;
    double g = 1. / tgamma(freqn);

    // zero moment of res computation
    double m0 = pow(1. + (double)(incn * incn) * time / (double)fact(D), -freqn);

    if (D == 2)
    {
#pragma omp parallel for
        for (int i = 0; i <= N / incn; ++i)
        {
           res[i * incn] = pow(m0, (double)D * freqn) * pow(1. - m0, (double)i);

           if (std::isnan(res[i * incn]))
                res[i * incn] = 0.;
        }
    } 
    else
    {
#pragma omp parallel for
        for (int i = 0; i <= N / incn; ++i)
        {
           res[i * incn] = pow(m0, (double)D * freqn) * pow(1. - m0, (double)i)
               * tgamma((double)i + freqn) * g / tgamma((double)(i + 1));

           if (std::isnan(res[i * incn]))
                res[i * incn] = 0.;
        }
    }

    return m0;
}

//============================================================================//
//    Euclid metrics                                                          //
//============================================================================//
// return: ||arg1 - arg2||_2
double euclid_metrics(
    // in: size of vectors 
    const int & N,
    // in: 
    const double * arg1,
    // in: 
    const double * arg2
) {
    double value = 0.;

#pragma omp parallel for reduction(+:value)
    for (int i = 0; i < N; ++i)
    {
        value += (arg1[i] - arg2[i]) * (arg1[i] - arg2[i]);
    }

    return sqrt(value);
}

