#include <stdio.h>
#include <cstdlib>
#include <mkl/mkl.h>
#include <cmath>
#include <cstring>
#include <omp.h>
#include "parallel_cross_omp.h"
#include "tensor_train.h"
#include "gsmol_tt_kernel.h"

using namespace std;

//============================================================================//
//    Main                                                                    //
//============================================================================//
int main(int argc, char ** argv)
{
    if (argc < 2)
    {
        printf(
            "=============================================================="
            "==================\n"
        );

        printf(
            "Required arguments:\n"
            "(1) kernel_name\n"
            "(2) collisions_type\n"
            "(3) dimensionality\n"
            "(4) weights in front of kernels\n"
            "(5) mode_size\n"
            "(6) number_iterations\n"
            "(7) dt\n"
            "(8) tolerance\n"
            "(9) filename_time\n"
            "(10) filename_result\n"
            "(11) filename_ranks\n"
        );

        printf(
            "=============================================================="
            "==================\n"
        );

        printf(
            "Tips: kernel_name"
            "== {const, sumsqrt, genprod, gensum, ibrae}\n"
            "Tips: kernel_name = const, otherwise\n"
            "Tips: ibrae == const, if dimensionality > 3\n"
        );

        printf(
            "=============================================================="
            "==================\n"
        );

        printf(
            "Tips: collisions_type"
            "== all   --   all collisions till dimensionality\n"
            "Tips: collisions_type"
            "!= all   --   only dimensionality-ary collisions\n"
        );

        printf(
            "=============================================================="
            "==================\n"
        );

        printf(
            "Tips: './log' file to be appended if present,"
            "to be generated otherwise\n"
        );

        printf(
            "=============================================================="
            "==================\n"
        );

        return 1;
    }

    char kname[256];
    char ktype[256];
    int D;
    int A;
    int N;
    int T;
    double dt;
    double tol;
    char ft_name[256];
    char fres_name[256];
    char frk_name[256];

    // read parameters from input file on root
    FILE * in = fopen(argv[1], "r");
    
    fscanf(in, "%s\n", kname);
    fscanf(in, "%s\n", ktype);
    fscanf(in, "%d\n", &D);
    A = (!strcmp(ktype, "all"))? D - 1: 1;

    double w[A];
    for (int a = 0; a < A; ++a)
    {
        fscanf(in, "%lf", w + a);
    }

    fscanf(in, "\n");

    fscanf(in, "%d\n", &N);
    fscanf(in, "%d\n", &T);
    fscanf(in, "%lf\n", &dt);
    fscanf(in, "%lf\n", &tol);
    fscanf(in, "%s\n", ft_name);
    fscanf(in, "%s\n", fres_name);
    fscanf(in, "%s\n", frk_name);

    fclose(in);

    if (N < 1 || D < 2 || T < 1)
    {
        return 1;
    }

    // initialize kernel
    TKernel_TT * kernel = new TKernel_TT(
        kname, ktype, D, N, w, tol
    );

    // allocate auxiliary memory
    double * u = (double *)malloc(N * sizeof(double));
    double * v = (double *)malloc(N * sizeof(double));

    //======================================//
    //    Monodisperse initial condition    //
    //======================================//
    double * n = (double *)calloc(N, sizeof(double));
    n[0] = 1.;

    //===========================//
    //    Predictor-corrector    //
    //===========================//
    double t_scheme = omp_get_wtime();

    for (int k = 0; k < T; ++k) 
    {
        kernel->compute(n, u);
        
        // u *= 0.5 * dt
        scal(N, 0.5 * dt, u, 1);
        // u += n
        axpy(N, 1., n, 1, u, 1);

        kernel->compute(u, v);

        // n += dt * v
        axpy(N, dt, v, 1, n, 1);
    }

    t_scheme = omp_get_wtime() - t_scheme;

    double t_appr = kernel->get_t_appr();
    double t_compress = kernel->get_t_compress();
    double t_overall = t_appr + t_compress + t_scheme;

    // mass
    double m = 0.;
    for (int i = 0; i < N; ++i)
    {
        m += (i + 1) * n[i];
    }
    printf("m = %.16e\n", m);

    //==================================//
    //    Write times & ranks >> log    //
    //==================================//
    FILE * f = fopen("log", "a");
    fprintf(
        f, "\nc %s %d-%s %d\ntol: %.8f\nt: %d %.8f %d\ntimes: %.2f %.2f %.2f "
        "%.2f\nranks_appr: ",
        kname, D, (strcmp(ktype, "all"))? "only": "all", N, tol,
        (int)rint(dt * T), dt, T, t_appr, t_compress, t_scheme, t_overall
    );

    for (int d = 1; d < D; ++d)
    {
        fprintf(f, "%d ", kernel->get_rank_appr(d));
    }

    fprintf(f, "\nranks_compress: ");

    for (int d = 1; d < D; ++d)
    {
        fprintf(f, "%d ", kernel->get_rank(d));
    }

    fprintf(f, "\n");

    fclose(f);

    //=============================//
    //    Write times > ft_name    //
    //=============================//
    f = fopen(ft_name, "a");

    fprintf(f, "\n%d-%s %d tol: %.8f\ntimes: %.2f %.2f %.2f %.2f\n", D,
        (strcmp(ktype, "all"))? "only": "all", N, tol,
        t_appr, t_compress, t_scheme, t_overall
    );

    fclose(f);

    //=================================//
    //    Write results > fres_name    //
    //=================================//
    f = fopen(fres_name, "w");

    for (int i = 0; i < N; ++i)
    {
        fprintf(f, "%d %.15f\n", i, n[i]);
    }

    fclose(f);

    //==============================//
    //    Write ranks > frk_name    //
    //==============================//
    f = fopen(frk_name, "w");

    fprintf(
        f, "\ns %s %d-%s %d\ntol: %.8f\nt: %d %.8f %d\nranks_appr: ", kname, D,
        (strcmp(ktype, "all"))? "only": "all", N, tol, (int)rint(dt * T), dt, T
    );

    for (int d = 1; d < D; ++d)
    {
        fprintf(f, "%d ", kernel->get_rank_appr(d));
    }

    fprintf(f, "\nranks_compress: ");

    for (int d = 1; d < D; ++d)
    {
        fprintf(f, "%d ", kernel->get_rank(d));
    }

    fprintf(f, "\n");

    fclose(f);

    //=========================//
    //    Deallocate memory    //
    //=========================//
    delete kernel;

    free(n);
    free(u);
    free(v);

    return 0;
}
