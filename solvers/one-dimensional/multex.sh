#!/bin/bash

for kname in "genprod";
do
    for ktype in "only";
    do
        for D in 5;
        do
            for time in 1;
            do
                for iter_per_one in 10;
                do
                    T=$(( $time * $iter_per_one ))
                    dt=$( echo 0$( bc <<< "scale=4; 1. / $iter_per_one" | sed -e 's/[0]*$//g' ) )

                    for tol in "1e-5";
                    do
                        for N in 131072;
                        do
                            echo bash singex.sh $kname $ktype $D $N $T $dt $tol \
                                "times/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "ranks/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol
                            bash singex.sh $kname $ktype $D $N $T $dt $tol \
                                "times/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "ranks/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol
                        done
                    done
                done
            done
        done
    done
done

for kname in "genprod";
do
    for ktype in "only";
    do
        for D in 5;
        do
            for time in 1;
            do
                for iter_per_one in 10;
                do
                    T=$(( $time * $iter_per_one ))
                    dt=$( echo 0$( bc <<< "scale=4; 1. / $iter_per_one" | sed -e 's/[0]*$//g' ) )

                    for tol in "1e-7";
                    do
                        for N in 16384 32768 65536 131072;
                        do
                            echo bash singex.sh $kname $ktype $D $N $T $dt $tol \
                                "times/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "ranks/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol
                            bash singex.sh $kname $ktype $D $N $T $dt $tol \
                                "times/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
                                "ranks/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol
                        done
                    done
                done
            done
        done
    done
done


#for kname in "gensum";
#do
#    for ktype in "only";
#    do
#        for D in 2 3 4 5 6 7;
#        do
#            for time in 1;
#            do
#                for iter_per_one in 10;
#                do
#                    T=$(( $time * $iter_per_one ))
#                    dt=$( echo 0$( bc <<< "scale=4; 1. / $iter_per_one" | sed -e 's/[0]*$//g' ) )
#
#                    for tol in "1e-6";
#                    do
#                        for N in 16384 32768 65536 131072;
#                        do
#                            echo bash singex.sh $kname $ktype $D $N $T $dt $tol\
#                                "times/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol\
#                                "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol\
#                                "ranks/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol
#                            bash singex.sh $kname $ktype $D $N $T $dt $tol \
#                                "times/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
#                                "results/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol \
#                                "ranks/s_"$kname"_"$D"-"$ktype"_"$N"_"$time"_"$dt"_"$T"_"$tol
#                        done
#                    done
#                done
#            done
#        done
#    done
#done
#
