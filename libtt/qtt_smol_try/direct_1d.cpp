#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <mkl/mkl.h>
#include <cmath>
#include <cstring>
#include <sys/time.h>
using namespace std;

double K(const double & u, const double &v)
{
    double u1 = u + 1.;
    double v1 = v + 1.;
    return pow(u1/v1, 0.3l) + pow(v1/u1, 0.3l);
	return 2.0;

	return pow(pow(u1,1.0l/3)+pow(v1, 1.0l/3),2)*sqrt((1.0l/u1 + 1.0l/v1));
}


double L1(const int &N, const int &i, const double *n)
{
    double l1 = 0;
    if (i == 0)
        return 0.0;

    for (int i1 = 0; i1 < i; i1++)
    {
        l1 += n[i1] * n[i - i1 - 1] * K((i - i1 - 1) , i1);
    }
    l1 *= 1./2.;
    return l1;
}

double L2(const int &N, const int &i, const double *n)
{
    double l2 = 0;
    for (int i1 = 0; i1 < N; i1++)
    {
        l2 += n[i1] * K(i , i1);
    }
    l2 = l2 * n[i];
    return l2;
}

double Source(const int &i)
{
    if (i == 0)
        return 1.0;
    else 
        return 0.0;

}

double mass(const int &N, const double *n)
{
    double l2 = 0.;
    for (int i1 = 0; i1 < N; i1++)
    {
        l2 += (i1+1) * n[i1];
    }
    return l2;
}

double start_cond(const double &v)
{
//    if (v == 0)
//        return 1.0;
//    else 
//        return 0.0;
    return pow(v+1., -2.5l);
}

double analitycal_solution(const double &v, const double & t)
{
    return (1.0 / ((1.0 + t / 2.0) * (1.0 + t / 2.0))) * exp(-v / (1.0 + t / 2.0));
}

int main(int argc, char** argv)
{
	if (argc != 5)
	{
		cout << "4 parameters have been expected" << endl;
		cout << "n_steps time_step N_eq ifpaint"<< endl;
		return -1;
	}
	int N = 101, steps = 100;
	double time_step = 0.01;
	char name_of_file[15] = "values.dat";
	char name_of_picture[30];
	FILE* values;
	int ifpaint = 0;
	struct timeval start, end;

	sscanf(argv[1], "%d", &steps);
	sscanf(argv[2], "%lf", &time_step);
	sscanf(argv[3], "%d", &N);
	sscanf(argv[4], "%d", &ifpaint);
	double *n_k, *n_k_half, *n_k_one, h, *n_analit, *S;
	double *L1_res_vec, *L2_res_vec, *L1_correction, *L2_correction;

	n_k = (double *) malloc(N*sizeof(double));
	n_k_half = (double *) malloc(N*sizeof(double));
	n_k_one = (double *) malloc(N*sizeof(double));
	n_analit = (double *) malloc(N*sizeof(double));
	S = (double *) malloc(N*sizeof(double));

	if (ifpaint)
		system("cd IMG; rm *.png; cd ..");

	for (int i = 0; i < N; i++)
	{
		n_k[i] = start_cond(i);
	}

	gettimeofday(&start, NULL);	

	for (int k = 0; k < steps; k++)
	{

		for (int i = 0; i < N; i++)
		{
    	    n_k_one[i] = ( L1(N, i, n_k) - L2(N, i, n_k) + Source(i) ) * time_step + n_k[i];
            //printf("%lf \n", n_k_one[i]);
		}
        //printf("--------------------------------------------\n");

		if (ifpaint)
		{
			system("gnuplot plotting_script1d");
			sprintf(name_of_picture, "mv 1.png IMG/file%03d.png",k+1);
			system(name_of_picture);
			cout<<"saving image data about the step..."<<endl;
			fclose(values);
		}

		double * tmp = n_k_one;
		n_k_one = n_k;
		n_k = tmp;
		tmp = NULL;
	}
	gettimeofday(&end, NULL);

	double r_time = end.tv_sec - start.tv_sec + ((double)(end.tv_usec - start.tv_usec)) / 1000000;

    //printf("after direct time integration\n");
    for (int i = 0; i < N; i++)
        printf("%d %lf\n", i+1, n_k[i]);
    
	//cout << "Average time for step = " << r_time / steps << " seconds" << endl;
	cout << "Full time = " << r_time << "seconds" << endl;
	free(n_k);
	free(n_k_half);
	free(n_k_one);
	free(n_analit);
}
