#include "jacobi.hpp"
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>

class TKernel : public TMatrix {
private:
  double a;

public:
  TKernel(int n, double a) : TMatrix(n, n), a(a) {}
  double value(const int &i, const int &j) {
    double u = i + 1.0;
    double v = j + 1.0;
    double u_a = pow(u, a), v_a = pow(v, a);
    return u_a / v_a + v_a / u_a;
  }
};

int main(int argc, char **argv) {
  --argc;
  ++argv;
  int const N = argc < 1 ? 131072 : std::stoi(*argv);
  --argc;
  ++argv;
  double const a = argc < 1 ? 0.0 : std::stod(*argv);
  --argc;
  ++argv;
  double const lambda = argc < 1 ? 0.0 : std::stod(*argv);
  --argc;
  ++argv;
  double const J = argc < 1 ? 1.0 : std::stod(*argv);
  --argc;
  ++argv;
  int const p = argc < 1 ? 1 : std::stoi(*argv);
  --argc;
  ++argv;
  int const fgmres_iters = argc < 1 ? 80 : std::stoi(*argv);
  --argc;
  ++argv;
  int const fgmres_norestart_iters = argc < 1 ? 80 : std::stoi(*argv);
  --argc;
  ++argv;
  std::string default_folder =
      "jacobi_" + std::to_string(N) + "_" + std::to_string(a) + "_" +
      std::to_string(lambda) + "_" + std::to_string(J) + "_" +
      std::to_string(p) + "_" + std::to_string(fgmres_iters) + "_" +
      std::to_string(fgmres_norestart_iters);
  std::string const folder = argc < 1 ? default_folder : *argv;
  --argc;
  ++argv;

  if (mkdir(folder.c_str(), 0777) != 0)
    return errno;

  std::ofstream rlog(folder + "/residues.log");
  std::ofstream ilog(folder + "/iterations.log");
  std::ofstream slog(folder + "/solution");
  std::ofstream info_log(folder + "/info.log");

  info_log << "N =\t" << N << std::endl;
  info_log << "a =\t" << a << std::endl;
  info_log << "λ =\t" << lambda << std::endl;
  info_log << "J =\t" << J << std::endl;
  info_log << "p =\t" << p << std::endl;
  info_log << "fgmres iters = \t" << fgmres_iters << std::endl;
  info_log << "fgmres norestart iters =\t" << fgmres_norestart_iters
           << std::endl;

  TCross_Parallel_v1_Parameters parameters;
  TKernel kernel(N, a);
  TCross_Parallel_v1 kernel_cross;

  parameters.tolerance = 1e-8;
  parameters.maximal_iterations_number = 0;
  kernel_cross.Approximate(&kernel, parameters);

  std::vector<double> n(N), S(N);
  std::fill(n.begin(), n.end(), 0.0);
  std::fill(S.begin(), S.end(), 0.0);

  std::fill(n.begin(), n.begin() + std::min<int>(N, 100), 1.0 / 5050.0);

  auto start = std::chrono::steady_clock::now();
  int iter = newton_smoluchowski(
      N, [&kernel](int i, int j) { return kernel.value(i, j); }, kernel_cross,
      lambda, J, p, 64, n.data(), rlog, ilog, 1e-12, fgmres_iters,
      fgmres_norestart_iters);
  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = end - start;
  info_log << iter << std::endl;

  std::copy(n.begin(), n.end(), std::ostream_iterator<double>(slog, "\n"));

  smoluchowski(N, [&kernel](int i, int j) { return kernel.value(i, j); },
               kernel_cross, lambda, J, n.data(), S.data());

  info_log << nrm2(N, S.data(), 1) << std::endl;
  info_log << diff.count() << " s" << std::endl;

  double const pi = 3.141592653589793;
  double const C = std::sqrt(J * (1 - 4 * a * a) * std::cos(pi * a) / (4 * pi));
  for (int i = 0; i < N; ++i)
    n[i] = C * std::pow(i + 1, -1.5);
  smoluchowski(N, [&kernel](int i, int j) { return kernel.value(i, j); },
               kernel_cross, lambda, J, n.data(), S.data());
  info_log << nrm2(N, S.data(), 1) << std::endl;

  return 0;
}
