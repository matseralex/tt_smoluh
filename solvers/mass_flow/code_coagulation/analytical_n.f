      subroutine analytical_solution(time,total_n,total_nv)
c---------------------------------------------------
c     Calculate analytical solition rn_2d_analyt(i,j) (i=0:Nx,j=0:Ny)
c     at constant kernal K=K0=const and the initial concentration
c     n(v1,v2,t-0)=N_0/(v1_0*v2_0)*exp(-(v1/v10+v2/v20)))
c
c     Calculate total number of particles:  total_n
c     and total volume: total_nv=
c     using numerical integration of the analytical concentration 
c  
c---------------------------------------------------
      use One_NmlMod
      use One_NoNmlMod 
      implicit none
     
c-----input
      real*8 
     &time
c-----output
c     rn_2d_analyt(0:Nx,0:Ny) !analytical concentation
c                             !It will be in One_NoNmlMod
      real*8
     &total_n,                !total number of particles
     &total_nv                !total volume

c-----locals
      real, dimension(1:3) ::  bslr,bsli
      real :: rarg
      integer :: izi,nb,ncalc

c     izi=1 to calculate modified Bessel functions
c     BESSEL FUNCTIONS AND ARGUMENTS ARE REAL

      real*8 :: tau_arg,rI_0

      integer :: i,j

c-----externals
c     subroutine beslci to calculate modified bessel function

      write(*,*)'analytical_solution time',time
c      write(*,*)'v_x(0)',v_x(0)
c      write(*,*)'v_x(1)',v_x(1)
c      step_x=v_x(1)-v_x(0)
c      step_y=v_y(1)-v_y(0)

      write(*,*)'step_x,step_y', step_x,step_y

      total_n=0.d0
      total_nv=0.d0

      izi=1
      nb=1 
      tau_arg=0.5d0*rK_0*rN_0*time

      do i=0,Nx
        do j=0,Ny
           rarg=2.d0*dsqrt(tau_arg*v_x(i)*v_y(j)/
     &                ((1.d0+tau_arg)*v_x_0*v_y_0))
           call beslci(rarg,0.,nb,izi,bslr,bsli,ncalc)
           rI_0=bslr(1)  !modifed bessel function 0 order  
           rn_2d_analyt(i,j)=rN_0/(v_x_0*v_y_0*(1+tau_arg)**2)*
     &                  dexp(-v_x(i)/v_x_0-v_y(j)/v_y_0)*rI_0 
           total_n=total_n+rn_2d_analyt(i,j) 
           total_nv=total_nv+rn_2d_analyt(i,j)*(v_x(i)+v_y(j))
        enddo
      enddo

      total_n=total_n*step_x*step_y
      total_nv=total_nv*step_x*step_y

      return
      end      

      
