      subroutine rejected_accepted(N_particle,N_species,
     &w_x,w_x_prime,v0_vector,
     &v_particle_max_vector,v_particle_min_vector,y_ar)
c----------------------------------
c     find points y_ar(j,i),
c     j=1,...,N_species, i=1,...,N_particle, 
c     according to distribution of probability 
c     w_x(x_vector,N_species) x_vectro(j=1,...,,N_species)
c     w_x_prime(x) is the estimation of w_x(x)
c     w_x(x) <  w_x_prime(x) 
c---------------------------------
      implicit none
c-----input
      integer  ::
     & N_particle,             ! number of particles 
     & N_species                ! number of species

      real*8, dimension(1:N_species) :: v_particle_max_vector,
     &v_particle_min_vector,        !normalized to v_ch
                                   !max and min aerosol volumes       
     & v0_vector        !paramers of initial distribution

c-----used functions
      real*8 w_x,w_x_prime
c-----external
      external w_x,wx_prime

c-----output 
      real*8, dimension (1:N_species,1:N_particle) :: y_ar 
                                                    !volumes of particles 

c-----locals
      integer, parameter :: seed=86456            

      real*8 :: w_prime,v,y,ymax,sum,sum_anal
      real*8 :: rand,k

      real*8, dimension (1:N_particle) :: v_vector
      
      integer :: i,j
c------------
      write(*,*)'in  subroutine rejected_accepted'
      write(*,*)'N_particle,N_species',N_particle,N_species
      do j=1,N_species
         write(*,*)'j,v0_vector(j)',j,v0_vector(j)
         write(*,*)'v_particle_max_vector(j),v_particle_min_vector(j)'
     &             ,v_particle_max_vector(j),v_particle_min_vector(j)
      enddo

      call srand(seed)

      i=0
      do while (i.le.N_particle-1)

c--------------------------------------------------------------
c        create random v_vector(j=1,...,N_species)
c-------------------------------------------------------------

c         write(*,*)'i',i,'N_species',N_species

         do j=1,N_species
          
           call random_number(rand)
           v_vector(j)=v_particle_min_vector(j)+
     &       (v_particle_max_vector(j)-v_particle_min_vector(j))*rand

c           write(*,*)' v_vector(j)',v_vector(j)

         enddo !j

c--------------------------------------------------------------
c        calculate value of density distribution w_x  at argument
c        v_vector
c-------------------------------------------------------------
c         write(*,*)'before  y=w_x'

         y=w_x(N_species,v0_vector,v_particle_min_vector,
     &         v_particle_max_vector,v_vector)

c         write(*,*)'y',y
c---------------------------------------------------------------
c        calculate over estimation for distributin w_x
c-------------------------------------------------------------- 
c         write(*,*)'before  y_max=w_x_prime'

         ymax=w_x_prime(N_species,v0_vector,
     &     v_particle_min_vector,v_particle_max_vector,v_vector)

c        write(*,*)'ymax',ymax
c--------------------------------------------------------------
c        check the condition for rejection/acception algorithm
c--------------------------------------------------------------
   
         call random_number(rand)

c         write(*,*)'i,y,ymax.rand.y/ymax',i,y,ymax,rand,y/ymax

         if(rand.lt.y/ymax) then
            i=i+1
            do j=1,N_species
               y_ar(j,i)= v_vector(j)
c               write(*,*)'j,i,y_ar(j,i)',j,i,y_ar(j,i)
            enddo
         endif
          
      enddo !while


     
      write(*,*)'before end of rejected_accepted'
  
      return
      end

      real*8 function w_x_prime(N_species,x0_vector,
     &v_particle_min_vector,v_particle_max_vector,x_vector)
c-----------------------------------
c     w_x_prime(x) is the estimation of w_x(x)
c     w_x(x) <  w_x_prime(x)
c--------------------------------------
      implicit none

c-----input 
      integer  ::
     & N_species   ! number of species
   
      real*8, dimension(1:N_species) :: 
     & x_vector,        !volumes
     & x0_vector        !paramers of distribution

       real*8, dimension(1:N_species) :: 
     & v_particle_min_vector,v_particle_max_vector
c-----locals
      integer :: j,k
      real*8 :: p,x0_max,power1,power2,q,q1


c-----externals
      real*8 Q_normalization_N_species
c-----find  x0_max = max{j}(x0_vector(j))      
      x0_max=0.d0
      do j=1,N_species
         if(x0_vector(j).gt.x0_max) x0_max=x0_vector(j)
      enddo

c-----new-----------      
      q=0.d0
      do k=1,N_species
         power1=0.d0 
         power2=0.d0
         do j=1,N_species
            if(j.ne.k) then
               power1=power1 + v_particle_min_vector(j)
               power2=power2 + v_particle_min_vector(j)/
     &                         x0_vector(j)
            endif
         enddo
         q1= x0_vector(k)*exp(-1.d0+power1/x0_vector(k)-power2)
         if(q.lt.q1) q=q1
      enddo

c-----find
c     Q_normalization_N_species=Product{j=1,,.,N_species}(v0_vector(j))*
c     Sum{j=1,,.,N_species}(v0_vector(j)*I2(j)*
c     product{m=1,..,N_species, m.ne,j}(I1(m)))

c      write(*,*)'before Q0_N_species ,N_species',N_species
c      do j=1,N_species
c         write(*,*)'j,v_particle_min_vector(j),v_particle_max_vector(j)'
c     &             ,j,v_particle_min_vector(j),v_particle_max_vector(j)
c      enddo
c      do j=1,N_species
c         write(*,*)'j,x0_vector(j)',j,x0_vector(j)
c      enddo

      p=Q_normalization_N_species(N_species,v_particle_min_vector,
     &v_particle_max_vector,
     &x0_vector)

c------
c      w_x_prime=1.01*x0_max*exp(-1.)/p 

      w_x_prime=1.01*q/p 
c      w_x_prime=1.01*q
   
      return
      end

      real*8 function w_x(N_species,v0_vector, 
     &v_particle_min_vector,v_particle_max_vector,
     &v_vector)
c-------------------------------------
c     distribution of probability w(x)
c--------------------------------------
      implicit none

c-----input 
      integer  ::
     & N_species   ! number of species

      real*8, dimension(1:N_species) :: 
     & v_vector,        !volumes
     & v0_vector,        !paramers of distribution
     & v_particle_min_vector,v_particle_max_vector !normalized
c-----locals
      real*8 :: arg,p,v_sum
      integer j
c-----externals
      real*8 Q_normalization_N_species

c      write(*,*)'in w_x N_species',N_species
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v0_vector',v0_vector
c      write(*,*)'v_vector',v_vector
    

c      do j=1,N_species
c         write(*,*)'j,v0_vector(j)',j,v0_vector(j)
c         write(*,*)'v_particle_min_vector(j),v_particle_max_vector(j)'
c     &             ,v_particle_min_vector(j),v_particle_max_vector(j)
c         write(*,*)'v_vector(j)',v_vector(j)
c      enddo

      v_sum=0.d0
      arg=0.0d0
     
 
      do j=1,N_species
         arg = arg + v_vector(j)/v0_vector(j)
         v_sum = v_sum +v_vector(j)
      enddo

c      write(*,*)'v_sum',v_sum

      p=Q_normalization_N_species(N_species,
     &v_particle_min_vector,v_particle_max_vector,v0_vector)
  
c       write(*,*)'p=Q0_N_species',p

      w_x=v_sum*exp(-arg)/p
c      w_x=v_sum*exp(-arg)

c      write(*,*)'v_sum,p=Q0_N_species,arg,w_x',
c     &           v_sum,p,arg,w_x

      return
      end

c-----for f() normalization

      real*8 function r_i2(v_min,v_max)
c-----calculate I2=exp(-v_min)*(1+v_min)-exp(-v_max)*(1+v_max) 
      implicit none
c-----input 
      real*8 ::    v_min,v_max
      r_i2=exp(-v_min)*(1+v_min)-exp(-v_max)*(1+v_max) 
      return
      end    

      real*8 function r_i1(v_min,v_max)
c-----calculate I1=exp(-v_min)-exp(-v_max) 
      implicit none
c-----input 
      real*8 ::    v_min,v_max
      r_i1=exp(-v_min)-exp(-v_max)
      return
      end  




      real*8 function Q_normalization_N_species(N_species,
     &v_particle_min_vector,v_particle_max_vector,v0_vector)

c-----calculate 
c     Q0=intergral{v_particle_min,v_particle_max}q_0(v_vector)d_v_vector
c     here d_v_vector=d_v_vector(1)*...*d_v_vector(N_species)
c
c     This integral is calculated analytically for 
c     q_0(v_vector)=exp{-Sum_(j=1...N_species)[v_vector(j)/v0_vectror(j)]
c
c     Q0_N_species=Product{j=1,,.,N_species}(v0_vector(j))*
c     Sum{j=1,,.,N_species}(v0_vector(j)*I2(j)*
c     product{m=1,..,N_species, m.ne,j}(I1(m)))
c
c    Q_normalization_N_species  is normalised to v_ch**(N_species+1)
c
      implicit none
c-----input 
      integer :: N_species 
      real*8, dimension(1:N_species) :: 
     &v_particle_min_vector,          !normaliszed to v_ch
     &v_particle_max_vector,
     & v0_vector
c-----locals
      integer j,m
      real*8 :: product_j,product_m,sum_j,v_min,v_max
c-----externals
      real*8 r_i1,r_i2

      product_j=1.d0
 
c      write(*,*)'in Q_normalization_N_species N_species',N_species

c      do j=1,N_species
c         write(*,*)'j,v_particle_min_vector(j),v_particle_max_vector(j)'
c     &             ,j,v_particle_min_vector(j),v_particle_max_vector(j)
c      enddo

c      do j=1,N_species
c         write(*,*)'j,v0_vector(j)',j,v0_vector(j)
c      enddo

      do j=1,N_species
         product_j=product_j*v0_vector(j)
      enddo

c       write(*,*)' product_j', product_j

      sum_j=0.d0
      do j=1,N_species

c         write(*,*)'j',j
 
        product_m=1.d0

        do m=1,N_species

c            write(*,*)'j,m',j,m

           if (m.ne.j) then
c             write(*,*)'1 j,m',j,m 
c          write(*,*)'v_particle_min_vector(m),v_particle_max_vector(m)'
c     &              ,v_particle_min_vector(m),v_particle_max_vector(m)
c             write(*,*)'v0_vector(m)',v0_vector(m)

             v_min = v_particle_min_vector(m)/v0_vector(m)
             v_max = v_particle_max_vector(m)/v0_vector(m)
             product_m = product_m*r_i1(v_min,v_max)

c             write(*,*)'v_min,v_max,r_i1(v_min,v_max),product_m ',
c     &                  v_min,v_max,r_i1(v_min,v_max),product_m 
           endif
 
        enddo

c        write(*,*)'product_m', product_m 
       
        v_min = v_particle_min_vector(j)/v0_vector(j)
        v_max = v_particle_max_vector(j)/v0_vector(j)

c        write(*,*)'j,v0_vector(j),v_min,v_max',
c     &             j,v0_vector(j),v_min,v_max



        sum_j = sum_j +v0_vector(j)*r_i2(v_min,v_max)*product_m 

c        write(*,*)'v0_vector(j),r_i2(v_min,v_max),product_m', 
c     &             v0_vector(j),r_i2(v_min,v_max),product_m

c        write(*,*)'sum_j',sum_j

      enddo

      Q_normalization_N_species = product_j*sum_j !normalised to
                                                  !v_ch**(N_species+1)
c      write(*,*)'product_j,sum_j,Q_normalization_N_species',
c     &           product_j,sum_j,Q_normalization_N_species

      return
      end     

      subroutine q0_calculation_2d( N_species, 
     &V_particle_min_vector,V_particle_max_vector,
     &N_integration,v0_vector,w_x,Q0_normalized)
c----------------------------------------------------------
c     calculate Q0=integral{v0,vmax}q0(v)dv
c
c     q0(v)=v_sum*n0(v)=v*w_x(x)
c------------------------------------------------------------
      implicit none

c-----input
      integer :: N_integration       !=2k number of integration points

      integer  :: 
     & N_species                ! number of species
      real*8, dimension(1:N_species) :: 
     &  v_particle_max_vector,
     &  v_particle_min_vector,        !normalized to v_ch
                                      ! max and min aerosol volumes     
     &  v0_vector                       !parametrs of initial distribution 
                                      !q(v,t=0)
      real*8 ::                                 
     & r_N_0                ! avegaredover system volume V_system 
                             ! number of aerosol particles 
                             ! at the initial time moment t=0                  
   

c------------------------------------------
      real*8 w_x      ! It is a function of
                    ! initial destribution of concentration n(v) 
      external w_x
c--------------------------------------------------
c-----output
      real*8 Q0_normalized     
c-----locals
      integer :: j,k1,k2
      real*8 Q0     
      real*8, dimension(1:N_species) :: step_vector,  !integration steps
     &                                v_vector
c------------------------------------------
      write(*,*)'in q0_calculation_2d'
      
c      write(*,*)'N_species',N_species 
c      write(*,*)'V_particle_min_vector',V_particle_min_vector
c      write(*,*)'V_particle_max_vector',V_particle_max_vector
c      write(*,*)'N_integration',N_integration
c      write(*,*)'v0_vector',v0_vector

      if (N_species.ne.2) then
          write(*,*)'in q0_calculation_2d N_species.ne.2'
          write(*,*)'N_species =',N_species 
          write(*,*)'it should be N_species=2'
          stop 'in q0_calculation_2d N_species.ne.2'
      endif

      Q0=0.d0

     
c-----for N_species=2 case

      do j=1,N_species
         step_vector(j)=
     &      (V_particle_max_vector(j)-V_particle_min_vector(j))/
     &      N_integration
      enddo
c      write(*,*)'step_vector',step_vector

      do k1=1,N_integration
         v_vector(1)=V_particle_min_vector(1)+step_vector(1)*(k1-0.5)

c         write(*,*)'k1,v_vector(1)',k1,v_vector(1)

         do k2=1,N_integration         
            v_vector(2)=V_particle_min_vector(2)+step_vector(2)*(k2-0.5)

c            write(*,*)'k2,v_vector(2)',k2,v_vector(2)


            Q0 = Q0+w_x(N_species,v0_vector, 
     &        v_particle_min_vector,v_particle_max_vector,v_vector)

c            write(*,*)'Q0',Q0

         enddo
       
      enddo

      Q0_normalized = Q0*step_vector(1)*step_vector(2)   

      write(*,*)'q0_calculation_2d  Q0,Q0_normalized',Q0,Q0_normalized

      return
      end
      
c=======================================

      subroutine analytical_Q0_for_exp_distribution(N_species,
     &v_particle_min_vector,v_particle_max_vector,v0_vector,
     &rN_0,Q0)
c--------------------------------------------------------------
c      Calculate analytically dimensional Q0 for the case
c      q~v_sum*exp(-v/v0)
c--------------------------------------------------------------
      implicit none
c-----input  
      integer :: N_species 
      real*8, dimension(1:N_species) :: v_particle_min_vector,
     & v_particle_max_vector,
     & v0_vector                           !normalized to v_ch

      real*8 ::      
     &rN_0             !initial overaged number aerosol particle
                        !in the system #/m**3
c-----output
      real*8 :: Q0        ! is non-dimensional

c-----locals
      integer j

      real*8 :: product,Q_norm

c-----externals
      real*8  Q_normalization_N_species

      write(*,*)' analytical_Q0_for_exp_distribution'
      write(*,*)'N_species',N_species
      write(*,*)'v_particle_min_vector',v_particle_min_vector
      write(*,*)'v_particle_max_vector',v_particle_max_vector
      write(*,*)'v0_vector',v0_vector
      write(*,*)'rN_0',rN_0

      product=1.d0

       write(*,*)'product',product

      do j=1,N_species

         write(*,*)'j',j

         product = product * v0_vector(j)*
     &   (dexp(-(v_particle_min_vector(j)/v0_vector(j)))-
     &    dexp(-(v_particle_max_vector(j)/v0_vector(j))))

      enddo
    
      write(*,*)'product',product

      Q_norm=Q_normalization_N_species(N_species,                !normalised to
     &v_particle_min_vector,v_particle_max_vector,v0_vector) !v_ch**(N_species+1)
                              
      !product is non-dimensional
      !r_N_0 [# / v]

      Q0 = (rN_0/product)*Q_norm !non-dimensional

      write(*,*)'in analytical_Q0_for_exp_distribution Q_norm',Q_norm
      write(*,*)'rN_0,product,Q0',rN_0,product,Q0


      return
      end       



      subroutine integral_from_f_num_anal_arrays(M1,M2,
     &v_1_2d_mesh,v_2_2d_mesh,v_c_1_2d_mesh,v_c_2_2d_mesh,      
     &fn_2d_mesh,fq_2d_mesh,fn_2d_analyt,fq_2d_analyt)
c-------------------------------------------------------------
c      calculate integrals fom analytical and numerical distributions
c     fn_2d_mesh,fq_2d_mesh,fn_2d_analyt,fq_2d_analyt
c----------------------------------------------------------------
      implicit none

c-----input
      integer :: M1,M2 

      real*8, dimension(0:M1) :: v_1_2d_mesh !2d volume mesh for plotting
      real*8, dimension(0:M2) :: v_2_2d_mesh !2d volume mesh for plotting
      real*8, dimension(1:M1) ::  v_c_1_2d_mesh !
      real*8, dimension(1:M2) ::  v_c_2_2d_mesh !
     
      real*8, dimension (1:M1,1:M2) ::
     & fn_2d_mesh,    ! concentration distribution
     & fq_2d_mesh,    ! q=v*comcentration distribution
     & fn_2d_analyt,  ! analytical concentration distribution
     & fq_2d_analyt   ! analytical q=v_sum*concentration distribution

c-----locals
      real*8 :: 
     &fn_anal_integral,
     &fn_numer_integral,
     &fq_anal_integral,
     &fq_numer_integral,
     &step1,step2

      integer k1,k2


      step1=v_1_2d_mesh(1)-v_1_2d_mesh(0)
      step2=v_2_2d_mesh(1)-v_2_2d_mesh(0)

      fn_anal_integral=0.
      fn_numer_integral=0.
      fq_anal_integral=0.
      fq_numer_integral=0.

      do k1=1,M1
         do k2=1,M2
            fn_anal_integral = fn_anal_integral + fn_2d_analyt(k1,k2)
            fn_numer_integral = fn_numer_integral + fn_2d_mesh(k1,k2) 
            fq_anal_integral = fq_anal_integral + fq_2d_analyt(k1,k2)
            fq_numer_integral = fq_numer_integral + fq_2d_mesh(k1,k2) 
         enddo
      enddo

      fn_anal_integral = fn_anal_integral*step1*step2
      fn_numer_integral = fn_numer_integral*step1*step2 
      fq_anal_integral = fq_anal_integral*step1*step2
      fq_numer_integral = fq_numer_integral*step1*step2

      write(*,*)'in integral_from_f_num_anal' 
      write(*,*)'fn_numer_integral',fn_numer_integral 
      write(*,*)'fn_anal_integral',fn_anal_integral 
      write(*,*)'fq_numer_integral',fq_numer_integral
      write(*,*)'fq_anal_integral',fq_anal_integral
 
      return
      end

   
      subroutine calculate_integral_from_w_x(N_species,v0_vector,
     &v_particle_min_vector,
     &v_particle_max_vector)

      implicit none
c-----input
      integer :: N_steps
      parameter (N_steps=100)

c-----
      integer :: N_species
      real*8, dimension(1:N_species) ::
     & v0_vector,
     & v_particle_min_vector,
     & v_particle_max_vector

c-----locals
      integer k1,k2,i,j

      real*8, dimension(1:N_species) :: steps
      real*8, dimension(0: N_steps) :: v_1_2d_mesh
      real*8, dimension(0: N_steps) :: v_2_2d_mesh
      
      real*8, dimension(1: N_steps) :: v_c_1_2d_mesh
      real*8, dimension(1: N_steps) :: v_c_2_2d_mesh
      real*8, dimension(1:N_species) :: v_vector
       
      real*8 :: g_p,w_x_integral,dens_integral,q_integral
c-----externals
      real*8 w_x
       
      do j=1,N_species
         steps(j)=(v_particle_max_vector(j)-v_particle_min_vector(j))/
     &            N_steps 
      enddo  



      do i=0,N_steps
         v_1_2d_mesh(i)=v_particle_min_vector(1)+steps(1)*i
         v_2_2d_mesh(i)=v_particle_min_vector(2)+steps(2)*i
      enddo

      do i=1,N_steps
         v_c_1_2d_mesh(i)= v_1_2d_mesh(i)-0.5*steps(1)
         v_c_2_2d_mesh(i)= v_2_2d_mesh(i)-0.5*steps(2)
      enddo

      w_x_integral=0.d0
      q_integral=0.d0

      do k1=1,N_steps
         v_vector(1)=v_c_1_2d_mesh(k1)

         do k2=1,N_steps

            v_vector(2)=v_c_2_2d_mesh(k2)

            w_x_integral = w_x_integral +
     &         w_x(N_species,v0_vector,v_particle_min_vector,
     &         v_particle_max_vector,v_vector)
         enddo 
      enddo 

      w_x_integral =  w_x_integral*steps(1)*steps(2)

      write(*,*)'w_x_integral',w_x_integral

      g_p=1.
      do j=1,N_species
         g_p=g_p*v0_vector(j)*
     &   (exp(- v_particle_min_vector(j)/v0_vector(j))-
     &    exp(- v_particle_max_vector(j)/v0_vector(j)))
      enddo

      write(*,*)'g_p',g_p

      return
      end

c!!!!!!!!!!!!!!!!!!!!

      subroutine create_2d_distribution_function_from_particles(
     &N_particle,N_particle_ini,N_species,M1,M2,
     &v_particle_max_vector,v_particle_min_vector,y_ar,
     &v_c_1_2d_mesh,v_c_2_2d_mesh,
     &fn_2d_mesh,fq_2d_mesh) 

      implicit none
c-----input
      integer ::
     &N_particle,     !number of particles
     &N_particle_ini, !initial number of particles
     &N_species,      !number of species for this case =1  
     &M1,M2           !numbers of 2D mesh points

      real*8, dimension(1:N_species) :: 
     & v_particle_min_vector,v_particle_max_vector 

      real*8, dimension(1:N_species,1:N_particle) :: 
     &y_ar    ! volumes of points
    
c-----output 
c      real*8, dimension(0:M1) :: v_1_2d_mesh !2d volume mesh for plotting
c      real*8, dimension(0:M2) :: v_2_2d_mesh !2d volume mesh for plotting
c      real*8, dimension(1:M1) ::  v_c_1_2d_mesh !
c      real*8, dimension(1:M2) ::  v_c_2_2d_mesh !
       real*8, dimension(0:M1) ::  v_c_1_2d_mesh !
       real*8, dimension(0:M2) ::  v_c_2_2d_mesh !
c-----output 
     
c      real*8, dimension(1:M1,1:M2) ::  fn_2d_mesh,fq_2d_mesh

      real*8, dimension(0:M1,0:M2) ::  fn_2d_mesh,fq_2d_mesh

c-----locals
      integer :: j,k,i,k1,k2
      real*8, dimension(1:N_species) :: steps,v_vector
      real*8 :: delta_n,delta_q
      real*8 :: q_sum,q_sum_analyt
      integer, dimension(1:N_species) :: M_ar

      integer, dimension(1:N_species) :: index_v_mesh
      
c-----externals
c      real*8 w_x,w_x_prime

c      write(*,*)'in create_2d_distribution_function_from_particles'

c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
      


      if(N_species.ne.2)then  
         write(*,*)'in subroutine create_2d_distribution_f'
         write(*,*)'N_species.ne.2'
         write(*,*)'it should be N_species=2'
         stop 'subroutine create_2d_distribution_f'
      endif

      M_ar(1)=M1
      M_ar(2)=M2

c      write(*,*)'M1,M2',M1,M2
c      write(*,*) 'M_ar', M_ar

c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v_particle_min_vector',v_particle_min_vector

c-----create 2D v_mesh for MC distribution function


      do j=1,N_species

c         write(*,*)'j',j,'M_ar(j)', M_ar(j) 

         steps(j)=(v_particle_max_vector(j)-v_particle_min_vector(j))/
     &            (M_ar(j)+1)
   
      enddo

c      write(*,*)'steps',steps

      v_c_1_2d_mesh(0)=v_particle_min_vector(1)+0.5d0*steps(1)
      do k1=1,M1
         v_c_1_2d_mesh(k1)=v_c_1_2d_mesh(k1-1)+steps(1)
      enddo

      v_c_2_2d_mesh(0)=v_particle_min_vector(2)+0.5d0*steps(2)
      do k2=1,M2
         v_c_2_2d_mesh(k2)=v_c_2_2d_mesh(k2-1)+steps(1)
      enddo
     
  
c      write(*,*)'v_c_1_2d_mesh',v_c_1_2d_mesh
c      write(*,*)'v_c_2_2d_mesh',v_c_2_2d_mesh


c-----calculate MC 2D distribution function

      do k1=0,M1
         do k2=0,M2
            fn_2d_mesh(k1,k2)=0.d0
            fq_2d_mesh(k1,k2)=0.d0
         enddo
      enddo


      do k1=0,M1
         do k2=0,M2 
            delta_n=0.d0
            delta_q=0.d0
            do i=1,N_particle
              if((
     &            ((v_c_1_2d_mesh(k1)-0.5d0*steps(1)).le.y_ar(1,i)).and.
     &            ((v_c_1_2d_mesh(k1)+0.5d0*steps(1)).gt.y_ar(1,i))
     &           ).and.
     &           ( 
     &            ((v_c_2_2d_mesh(k2)-0.5d0*steps(2)).le.y_ar(2,i)).and.
     &            ((v_c_2_2d_mesh(k2)+0.5d0*steps(2).gt.y_ar(2,i)))
     &            ))then

                  delta_n=delta_n+1.d0/
     &            (v_c_1_2d_mesh(k1)+v_c_2_2d_mesh(k2))

                  delta_q=delta_q+1.d0
              endif
            enddo !N_particle

               
            fn_2d_mesh(k1,k2)=delta_n/
     &               (float(N_particle_ini)*steps(1)*steps(2))
            fq_2d_mesh(k1,k2)=delta_q/
     &               (float(N_particle_ini)*steps(1)*steps(2))          
         enddo     
      enddo     

      q_sum=0.d0

      do k1=0,M1
         do k2=0,M2           
c            q_sum_analyt = q_sum_analyt+step(1)*step(2)*f_2d_analyt(k1,k2)
            q_sum = q_sum+steps(1)*steps(2)*fq_2d_mesh(k1,k2)            
         enddo
      enddo

c      write(*,*)'q sum',q_sum
      
      return
      end

      subroutine  create_plot_distribution_particle_number_mass_on_vol(
     &Nx,Ny,N_particle,N_particle_ini,N_species,
     &v0_vector,v_particle_max_vector,v_particle_min_vector,y_ar,t,
     &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

 
      implicit none
c-----input
      integer :: Nx,Ny,N_particle,N_particle_ini,N_species,
     &n_vol_plot

      real*8, dimension(1:N_species) :: 
     &v0_vector,v_particle_max_vector,v_particle_min_vector

      real*8, dimension(1:N_species,1:N_particle) :: y_ar
      real*8 t

      real*8, dimension(0:n_vol_plot) ::
     &vol_plot_ar             !Loyalka output volume mesh(0:n_vol_plot

      real*8, dimension(1:n_vol_plot) ::
     &particle_numb_ar

      real*8, dimension(1:n_vol_plot,1:2) :: 
     &component_mass_ar      !(1:n_vol_plot,1:2) 

c-----locals
      real*8, allocatable, dimension(:) :: 
     &v_c_1_2d_mesh,v_c_2_2d_mesh

      real*8, allocatable, dimension(:,:) ::
     &fn_2d_mesh,fq_2d_mesh,
     &fn_2d_analyt,fq_2d_analyt
      integer :: istat

c      write(*,*)'create_plot_distribution_particle_number_mass_on_vol'


      allocate(v_c_1_2d_mesh(0:Nx),STAT=istat)
      allocate(v_c_2_2d_mesh(0:Nx),STAT=istat)
      allocate(fn_2d_mesh(0:Nx,0:Ny),STAT=istat)
      allocate(fq_2d_mesh(0:Nx,0:Ny),STAT=istat)
      allocate(fn_2d_analyt(0:Nx,0:Ny),STAT=istat)
      allocate(fq_2d_analyt(0:Nx,0:Ny),STAT=istat)

c      write(*,*)'bef create_2d_distribution_function_from_particles'

      call create_2d_distribution_function_from_particles(
     &N_particle,N_particle_ini,N_species,Nx,Ny,
     &v_particle_max_vector,v_particle_min_vector,y_ar,
     &v_c_1_2d_mesh,v_c_2_2d_mesh,
     &fn_2d_mesh,fq_2d_mesh) 

c      write(*,*)'bef plot_distribution_particle_number_mass_on_volume'

      call plot_distribution_particle_number_mass_on_volume
     &(v0_vector(1),v0_vector(2),Nx,Ny,
     & fn_2d_mesh,v_c_1_2d_mesh,v_c_2_2d_mesh,t,'MC',
     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

c      write(*,*)'aft plot_distribution_particle_number_mass_on_volume'
c      write(*,*)'N_species,Nx,Ny',N_species,Nx,Ny
c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
c      write(*,*)'v0_vector',v0_vector 
c      write(*,*)'v_c_1_2d_mesh',v_c_1_2d_mesh
c      write(*,*)'v_c_2_2d_mesh',v_c_2_2d_mesh

      call create_2d_distribution_array_from_w_x(
     &N_species,Nx,Ny,
     &v0_vector,v_particle_max_vector,v_particle_min_vector,
     &v_c_1_2d_mesh,v_c_2_2d_mesh,
     &fn_2d_analyt,fq_2d_analyt) 


      call plot_distribution_particle_number_mass_on_volume
     &(v0_vector(1),v0_vector(2),Nx,Ny,
     & fn_2d_analyt,v_c_1_2d_mesh,v_c_2_2d_mesh,t,'w_x',
     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

      deallocate(v_c_1_2d_mesh)
      deallocate(v_c_2_2d_mesh)
      deallocate(fn_2d_mesh)
      deallocate(fq_2d_mesh)
      deallocate(fn_2d_analyt)
      deallocate(fq_2d_analyt)


      return
      end

      subroutine create_2d_distribution_array_from_w_x(
     &N_species,M1,M2,
     &v0_vector,v_particle_max_vector,v_particle_min_vector,
     &v_c_1_2d_mesh,v_c_2_2d_mesh,
     &fn_2d_mesh,fq_2d_mesh) 

      implicit none
c-----input
      integer ::     
     &N_species,      !number of species for this case =1  
     &M1,M2           !numbers of 2D mesh points

      real*8, dimension(1:N_species) :: 
     &v_particle_min_vector,v_particle_max_vector,
     &v0_vector
 
      real*8, dimension(0:M1) ::  v_c_1_2d_mesh !
      real*8, dimension(0:M2) ::  v_c_2_2d_mesh      

c-----output
      real*8, dimension(0:M1,0:M2) ::  fn_2d_mesh,fq_2d_mesh
c-----locals
      integer k1,k2     
      real*8,  dimension(1:2) ::  v_vector
c-----locals
c-----externals
      real*8 w_x

c      write(*,*)'in create_2d_distribution_array_from_w_x'
 
c      write(*,*)'N_species,M1,M2',N_species,M1,M2
c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
c      write(*,*)'v0_vector',v0_vector
c      write(*,*)'v_c_1_2d_mesh',v_c_1_2d_mesh
c      write(*,*)'v_c_2_2d_mesh',v_c_2_2d_mesh


      if(N_species.ne.2)then  
         write(*,*)'in subr create_2d_distribution_array_from _w_x'
         write(*,*)'N_species.ne.2'
         write(*,*)'it should be N_species=2'
         stop 'subroutine create_2d_distribution_array_from _w_x'
      endif

      do k1=0,M1
         v_vector(1)=v_c_1_2d_mesh(k1)
         do k2=0,M2
            v_vector(2)=v_c_2_2d_mesh(k2)
       
            fq_2d_mesh(k1,k2)=w_x(N_species,v0_vector, 
     &      v_particle_min_vector,v_particle_max_vector,
     &      v_vector) 

          fn_2d_mesh(k1,k2)=fq_2d_mesh(k1,k2)/(v_vector(1)+v_vector(2))

         enddo
      enddo

   
      return
      end


c!!!!!!!!!!!!!!!!!!!!


      subroutine create_plot_distribution_particle_number_mass_on_vol_t(
     &Nx,Ny,N_particle,N_particle_ini,N_species,
     &v0_vector,v_particle_max_vector,v_particle_min_vector,y_ar,t,
     &n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar,
     &v_x,v_y,v_x_0,v_y_0,rN_0,rn_2d_wx)


 
      implicit none
c-----input
      integer :: Nx,Ny,N_particle,N_particle_ini,N_species,
     &n_vol_plot

      real*8, dimension(1:N_species) :: 
     &v0_vector,v_particle_max_vector,v_particle_min_vector

      real*8, dimension(1:N_species,1:N_particle) :: y_ar
      real*8 t,rN_0

      real*8, dimension(0:n_vol_plot) ::
     &vol_plot_ar             !Loyalka output volume mesh(0:n_vol_plot

      real*8, dimension(1:n_vol_plot) ::
     &particle_numb_ar

      real*8, dimension(1:n_vol_plot,1:2) :: 
     &component_mass_ar      !(1:n_vol_plot,1:2)
 
c---- for-test-----------------------------------

      real*8, dimension(0:Nx) :: v_x,v_y

      real*8, dimension(0:Nx,0:Ny) :: rn_2d_wx

      real*8 :: v_x_0,v_y_0
c-----locals
      real*8, allocatable, dimension(:) :: 
     &v_c_1_2d_mesh,v_c_2_2d_mesh

      real*8, allocatable, dimension(:,:) ::
     &fn_2d_mesh,fq_2d_mesh

      integer :: istat

      write(*,*)'create_plot_distribution_particle_number_mass_on_vol_t'

c      write(*,*)'Nx,Ny,N_particle,N_particle_ini,N_species',
c     &           Nx,Ny,N_particle,N_particle_ini,N_species

c      write(*,*)'v0_vector',v0_vector
c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
c      write(*,*)'v_x',v_x
c      write(*,*)'v_y',v_y

      allocate(v_c_1_2d_mesh(0:Nx),STAT=istat)
      allocate(v_c_2_2d_mesh(0:Nx),STAT=istat)
      allocate(fn_2d_mesh(0:Nx,0:Ny),STAT=istat)
      allocate(fq_2d_mesh(0:Nx,0:Ny),STAT=istat)

      write(*,*)'bef create_2d_distribution_function_from_particles'

      call create_2d_distribution_function_from_particles(
     &N_particle,N_particle_ini,N_species,Nx,Ny,
     &v_particle_max_vector,v_particle_min_vector,y_ar,
     &v_c_1_2d_mesh,v_c_2_2d_mesh,
     &fn_2d_mesh,fq_2d_mesh) 

      write(*,*)'bef plot_distribution_particle_number_mass_on_volume'

      call plot_distribution_particle_number_mass_on_volume
     &(v0_vector(1),v0_vector(2),Nx,Ny,
     & fn_2d_mesh,v_c_1_2d_mesh,v_c_2_2d_mesh,t,'initial  MC',
     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

c      write(*,*)'aft plot_distribution_particle_number_mass_on_volume'
c      write(*,*)'N_species,Nx,Ny',N_species,Nx,Ny
c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
c      write(*,*)'v0_vector',v0_vector 
c      write(*,*)'v_c_1_2d_mesh',v_c_1_2d_mesh
c      write(*,*)'v_c_2_2d_mesh',v_c_2_2d_mesh

      call create_2d_distribution_array_from_w_x(
     &N_species,Nx,Ny,
     &v0_vector,v_particle_max_vector,v_particle_min_vector,
     &v_c_1_2d_mesh,v_c_2_2d_mesh,
     &fn_2d_mesh,fq_2d_mesh) 

c      write(*,*)'after create_2d_distribution_array_from_w_x'
c      write(*,*)'v_x',v_x
c      write(*,*)'v_c_1_2d_mesh',v_c_1_2d_mesh
      
c      write(*,*)'v_y',v_y
c      write(*,*)'v_c_2_2d_mesh',v_c_2_2d_mesh
c      write(*,*)'rn_2d_wx',rn_2d_wx
c      write(*,*)'fn_2d_mesh',fn_2d_mesh


      call plot_distribution_particle_number_mass_on_volume
     &(v0_vector(1),v0_vector(2),Nx,Ny,
     & fn_2d_mesh,v_c_1_2d_mesh,v_c_2_2d_mesh,t,'initial w_x',
     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)

      deallocate(v_c_1_2d_mesh)
      deallocate(v_c_2_2d_mesh)
      deallocate(fn_2d_mesh)
      deallocate(fq_2d_mesh)


      return
      end


c!!!

      real*8 function w_x_t(N_species,v0_vector, 
     &v_particle_min_vector,v_particle_max_vector,
     &v_vector)
c-------------------------------------
c     distribution of probability w(x)
c--------------------------------------
      implicit none

c-----input 
      integer  ::
     & N_species   ! number of species
      real*8, dimension(1:N_species) :: 
     & v_vector,        !volumes
     & v0_vector,        !paramers of distribution
     & v_particle_min_vector,v_particle_max_vector !normalized
c-----locals
      real*8 :: arg,p,v_sum
      integer j
c-----externals
      real*8 Q_normalization_N_species

c      write(*,*)'in w_x N_species',N_species
c      write(*,*)'v_particle_min_vector',v_particle_min_vector
c      write(*,*)'v_particle_max_vector',v_particle_max_vector
c      write(*,*)'v0_vector',v0_vector
c      write(*,*)'v_vector',v_vector
    

c      do j=1,N_species
c         write(*,*)'j,v0_vector(j)',j,v0_vector(j)
c         write(*,*)'v_particle_min_vector(j),v_particle_max_vector(j)'
c     &             ,v_particle_min_vector(j),v_particle_max_vector(j)
c         write(*,*)'v_vector(j)',v_vector(j)
c      enddo

      v_sum=0.d0
      arg=0.0d0
     
 
      do j=1,N_species
         arg = arg + v_vector(j)/v0_vector(j)
         v_sum = v_sum +v_vector(j)
      enddo

      p=Q_normalization_N_species(N_species,
     &v_particle_min_vector,v_particle_max_vector,v0_vector)
  
c      write(*,*)'p=Q0_N_species',p

c      w_x=v_sum*exp(-arg)/p
       w_x_t=v_sum*exp(-arg)
c      write(*,*)'v_sum,p=Q0_N_species,arg,w_x_t',
c     &           v_sum,p,arg,w_x_t

      return
      end


      subroutine calculate_total_density_from_particles(N_species,
     &N_particle,y_ar,
     &v_particle_min_vector,
     &v_particle_max_vector,
     &v0_vector,
     &density,volume)

c----------------------------------------------------------------
c     calculate density from particles
c----------------------------------------------------------------
      implicit none

c-----input
      integer :: 
     &N_species,   !number of species 
     &N_particle   !number of particles

      real*8, dimension(1:N_species,1:N_particle) :: y_ar
      real*8, dimension(1:N_species ) :: v_particle_max_vector,
     &v_particle_min_vector,        !normalized to v_ch
                                   !max and min aerosol volumes       
     & v0_vector        !paramers of initial distribution

c-----used functions
     &



c-----output
      real*8 :: density,volume

c-----locals
      integer i,j 

      real*8 v_sum,p

c-----externals
      real*8 Q_normalization_N_species
      
   

      density=0.d0
      volume=0.d0
      do i=1,N_particle
          v_sum=0.d0
          do j=1,N_species
             v_sum=v_sum + y_ar(j,i)
          enddo 
          density = density+1.d0/v_sum
          volume = volume+1.d0
      enddo

      p=Q_normalization_N_species(N_species,
     &v_particle_min_vector,v_particle_max_vector,v0_vector)

      density=density*p

      volume=volume*p

      return
      end
