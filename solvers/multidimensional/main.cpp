using namespace std;

#include <iostream>
#include <cstdio>
#include <string.h>
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <mkl.h>
#include <omp.h>
#include <ctime>
#include "tt_Smoluh_2.h"
#include "../../libtt/tensor_train.h"

int main(int argc, char ** argv)
{
	double r_time;
	struct timeval start, end;
	
	int d, *m, *m_for_second , *m_for_start, *m_for_error;
	double * curr_knots, *curr_weights;
	int *point;
	FILE* values;
	char name_of_file[15]="values.dat";
	char name_of_picture[30];
	//values=fopen(name_of_file,"w+");
	int number_of_knots=101;
	int number_of_steps=100;
	double max_knot = 10.0;
	double step_time=1e-2;
	double tolerance=1e-2;
	int sweeps=0;
	
	//cout<<name_of_picture<<endl;
	omp_set_num_threads(1);//non parallel
//Set parameters, allocate memory, prepare arrays	
//===================================================================================================================
//===================================================================================================================
	if (argc<8){
		printf("Error. Not enough arguments.\n");
		printf("Must look like:\nd n_knots n_steps max_v st_time tol sweeps\n");
		exit(1);
	}
	sscanf(argv[1], "%d", &d);
	sscanf(argv[2], "%d", &number_of_knots);
	sscanf(argv[3], "%d", &number_of_steps);
	sscanf(argv[4], "%lf", &max_knot);
	sscanf(argv[5], "%lf", &step_time);
	sscanf(argv[6], "%lf", &tolerance);
	sscanf(argv[7], "%d", &sweeps);

	FILE *f;
	StartCondition::weights = (double*)malloc(number_of_knots*sizeof(double));
	StartCondition::knots = (double*)malloc(number_of_knots*sizeof(double));

	FirstIntegral::weights = (double*)malloc(number_of_knots*sizeof(double));
	FirstIntegral::knots = (double*)malloc(number_of_knots*sizeof(double));
	FirstIntegral::average_ranks = (int*)malloc((d+1)*sizeof(int));
	FirstIntegral::number_of_computations=0;

	ForVolumeCheck::knots= (double*)malloc(number_of_knots*sizeof(double));

	SecondIntegral::weights= (double*)malloc(number_of_knots*sizeof(double));
	SecondIntegral::knots = (double*)malloc(number_of_knots*sizeof(double));
	SecondIntegral::average_ranks = (int*)malloc((d+1)*sizeof(int));
	SecondIntegral::number_of_computations=0;

	Steps::weights = (double*)malloc(number_of_knots*sizeof(double));
	Steps::knots = (double*)malloc(number_of_knots*sizeof(double));
	Steps::step_time=step_time;
	Steps::mesh_step=max_knot/(double(number_of_knots-1));

	TDiscreteAnalitycal::knots = (double*)malloc(number_of_knots*sizeof(double));
	
	curr_knots=  (double*)malloc(number_of_knots*sizeof(double));
	curr_weights= (double*)malloc(number_of_knots*sizeof(double));
	
//	f = fopen("saved_weights_11", "rb");
//	fread(curr_knots, sizeof(double), number_of_knots, f);
//	fread(curr_weights, sizeof(double), number_of_knots, f);

	for (int i=0; i<number_of_knots;i++)
	{
		curr_knots[i]= max_knot*(number_of_knots-i-1 )/(number_of_knots - 1);
		curr_weights[i]=max_knot/double(number_of_knots - 1);
	}

	for(int i=0;i<number_of_knots;i++)
	{
		cout << " " <<curr_knots[number_of_knots-i-1];
		
		StartCondition::knots[i]=curr_knots[number_of_knots-i-1];
		StartCondition::weights[i]=curr_weights[number_of_knots-i-1];
		
		FirstIntegral::knots[i]=curr_knots[number_of_knots-i-1];
		FirstIntegral::weights[i]=curr_weights[number_of_knots-i-1];
		
		ForVolumeCheck::knots[i]=curr_knots[number_of_knots-i-1];
		SecondIntegral::weights[i] = curr_weights[number_of_knots-i-1];
		SecondIntegral::knots[i] = curr_knots[number_of_knots-i-1];

	
		Steps::weights[i]= curr_weights[number_of_knots-i-1];
		Steps::knots[i]= curr_knots[number_of_knots-i-1];
		
		TDiscreteAnalitycal::knots[i]= curr_knots[number_of_knots-i-1];

	}


	point = (int*) malloc(d * sizeof(int));
	int * m_for_vol_check;
	m = (int *) malloc(d * sizeof(int));
	m_for_second=(int*) malloc(d * sizeof(int));
	m_for_start=(int*) malloc(d * sizeof(int));
	m_for_error=(int*) malloc(d * sizeof(int));
	m_for_vol_check=(int*) malloc (d*sizeof(int));

	
	for (int i = 0; i < d; i++)
	{ 
		m[i] = number_of_knots;
		m_for_second[i] = number_of_knots;
		m_for_start[i] = number_of_knots;
		m_for_error[i] = number_of_knots;
		m_for_vol_check[i]=number_of_knots;
		point[i]= number_of_knots-1;
	}

	gettimeofday(&start, NULL);
//===================================================================================================================
//===================================================================================================================
//Start conditions
//===================================================================================================================
//===================================================================================================================
	StartCondition start_cond(d, m_for_start);
	Steps steps(d, m);

	TDiscreteAnalitycal discrete_analitycal(d, m_for_error);	

	TTensorTrain tt_step[number_of_steps];
	TTensorTrain tt_half[number_of_steps];
	TTensorTrain tt_error[number_of_steps];
	TTensorTrain tt_half_error[number_of_steps];

	TTensorTrainParameters parameters;
	
	parameters.tolerance=tolerance;
	parameters.maximal_iterations_number=0;
	TTrapecialWeights weights(d, m_for_vol_check, Steps::mesh_step);

	ForVolumeCheck for_vol_check(d,m_for_vol_check);

	TTensorTrain vol_check_tt;
	vol_check_tt.Approximate(&for_vol_check,parameters);
	vol_check_tt.SVDCompress(tolerance);

	//TTensorTrain discrete_analitycal_tt;

	tt_step[0].Approximate(&start_cond, parameters);

	cout << "Starting condition ranks before compression: ";
	for (int i=0; i<=d; i++)
	{
		cout << tt_step[0].get_rank(i) << ' ';
	}
	cout << endl;
	parameters.maximal_iterations_number=0;
        tt_step[0].SVDCompress(tolerance);

	double coag_volume = tt_step[0].dot(vol_check_tt)*(pow(Steps::mesh_step,d));//((number_of_knots-1)*(number_of_knots-1));
	double full_integral = tt_step[0].dot(weights);
	cout << "Starting coagulation volume = " << coag_volume << endl; 			
	//cout << "full integral of n_0 = "<<full_integral << endl;
	cout << "Starting condition ranks after compression: ";
	for (int i=0; i<=d; i++)
	{
		cout << tt_step[0].get_rank(i) << ' ';
	}
	cout << endl;
	double t=0.0;
	int _ppp[2];
//===================================================================================================================
//===================================================================================================================
// Start predictor-corrector scheme
//===================================================================================================================
//===================================================================================================================
        double err2, errC, nrm2, nrmC;

	for (int s=0; s<number_of_steps-1; s++)
	{
		//===================================================================================================================
		//===================================================================================================================
		//Predictor		
		//===================================================================================================================
		//===================================================================================================================

	        err2 = 0;
	        errC = 0;
	        nrm2 = 0;
	        nrmC = 0;
		t+=step_time/2;
		Steps::number_of_computations=0;
		cout << "predictor-half-step"<<s+1<<endl;
		steps.set_trains(&tt_step[s],&tt_step[s], 1);
		steps.set_parameters(tolerance, sweeps);

		FirstIntegral::number_of_computations=0;
		SecondIntegral::number_of_computations=0;
		for (int i=0; i<d; i++)
		{
			FirstIntegral::average_ranks[i]=0;
			SecondIntegral::average_ranks[i]=0;
		}
		tt_half[s+1].Approximate(&steps, parameters);

		cout << "Predictor step ranks before compression: ";
		for (int i=0; i<=d; i++)
		{
			cout << tt_half[s+1].get_rank(i) << ' ';
		}
		cout << endl;
		tt_half[s+1].SVDCompress(tolerance);
		cout << "Predictor step ranks after compression: ";

		for (int i=0; i<=d; i++)
		{
			cout << tt_half[s+1].get_rank(i) << ' ';
		}
		cout << endl;
		cout << "predictor took " << Steps::number_of_computations << " computations of tensor elements" << endl;

		cout << "average tt_ranks of integrands in First Integral = " ;
		for (int i=0; i<d;i++)
		{
			cout << (double(FirstIntegral::average_ranks[i]))/FirstIntegral::number_of_computations << " ";
		}
		cout <<endl;

		cout << "average tt_ranks of integrands in Second Integral = " ;
		for (int i=0; i<d;i++)
		{
			cout << (double(SecondIntegral::average_ranks[i]))/SecondIntegral::number_of_computations << " ";
		}
		cout <<endl;

		discrete_analitycal.t=t;
		if (d==2)
		{

			double half_max=0;
			for (int i=0; i<number_of_knots;i++)
				for (int j=0; j<number_of_knots; j++)
				{
					_ppp[0]=i;
					_ppp[1]=j;
                                        double analitycal = discrete_analitycal[_ppp], error = tt_half[s+1][_ppp] - analitycal;
                                        err2 += error * error;
                                        nrm2 += analitycal * analitycal;
					if (fabs(analitycal) > nrmC) nrmC = fabs(analitycal);
					if (fabs(error) > errC) errC = fabs(error);
				}
			cout << "relevant_error2_half_step=" << sqrt(err2 / nrm2) << endl;
			cout << "relevant_errorC_half_step=" << errC / nrmC << endl;
			cout << "absolute_error2_half_step=" << sqrt(err2 ) << endl;
			cout << "ablolute_errorC_half_step=" << errC  << endl;
		}
		//===================================================================================================================
		//===================================================================================================================
		//Corrector		
		//===================================================================================================================
		//===================================================================================================================
		t+=step_time/2;
		Steps::number_of_computations=0;
		cout << "corrector-half-step"<< endl;
		steps.set_trains(&tt_step[s], &tt_half[s+1], 0);
		steps.set_parameters(tolerance, sweeps);

		FirstIntegral::number_of_computations=0;
		SecondIntegral::number_of_computations=0;
		for (int i=0; i<d; i++)
		{
			FirstIntegral::average_ranks[i]=0;
			SecondIntegral::average_ranks[i]=0;
		}

		tt_step[s+1].Approximate(&steps, parameters);

		cout << "Corrector step ranks before compression: ";
		for (int i=0; i<=d; i++)
		{
			cout << tt_step[s+1].get_rank(i) << ' ';
		}
		cout << endl;
		tt_step[s+1].SVDCompress(tolerance);
		cout << "Corrector step ranks after compression: ";
		for (int i=0; i<=d; i++)
		{
			cout << tt_step[s+1].get_rank(i) << ' ';
		}
		cout << endl;
		cout << "corrector took " << Steps::number_of_computations << " computations of tensor elements" << endl;

		cout << "average tt_ranks of integrands in First Integral = " ;
		for (int i=0; i<d;i++)
		{
			cout << (double(FirstIntegral::average_ranks[i]))/FirstIntegral::number_of_computations << " ";
		}
		cout <<endl;

		cout << "average tt_ranks of integrands in Second Integral = " ;
		for (int i=0; i<d;i++)
		{
			cout << (double(SecondIntegral::average_ranks[i]))/SecondIntegral::number_of_computations << " ";
		}
		cout <<endl;

		discrete_analitycal.t=t;
		for (int i=0; i<d; i++)
		{
			cout << "step_" << s+1 <<"_rank"<<i<<"= "<<tt_step[s].get_rank(i)<<endl;
		}

		if(d==2)
		{

			/*discrete_analitycal_tt.Approximate(&discrete_analitycal, parameters);
			discrete_analitycal_tt.SVDCompress(tolerance);
			for (int i=0; i<d; i++)
			{
				cout << "analitycal_step_" << s+1 <<"_rank"<<i<<"= "<<discrete_analitycal_tt.get_rank(i)<<endl;
			}
			cout << endl;*/
			
			double max_rel_error=0;
			double max=0;
			values=fopen(name_of_file, "w+");
			for (int i=0; i<number_of_knots;i++)
			{
				for (int j=0; j<number_of_knots; j++)
				{
					_ppp[0]=i;
					_ppp[1]=j;
					double analitycal = discrete_analitycal[_ppp], error = tt_step[s+1][_ppp] - analitycal;
					fprintf(values, "%lf ", tt_step[s+1][_ppp]);
					
					err2 += error * error;
					nrm2 += analitycal * analitycal;
					if (fabs(analitycal) > nrmC) nrmC = fabs(analitycal);
					if (fabs(error) > errC) errC = fabs(error);
				}
				fprintf(values, "\n");
			}

			cout << "relevant_error2_half_step=" << sqrt(err2 / nrm2) << endl;
			cout << "relevant_errorC_half_step=" << errC / nrmC << endl;
			cout << "absolute_error2_half_step=" << sqrt(err2 ) << endl;
			cout << "ablolute_errorC_half_step=" << errC  << endl;
			
			fclose(values);
			system ("gnuplot plotting_script");
			sprintf(name_of_picture,"mv 1.jpg IMG/file%03d.jpg",s+1);
			system(name_of_picture);
			cout<<"saving results into 'values.dat' ..."<<endl;
			


			
			double coag_volume = tt_step[s+1].dot(vol_check_tt)*(pow(Steps::mesh_step,d));
			double full_integral = tt_step[s+1].dot(weights);
			//double full_analitycal_integral = discrete_analitycal_tt.dot(weights);
			//double analit_volume = discrete_analitycal_tt.dot(vol_check_tt)*(pow(Steps::mesh_step,d));
			cout << "coagulation volume = " << coag_volume << endl;
			//cout << "analitycal volume = " << analit_volume << endl;

			//cout << "full analytical integral of n_"<< s+1<<"="<<full_analitycal_integral << endl;
			cout << "full integral of n_"<< s+1<<"="<<full_integral << endl;
		}
	}

	gettimeofday(&end, NULL);
	r_time = end.tv_sec  - start.tv_sec + ((double)(end.tv_usec  - start.tv_usec))/1000000;
	cout<< "Time = " << r_time << "\n";
	cout<< "Average time for step  = " << r_time/number_of_steps << "\n";
}
