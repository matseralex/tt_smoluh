#include <mpi.h>
#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include "../../libtt/tensor_train.h"


void advection_init(double ** u, double dm, int Mrange, int Xrange, int rank)// initial and boundary conditions for Smoluchowski equation i - space, m - particle size
{
	for (int i = 0; i < Xrange; i++)
        {
        	for(int m = 0; m < Mrange; m++)
                	u[i][m] = 0.0;
	}
	if(rank==0) 
		for(int m = 0; m < Mrange; m++)
                	u[1][m] = exp(-pow((m) * dm, 2));
}

double velocity(double r, double m)
{
	//return 1.0/(1.0+m);
	return 1.0;
}

double sigma(double r, int PML, double dr, int rank, int max_distance_PML)
{
	if (r > (dr * (max_distance_PML - PML))) {
        	return pow((r - (max_distance_PML - PML) * dr) / (PML * dr), 2) * 3.0 * log(10.0) * 10.0 / (PML * dr);
    	} else{
        	return 0;
    	}
}

void limiters(double dm, double dr, double dt, int Xrange, int Mrange, int PML, double ** u, int p, int rank, MPI_Comm comm, MPI_Status status, MPI_Request request, int max_distance_PML)
{
	double a,b;
	double ** res = new double * [Xrange];
	for (int i = 0; i < Xrange; i++)
		res[i] = new double [Mrange];
    	double * vel;
    	vel = new double [Mrange];
    	for (int m = 0;m < Mrange; m++)
        	vel[m] = velocity(dr*0.0, dm*m);
	if (rank==0){
    		for (int m = 0; m < Mrange; m++){
        		res[1][m] = u[1][m];
		}
	} else if (rank==p-1){
    		for (int m = 0; m < Mrange; m++)
    		{
        		res[Xrange-2][m] = u[Xrange - 2][m];
    		}
	}
	int left;
	int right;
	if (rank != 0) left = 1;
	else left = 0;
	if (rank != p-1) right = 1;
	else right = 0;
	//U EXCHANGE HERE
	if (left) MPI_Isend(u[1], Mrange, MPI_DOUBLE, rank - 1 /*send to*/, rank/*our tag*/, comm, &request);
	if (right) MPI_Isend(u[Xrange-2], Mrange, MPI_DOUBLE, rank + 1 /*send to*/, rank/*our tag*/, comm, &request);

	//MPI_Barrier(comm);

	if (left) {MPI_Irecv(u[0], Mrange, MPI_DOUBLE, rank - 1, rank - 1, comm, &request);
	MPI_Wait(&request, &status);}
	if (right) {MPI_Irecv(u[Xrange-1], Mrange, MPI_DOUBLE, rank + 1, rank + 1, comm, &request);

	MPI_Wait(&request, &status);}
	//U EXCHANGE HERE END

    	for (int i = (rank==0 ? 2 : 1); i < ((rank==p-1) ? Xrange-2 : Xrange - 1); i++){
        	for (int m = 0; m < Mrange; m++){
            		b = (u[i + 1][m] - u[i - 1][m]);
            		if (( (u[i + 1][m]-u[i][m])*(u[i][m]-u[i-1][m]))>0)
                		a = min(min(abs(b) / (2.0 * dr),
                            		2.0 * abs(u[i + 1][m] - u[i][m]) / dr),
                        		2.0 * abs(u[i][m] - u[i-1][m]) / dr) * (b >= 0 ? 1.0: -1.0);
            		else a = 0;
            		res[i][m] = u[i][m]+dr/2.0*(1.0-vel[m]*dt/dr)*a;
        	}
    	}
	//RES EXCHANGE HERE
	if (right) MPI_Isend(res[Xrange-2], Mrange, MPI_DOUBLE, rank + 1 /*send to*/, rank/*our tag*/, comm, &request);
	//MPI_Barrier(comm);
	if (left)  {MPI_Irecv(res[0], Mrange, MPI_DOUBLE, rank - 1, rank-1, comm, &request); MPI_Wait(&request, &status);}
	//RES EXCHANGE HERE

    	for (int i = (rank==0 ? 2 : 1);i < Xrange - 1; i++){
        	for(int m = 0;m < Mrange; m++)
            		u[i][m] = (vel[m] * res[i][m] - vel[m] * res[i - 1][m]) / dr +
                       		vel[m] * sigma(dr*(i+rank*(Xrange-2)), PML, dr, rank, max_distance_PML) * u[i][m];
    	}
    	delete [] vel;
    	for (int i = 0; i < Xrange; i++)
        	delete [] res[i];
    	delete [] res;
}


class TKernel2: public TMatrix
{
/// TKernel2 approximates the matrix of solutions at time steps =0s, 1s, ... 9s

        public:
                double **Sol;                             /// the matrix of solutions itself
                int S;
                TKernel2 (const int &, const int &);      /// a better function to initialize memory allocation, but I failed to use it
                ~TKernel2()
                {
                    for (int i = 0; i < S; i++) delete [] Sol[i];
                    delete [] Sol;
                }
                double value (const int & , const int &); /// get elements of matrix

};


TKernel2::TKernel2(const int &S, const int &N): TMatrix (S, N)
{
	this->S = S;
	Sol = new double * [S];
        for (int i = 0; i < S; i++)
        	Sol[i] = new double [N];
}

double TKernel2::value(const int &i, const int &j)
{
        return Sol[i][j];
}

double K(const int & u, const int &v, const double h)
{
    	//return 1.0;
	double u1=(u+1.0) * h;
	double v1=(v+1.0) * h;
    	return pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), 2) * sqrt((1.0l / u1 + 1.0l / v1));
}

class TKernel: public TMatrix
{
        public:
                double h;
                TKernel (const int &, const int &);
                double value (const int & , const int &);
};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{

}

double TKernel::value(const int &i, const int &j)
{
        return h * K(i, j, h);
}

double L1(const int &N, const int &i, const double *n, const double &h)
{
	double l1 = 0;
    	for (int i1 = 1; i1 <= i; i1++){
        	l1 += n[i1] * n[i - i1] * K((i - i1) , i1 ,h) + n[i1 - 1] * n[i - i1 + 1] * K((i - i1 + 1) , (i1 - 1) ,h);
    	}
    	l1 *= h/2;
    	return l1;
}

double L2(const int &N, const int &i, const double *n, const double &h)
{
    	double l2 = 0;
    	for (int i1 = 1; i1 < N; i1++){
        	l2 += n[i1] * K(i , i1,h) + n[i1 - 1] * K(i , (i1 - 1), h );
    	}
    	l2 *= h/2;
    	return l2;
}

int main(int argc, char ** argv)
{
   	// ***************************************************************************
	int p;
    	int rank;
	//***************************************************************************    
    	bool if_paint = true;
    	int num_of_threads = 1;
    	omp_set_num_threads(num_of_threads);
    	double dm  = 0.01;                                     ///particle's size step (dm)
    	double dr = 0.01;                                      ///space step
    	double dt = 0.005;                                     ///time step
    	int max_time_iterations = (int) 4000;                  ///(max_time_iterations)
	int max_distance_pml = 256;                             /// max distance from origin (includes PML)
	
    	

    	int periods = 10;                                      /// every periodsth calculation goes to gnuplot
    	double tolerance = 1e-6;

	if (argc != 10){
		printf("Need arguments:\n if_paint\n num_of_threads\n dm\n dr\n dt\n max_time_iterations\n max_distance_pml\n periods\n tolerance\n");
		return 2;
	}	
	sscanf(argv[1], "%lf", &if_paint);
	sscanf(argv[2], "%d", &num_of_threads);
	sscanf(argv[3], "%lf", &dm);
	sscanf(argv[4], "%lf", &dr);
	sscanf(argv[5], "%lf", &dt);
	sscanf(argv[6], "%d", &max_time_iterations);
	sscanf(argv[7], "%d", &max_distance_pml);
	sscanf(argv[8], "%d", &periods);
	sscanf(argv[9], "%lf", &tolerance);

	int PML = (int) (max_distance_pml*0.1);					       ///width of PML layer
    	int max_particle_size = (int) max_distance_pml - PML;                          ///(max_particle_size)	

    	TCross_Parallel_v1_Parameters parameters;
    	parameters.tolerance = tolerance;
    	parameters.maximal_iterations_number = 0;
    
    	TKernel kernel(max_particle_size, max_particle_size);
    	kernel.h = dm;
    	TCross_Parallel_v1 crossed_kernel;
    	crossed_kernel.Approximate(&kernel, parameters);

//********************************************************************
    	MPI_Comm comm = MPI_COMM_WORLD;
    	MPI_Init(&argc, &argv);
    	MPI_Comm_size(comm, &p);
    	MPI_Status status;
    	MPI_Request request;
    	int N_per_node = max_distance_pml / (int) p + 2;
    	MPI_Comm_rank(comm, &rank);
	if (max_distance_pml % p != 0){
        	printf("the space coordinate mesh must be dividable by the amount of processors without residuals");
		MPI_Finalize();
		return 3;
    	}
//********************************************************************
    	double *n_k;
    	double *L1_res_vec, *L2_res_vec, *L2_correction;
    	L2_correction = (double *) malloc (max_particle_size * sizeof(double));
    	FILE *output2d;                                      /// output for heat map
	if (rank == 0) {//********************************************************
    		if (if_paint){
        		output2d = fopen("data2.txt", "w");
    		}
	}//**********************************************************************
    	double **smoluch_operator;                           ///array for storing S(f^n,f^n) from ( f^(n+1) = f^n + dt*A(f^n)+dt*S(f^n,f^n) )
    	double **initial_layer;                              ///array for storing f^n(x,v)
    	double **u;
//****************************BUFFERS********************************************
   	double **collectmessage;
   	double **ucollect;
//****************************BUFFERS*******************************************

    ///allocate memory to arrays start
	if (rank == 0) {//*******************************************
    		if (if_paint){
    			ucollect = new double * [max_distance_pml];
    			for (int i = 0; i < max_distance_pml; i++)
    				ucollect[i] = new double [max_particle_size];
    			collectmessage = new double * [N_per_node-2];
    			for (int i = 0; i < N_per_node-2; i++)
    				collectmessage[i] = new double [max_particle_size];
		}
	}//*********************************************************
    	u = new double * [N_per_node];
    	for (int i = 0; i < N_per_node; i++)
    		u[i] = new double [max_particle_size];
//****************************************************************
    	smoluch_operator = new double * [N_per_node];
    	for (int i = 0; i < N_per_node; i++)
        	smoluch_operator[i] = new double [max_particle_size];

    	initial_layer = new double * [N_per_node];
    	for (int i = 0; i < N_per_node; i++)
        	initial_layer[i] = new double [max_particle_size];

    	advection_init(initial_layer, dm, max_particle_size, N_per_node, rank);
    	advection_init(u, dm, max_particle_size, N_per_node, rank);

    	n_k = (double *) malloc (max_particle_size * sizeof(double));
	///allocate memory to arrays end
	MPI_Barrier(comm);	
	double start;
	if(rank == 0) start = MPI_Wtime();
    	for (int t = 0; t < max_time_iterations; t += 1){
		//if (rank == 0) /printf("t = %d\n", t);//cout << "t equals: " << t << "\n";
    	///THE MAIN TIME LOOP START
    		if (if_paint && (t % periods == 0)){
    			if (rank==0){ 
        			for (int i = 0; i < p; i++){
					if (i != 0 ){
						for (int j = 0; j < N_per_node-2; j++)
							MPI_Recv(collectmessage[j], max_particle_size, MPI_DOUBLE, i/*recv from*/, j/*tag*/, comm, &status);	
					}else{
						for(int j = 0; j < N_per_node-2; j++)
							for(int k =0; k < max_particle_size; k++)
								collectmessage[j][k] = u[j+1][k];
					}
					for (int j = 0; j < N_per_node-2 ; j++)
						for (int k = 0; k < max_particle_size ; k++){
							ucollect[j+i*(N_per_node-2)][k] = collectmessage[j][k];
						}
				}
    			} else {
	    			for (int i = 0; i < N_per_node-2; i++)
            				MPI_Ssend(u[i+1], max_particle_size, MPI_DOUBLE, 0/*send to*/, i/*tag*/, comm);
    			}
    		}	
		//MPI_Barrier(comm);
		if (rank==0){
			if(if_paint && (t % periods == 0)){
				for(int i = 0; i < max_distance_pml; i++){
                			for(int m = 0; m < max_particle_size; m++) fprintf(output2d, "%f ", ucollect[i][m]);
                			fprintf(output2d, "\n");
            			}
            			fprintf(output2d, "\n\n");
	    			for (int i = 0; i < max_distance_pml; i++) 
					delete [] ucollect[i];
	    			delete [] ucollect;
	    	
	    			ucollect = new double * [max_distance_pml];
    	    			for (int i = 0; i < max_distance_pml; i++)
    	    				ucollect[i] = new double [max_particle_size];
				
				for (int i = 0; i < N_per_node-2; i++) 
					delete [] collectmessage[i];
	    			delete [] collectmessage;
	
				collectmessage = new double * [N_per_node-2];
    	    			for (int i = 0; i < N_per_node-2; i++) 
					collectmessage[i] = new double [max_particle_size];
			}
		}
		MPI_Barrier(comm);
		// COLLECTION ENDS HERE
        	///smoluchowski operator start
        	for (int j = (rank==0 ? 2 : 1); j < N_per_node-1; j++){
            		for (int m = 0; m < max_particle_size; m++)
                		n_k[m] = u[j][m];
            		/// fast computations of L1 and L2
            		L2_res_vec = crossed_kernel.matvec(n_k);
            		L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k);
            		/// compute corrections for L2 into trapecial weights
            		for (int i = 0; i < max_particle_size; i++){
                		L2_correction[i] = ( K( i, 1 ,dm ) * n_k[0] + K(i, max_particle_size ,dm) * n_k[max_particle_size - 1] ) * dm;
            		}
            		cblas_daxpy(max_particle_size, -0.5, L2_correction, 1, L2_res_vec, 1);
            		for (int i = 0; i < max_particle_size; i++){
                		smoluch_operator[j][i] = ( L1_res_vec[i] * 0.5 - n_k[i] * L2_res_vec[i] );
                		if (smoluch_operator[j][i] < 0.0) smoluch_operator[j][i] = 0.0;
            		}
            		free (L2_res_vec);
            		L2_res_vec = NULL;
            		free (L1_res_vec);
            		L1_res_vec = NULL;
            		free(n_k);
            		n_k = (double *) malloc (max_particle_size * sizeof(double));
			free(L2_correction);
			L2_correction = (double *) malloc (max_particle_size * sizeof(double));
        	} ///now smoluch_operator <==> S(f^n,f^n)
        	///smoluchowski operator end
        	limiters(dm, dr, dt, N_per_node, max_particle_size, PML, u, p, rank, comm, status, request, max_distance_pml); /// now u <==> A(f^n)
        	///calculation of next time layer start
        	for (int i = (rank==0 ? 2 : 1); i < N_per_node-1; i++){
            		for (int m = 0; m < max_particle_size; m++){
                		u[i][m] = initial_layer[i][m] + dt * (smoluch_operator[i][m] - u[i][m]); ///now u <==> f^(n+1)(x,v)
            		}
        	}

        	for (int i = 0; i < N_per_node; i++)
        		delete [] smoluch_operator[i];
        	for (int i = 0; i < N_per_node; i++)
        		delete [] initial_layer[i];
        	delete [] initial_layer;
        	delete [] smoluch_operator;

        	initial_layer = new double * [N_per_node];
        	smoluch_operator = new double * [N_per_node];
        	for (int i = 0; i < N_per_node; i++){
        		initial_layer[i] = new double [max_particle_size];
        		smoluch_operator[i] = new double [max_particle_size];
		}

        	for (int i = 1; i < N_per_node-1; i++){
        		for (int m = 0; m < max_particle_size; m++){
                		initial_layer[i][m] = u[i][m];
                		smoluch_operator[i][m] = u[i][m];
            		}
        	}
		for (int i = 0; i < N_per_node; i++)
			delete [] u[i];
		delete [] u;

		u = new double * [N_per_node];
		for (int i = 0; i < N_per_node; i++)
			u[i] = new double [max_particle_size];

		for (int i = 1; i < N_per_node-1; i++)
        		for (int m = 0; m < max_particle_size; m++)
                		u[i][m] = initial_layer[i][m];
        	///calculation of next time layer end

	} /// THE MAIN TIME LOOP END
	MPI_Barrier(comm);	
	double end;
	if(rank == 0) {
		end = MPI_Wtime();
		printf("duration: %f seconds\n", end-start);
                printf("kernel rank = %d\n", crossed_kernel.get_rank());
	}
	for (int i = 0; i < N_per_node; i++) delete [] smoluch_operator[i];
    	delete [] smoluch_operator;
    	for (int i = 0; i < N_per_node; i++) delete [] initial_layer[i];
    	delete [] initial_layer;
    	for (int i = 0; i < N_per_node; i++) delete [] u[i];
    	delete [] u;
    	free(n_k);

	if (rank==0){
    		if (if_paint){
        		fclose(output2d);
    		}
	}
	MPI_Finalize();
    	return 0;
}
