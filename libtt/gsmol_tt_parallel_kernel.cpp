#include <omp.h>
#include <stddef.h>
#include <cstring>
#include <cmath>

#include "gsmol_kernel.h"
#include "gsmol_tt_parallel_kernel.h"
// #define MKL_Complex16 std::complex<double>

//============================================================================//
//    Default constructor                                                     //
//============================================================================//
TKernel_TT::TKernel_TT(
    void
):
    D(0),
    A(0),
    N(0),
    R(0),
    L(0),
    mult(NULL),
    kers_tt(NULL),
    psize(0),
    prank(0),
    loads(NULL),
    mchunk(0),
    mld(0),
    moff(0),
    H(0),
    comms(NULL),
    descs(NULL),
    dones(NULL),
    aux(NULL),
    faux(NULL),
    ranks_appr(NULL),
    t_appr(0),
    t_compress(0),
    t_transfer(0)
{}

//============================================================================//
//    Kernels initialization                                                  //
//============================================================================//
TKernel_TT::TKernel_TT(
    const char * kname,
    // in: type of collisions
    const char * ktype,
    // in: kernels maximal dimension
    const int & kdim,
    // in: quantity of equations
    const int & kmode,
    // in: weights in front of kernels
    const double * kw,
    // in: tolerance of approximation
    const double & ktol,
    // in: MPI thread support
    const int & support
):
    D(kdim),
    A((!strcmp(ktype, "all"))? kdim - 1: 1),
    N(kmode),
    R(1),
    L(1),
    t_transfer(0)
{
    MPI_Comm_size(MPI_COMM_WORLD, &psize);
    MPI_Comm_rank(MPI_COMM_WORLD, &prank);

    kers_tt = (TTensorTrain **)malloc(A * sizeof(TTensorTrain *));
    loads = (int **)malloc(A * sizeof(int *));
    ranks_appr = (int *)malloc((D + 1) * sizeof(int)); 
    mult = (double *)calloc(A, sizeof(double));

    // initialize kernels multipliers 
    double factor = 0.5;

    mult[0] = (A == 1)? kw[0] / (double)fact(D): factor * kw[0];
   
    for (int a = 1; a < A; ++a)
    {
        factor /= (double)(a + 2);
        mult[a] = factor * kw[a];
    }

    // temporary 
    int dim;
/// good ///    int rem;
    int * ranks = NULL;
    double ** carriages = NULL;
/// good ///    double * car = NULL; 
/// good ///    int * ploads = NULL; 
/// good ///    int * poffs = NULL;
/// good ///    MPI_Datatype TVec_send;
/// good ///    MPI_Datatype TVec_send_res;
/// good ///    MPI_Datatype TVec_recv;
/// good ///    MPI_Datatype TVec_recv_res;
/// good ///
/// good ///    // initialize carriages partitions and offsets on root
/// good ///    if (!prank)
/// good ///    {
/// good ///        ploads = (int *)malloc(psize * sizeof(int)); 
/// good ///        poffs = (int *)malloc(psize * sizeof(int));
/// good ///    }
/// good ///

    //=================================//
    //    TT-kernels initialization    //
    //=================================//
    for (int a = 0; a < A; ++a)
    {
        dim = (a < A - 1)? a + 2: D;

        kers_tt[a] = new TTensorTrain();

        // allocate navigational arrays
        loads[a] = (int *)malloc((dim - 1) * sizeof(int));

        //================================//
        //    TT-kernels approximation    //
        //================================//
        MPI_Barrier(MPI_COMM_WORLD);
        if (!prank)
        {
            int * modes = (int *)malloc(dim * sizeof(int));

            // initialize kernel modes
            modes[0] = N;
            copy(dim, modes, 0, modes, 1); 

            // initialize kernel 
            TKernel * ker = new TKernel(dim, modes, kname);

            // initialize parameters for TT-cross approximation 
            TTensorTrainParameters pars;

            // convergence tolerance
            pars.tolerance = ktol;
            // number of iterations
            // if zero then iterates until convergence
            pars.maximal_iterations_number = 0;

            // TT-cross approximation
            t_appr = MPI_Wtime();
            (kers_tt[a])->Approximate(ker, pars);
            t_appr = MPI_Wtime() - t_appr;

            printf(
                "Approximation finished p = %d, N = %d, d = %d, t = %.2f\n",
                psize, N, dim, t_appr
            );

            // save approximation ranks of the highest TT-kernel
            if (a == A - 1)
            {
                copy(D + 1, (kers_tt[A - 1])->ranks, 1, ranks_appr, 1);
            }

            // SVD-compression
            // unused for now
            t_compress = 0;
            // t_compress = MPI_Wtime();
            // (kers_tt[a])->SVDCompress(parameters.tolerance / 1.);
            // t_compress = MPI_Wtime() - t_compress;

            delete ker;
        }

        MPI_Barrier(MPI_COMM_WORLD);
        if (prank)
        // allocate TT-kernel on non-root processors
        {
            (kers_tt[a])->modes_sizes = (int *)malloc(dim * sizeof(int));
            (kers_tt[a])->ranks = (int *)malloc((dim + 1) * sizeof(int));
            (kers_tt[a])->carriages = (double **)malloc(dim * sizeof(double *));

            // initialize kernel modes
            (kers_tt[a])->modes_sizes[0] = N;
            copy(
                dim, (kers_tt[a])->modes_sizes, 0, (kers_tt[a])->modes_sizes, 1
            ); 
        }

        // broadcast dimensionality
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(
            &((kers_tt[a])->dimensionality), 1, MPI_INT, 0, MPI_COMM_WORLD
        );

        // broadcast ranks 
        MPI_Bcast(
            (kers_tt[a])->ranks, dim + 1, MPI_INT, 0, MPI_COMM_WORLD
        );

        ranks = (kers_tt[a])->ranks;
        carriages = (kers_tt[a])->carriages;

        //==============================//
        //    Carriages distribution    //
        //==============================//
        for (int d = 0; d < dim - 1; ++d)
        {
            // initialize navigational arrays
            loads[a][d] = ranks[d] * ranks[d + 1];

            // update max rank
            if (R < ranks[d + 1])
            {
                R = ranks[d + 1];
            }

            // update max load
            if (L < loads[a][d])
            {
                L = loads[a][d];
            }

            if (prank)
            {
                carriages[d] = (double *)malloc(
                    N * loads[a][d] * sizeof(double)
                );
            }

            if (psize > 1)
            {
                MPI_Barrier(MPI_COMM_WORLD);
                MPI_Bcast(
                    carriages[d], N * loads[a][d], MPI_DOUBLE, 0, MPI_COMM_WORLD
                );
            }
        }

        // allocate last carriage on non-root
        if (prank)
        {
            carriages[dim - 1] = (double *)malloc(
                N * ranks[dim - 1] * sizeof(double)
            );
        }

        // broadcast last carriage
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(
            carriages[dim - 1], N * ranks[dim - 1],
            MPI_DOUBLE, 0, MPI_COMM_WORLD
        );

        MPI_Barrier(MPI_COMM_WORLD);
    }
  
    // broadcast approximation ranks of the highest TT-kernel 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(
        ranks_appr, D + 1, MPI_INT, 0, MPI_COMM_WORLD
    );

    // allocate auxiliary memory
    aux = (double *)malloc(((((L + R) << 1) + 1) * N + L) * sizeof(double));

    dones = (double *)malloc(((R < N)? N: R) * sizeof(double));
    dones[0] = 1.;
    copy(((R < N)? N: R), dones, 0, dones, 1);

    H = omp_get_max_threads();
    /// debug ///H = 1;
    H = (support == MPI_THREAD_MULTIPLE)? ((L < H)? L: H): 1;

    comms = (MPI_Comm *)malloc(H * sizeof(MPI_Comm));

    for (int h = 0; h < H; ++h)
    {
        MPI_Comm_dup(MPI_COMM_WORLD, comms + h);
    }

    descs = (DFTI_DESCRIPTOR_HANDLE *)malloc(
        H * sizeof(DFTI_DESCRIPTOR_HANDLE)
    );

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        if (h != omp_get_thread_num())
        {
            if (!prank)
            {
                printf("ERROR: TT-kernel constructor:"
                    " Thread to CDFT descriptor mismatch\n");
                fflush(stdout);
            }

            MPI_Abort(MPI_COMM_WORLD, 2);
        }

        MKL_LONG status;

        status = DftiCreateDescriptorDM(
            comms[h], descs + h, DFTI_DOUBLE, DFTI_COMPLEX, 1, N
        );

        status = DftiSetValueDM(
            descs[h], DFTI_BACKWARD_SCALE, 0.5 / (double)N
        );
        status = DftiSetValueDM(descs[h], DFTI_PLACEMENT, DFTI_NOT_INPLACE);
        status = DftiSetValueDM(
            descs[h], DFTI_ORDERING, DFTI_BACKWARD_SCRAMBLED
        );

        /// debug ///MKL_LONG of, ld, ch;
        /// debug ///status = DftiGetValueDM(descs[h], CDFT_LOCAL_X_START, &of);
        /// debug ///status = DftiGetValueDM(descs[h], CDFT_LOCAL_SIZE, &ld);
        /// debug ///status = DftiGetValueDM(descs[h], CDFT_LOCAL_NX, &ch);
        /// debug ///printf(
        /// debug ///    "p = %d, t = %d, off = %d, ld = %d, chunk = %d\n",
        /// debug ///    prank, omp_get_thread_num(), of, ld, ch
        /// debug ///);
        /// debug ///fflush(stdout);
        //

        status = DftiCommitDescriptorDM(descs[h]);
        /// debug ///printf(
        /// debug ///    "n = %d, p = %d, t = %d, status = %d, %s, H = %d\n",
        /// debug ///    prank * omp_get_max_threads() + omp_get_thread_num(),
        /// debug ///    prank, omp_get_thread_num(), status,
        /// debug ///    (status == DFTI_NO_ERROR)? "OK": "ERROR", H 
        /// debug ///);
        /// debug ///fflush(stdout);
    }

    MKL_LONG status;

    status = DftiGetValueDM(descs[0], CDFT_LOCAL_X_START, &moff);
    status = DftiGetValueDM(descs[0], CDFT_LOCAL_SIZE, &mld);
    status = DftiGetValueDM(descs[0], CDFT_LOCAL_NX, &mchunk);

    faux = (std::complex<double> *)mkl_malloc(
        3 * H * mld * sizeof(std::complex<double>), 64
    );

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        MKL_LONG status;

        MPI_Barrier(MPI_COMM_WORLD);
        status = DftiComputeForwardDM(
            descs[h], faux + (h << 1) * mld, faux + ((h << 1) + 1) * mld
        );
        status = DftiComputeBackwardDM(
            descs[h], faux + (h << 1) * mld, faux + ((h << 1) + 1) * mld
        );
    }

    /// debug ///MPI_Barrier(MPI_COMM_WORLD);
    /// debug ///if (support == MPI_THREAD_MULTIPLE)
    /// debug ///{
    /// debug ///    printf("#thread = %d\n", omp_get_max_threads());
    /// debug ///    fflush(stdout);
    /// debug ///    MPI_Barrier(MPI_COMM_WORLD);
    /// debug ///    MPI_Abort(MPI_COMM_WORLD, 0);
    /// debug ///} else {
    /// debug ///    printf("FOREVER ALONE\n");
    /// debug ///    MPI_Barrier(MPI_COMM_WORLD);
    /// debug ///    MPI_Abort(MPI_COMM_WORLD, 0);
    /// debug ///}

    return;
}
/// good ///    // temporary 
/// good ///    int dim;
/// good ///    int rem;
/// good ///    int * ranks = NULL;
/// good ///    double ** carriages = NULL;
/// good ///    double * car = NULL; 
/// good ///    int * ploads = NULL; 
/// good ///    int * poffs = NULL;
/// good ///    MPI_Datatype TVec_send;
/// good ///    MPI_Datatype TVec_send_res;
/// good ///    MPI_Datatype TVec_recv;
/// good ///    MPI_Datatype TVec_recv_res;
/// good ///
/// good ///    // initialize carriages partitions and offsets on root
/// good ///    if (!prank)
/// good ///    {
/// good ///        ploads = (int *)malloc(psize * sizeof(int)); 
/// good ///        poffs = (int *)malloc(psize * sizeof(int));
/// good ///    }
/// good ///
/// good ///    //=================================//
/// good ///    //    TT-kernels initialization    //
/// good ///    //=================================//
/// good ///    for (int a = 0; a < A; ++a)
/// good ///    {
/// good ///        dim = (a < A - 1)? a + 2: D;
/// good ///
/// good ///        kers_tt[a] = new TTensorTrain();
/// good ///
/// good ///        // allocate navigational arrays
/// good ///        loads[a] = (int *)malloc((dim - 1) * sizeof(int));
/// good ///
/// good ///        //================================//
/// good ///        //    TT-kernels approximation    //
/// good ///        //================================//
/// good ///            // TT-cross approximation
/// good ///
/// good ///        ranks = (kers_tt[a])->ranks;
/// good ///        carriages = (kers_tt[a])->carriages;
/// good ///
/// good ///        //==============================//
/// good ///        //    Carriages distribution    //
/// good ///        //==============================//
/// good ///        for (int d = 0; d < dim - 1; ++d)
/// good ///        {
/// good ///            // initialize navigational arrays
/// good ///            loads[a][d] = (ranks[d] * ranks[d + 1]) / psize;
/// good ///            rem = (ranks[d] * ranks[d + 1]) % psize;
/// good ///            offs[a][d] = loads[a][d] * prank + ((prank < rem)? prank: rem);
/// good ///
/// good ///            if (prank < rem)
/// good ///            {
/// good ///                ++(loads[a][d]);
/// good ///            }
/// good ///
/// good ///            lchunks[a][d] = ranks[d + 1] - (offs[a][d] % ranks[d + 1]);
/// good ///
/// good ///            if (loads[a][d] < lchunks[a][d])
/// good ///            {
/// good ///                lchunks[a][d] = loads[a][d];
/// good ///            }
/// good ///
/// good ///            if (lchunks[a][d] == ranks[d + 1])
/// good ///            {
/// good ///                lchunks[a][d] = 0;
/// good ///            }
/// good ///
/// good ///            ranks[d] = (offs[a][d] + loads[a][d]) / ranks[d + 1]
/// good ///                - (offs[a][d] + lchunks[a][d]) / ranks[d + 1];
/// good ///            rchunks[a][d]
/// good ///                = loads[a][d] - lchunks[a][d] - ranks[d] * ranks[d + 1];
/// good ///
/// good ///            if (psize > 1)
/// good ///            {
/// good ///                // form partition of d-th carriage
/// good ///                MPI_Barrier(MPI_COMM_WORLD);
/// good ///                MPI_Gather(
/// good ///                    loads[a] + d, 1, MPI_INT,
/// good ///                    ploads, 1, MPI_INT, 0, MPI_COMM_WORLD 
/// good ///                );
/// good ///
/// good ///                // form offsets of d-th carriage
/// good ///                MPI_Gather(
/// good ///                    offs[a] + d, 1, MPI_INT,
/// good ///                    poffs, 1, MPI_INT, 0, MPI_COMM_WORLD 
/// good ///                );
/// good ///
/// good ///                if (!prank)
/// good ///                {
/// good ///                    // temporary assign to d-th carriage
/// good ///                    car = carriages[d];
/// good ///
/// good ///                    // commit derived MPI datatype to send
/// good ///                    MPI_Type_vector(
/// good ///                        N, 1, ranks[d] * ranks[d + 1], MPI_DOUBLE, &TVec_send
/// good ///                    ); 
/// good ///                    MPI_Type_commit(&TVec_send); 
/// good ///
/// good ///                    MPI_Type_create_resized(
/// good ///                        TVec_send, 0, sizeof(double), &TVec_send_res
/// good ///                    );
/// good ///                    MPI_Type_commit(&TVec_send_res);
/// good ///                }
/// good ///
/// good ///                // allocate d-th partial carriage
/// good ///                MPI_Barrier(MPI_COMM_WORLD);
/// good ///                carriages[d] = (loads[a][d])?
/// good ///                    (double *)malloc(N * loads[a][d] * sizeof(double)):
/// good ///                    NULL;
/// good ///
/// good ///                // commit derived MPI datatype to receive
/// good ///                MPI_Type_vector(N, 1, loads[a][d], MPI_DOUBLE, &TVec_recv); 
/// good ///                MPI_Type_commit(&TVec_recv); 
/// good ///
/// good ///                MPI_Type_create_resized(
/// good ///                    TVec_recv, 0, sizeof(double), &TVec_recv_res
/// good ///                );
/// good ///                MPI_Type_commit(&TVec_recv_res);
/// good ///                
/// good ///                // distribute d-th carriage
/// good ///                MPI_Barrier(MPI_COMM_WORLD);
/// good ///                MPI_Scatterv(
/// good ///                    car, ploads, poffs, TVec_send_res,
/// good ///                    carriages[d], loads[a][d], TVec_recv_res,
/// good ///                    0, MPI_COMM_WORLD
/// good ///                );
/// good ///
/// good ///                // clean-up
/// good ///                MPI_Type_free(&TVec_recv);
/// good ///                MPI_Type_free(&TVec_recv_res);
/// good ///
/// good ///                if (!prank)
/// good ///                {
/// good ///                    // free old d-th carriage on root
/// good ///                    free(car);
/// good ///
/// good ///                    MPI_Type_free(&TVec_send);
/// good ///                    MPI_Type_free(&TVec_send_res);
/// good ///                }
/// good ///            }
/// good ///        }
/// good ///
/// good ///        // allocate last carriage on non-root
/// good ///        if (prank)
/// good ///        {
/// good ///            carriages[dim - 1] = (double *)malloc(
/// good ///                N * ranks[dim - 1] * sizeof(double)
/// good ///            );
/// good ///        }
/// good ///
/// good ///        // broadcast last carriage
/// good ///        MPI_Barrier(MPI_COMM_WORLD);
/// good ///        MPI_Bcast(
/// good ///            carriages[dim - 1], N * ranks[dim - 1],
/// good ///            MPI_DOUBLE, 0, MPI_COMM_WORLD
/// good ///        );
/// good ///
/// good ///        MPI_Barrier(MPI_COMM_WORLD);
/// good ///    }

//============================================================================//
//    Kernels deallocation                                                    //
//============================================================================//
TKernel_TT::~TKernel_TT(
    void
)
{
    if (mult)
    {
        free(mult);
    }

#pragma omp parallel for 
    for (int a = 0; a < A; ++a)
    {
        delete kers_tt[a];

        free(loads[a]);
    }

    if (kers_tt)
    {
        free(kers_tt);
    }

    if (loads)
    {
        free(loads);
    }

#pragma omp parallel for
    for (int h = 0; h < H; ++h)
    {
        DftiFreeDescriptor(descs + h);
        MPI_Comm_free(comms + h);
    }

    if (comms)
    {
        free(comms);
    }

    if (descs)
    {
        free(descs);
    }

    if (dones)
    {
        free(dones);
    }

    if (aux)
    {
        free(aux);
    }

    if (faux)
    {
        mkl_free(faux);
    }

    if (ranks_appr)
    {
        free(ranks_appr);
    }

    return;
}

//// //============================================================================//
//// //    Binary convolution using +1 and -1 circulant                            //
//// //============================================================================//
//// void TKernel_TT::conv(
////     // in: index of DFTI descriptor
////     const int & h,
////     // in: arg1
////     const double * arg1,
////     // in: arg2
////     const double * arg2,
////     // out: arg1 * arg2
////     double * res
//// )
//// {
////     const double dzero = 0.;
////     int status;
////     std::complex<double> * farg1 = faux + 3 * h * mld;
////     std::complex<double> * farg2 = farg1 + mld;
////     std::complex<double> * fimg = farg2 + mld;
//// 
////     //====================//
////     //    +1-circulant    //
////     //====================//
////     copy(mchunk, arg1, 1, (double *)farg1, 2);
////     copy(mchunk, &dzero, 0, ((double *)farg1) + 1, 2);
////     copy(mchunk, arg2, 1, (double *)farg2, 2);
////     copy(mchunk, &dzero, 0, ((double *)farg2) + 1, 2);
//// 
////     status = DftiComputeForward(descs[h], farg1, fimg);
////     status = DftiComputeForward(descs[h], farg2, farg1);
//// 
//// //#pragma omp parallel for
////     for (int i = 0; i < mld; ++i)
////     {
////         fimg[i] *= farg1[i];
////     }
//// 
////     status = DftiComputeBackward(descs[h], fimg, farg1);
////     copy(mchunk, (double *)farg1, 2, res, 1);
//// 
////     //====================//
////     //    -1-circulant    //
////     //====================//
//// //#pragma omp parallel for
////     for (int i = 0; i < mchunk; ++i)
////     {
////         // double c = cos(-M_PI * i / (double)N);
////         // int j = i << 1;
////         // ((double *)farg1)[j] = arg1[i] * c;
////         // ((double *)farg2)[j] = arg2[i] * c;
//// 
////         // c = sin(-M_PI * i / (double)N);
////         // ++j;
////         // ((double *)farg1)[j] = arg1[i] * c;
////         // ((double *)farg2)[j] = arg2[i] * c;
////         ((double *)farg1)[2 * i]
////             = arg1[i] * cos(M_PI * (moff + i) / (double)N);
////         ((double *)farg1)[2 * i + 1]
////             = -arg1[i] * sin(M_PI * (moff + i) / (double)N);
//// 
////         ((double *)farg2)[2 * i]
////             = arg2[i] * cos(M_PI * (moff + i) / (double)N);
////         ((double *)farg2)[2 * i + 1]
////             = -arg2[i] * sin(M_PI * (moff + i) / (double)N);
////     }
//// 
////     status = DftiComputeForward(descs[h], farg1, fimg);
////     status = DftiComputeForward(descs[h], farg2, farg1);
//// 
//// //#pragma omp parallel for
////     for (int i = 0; i < mld; ++i)
////     {
////         fimg[i] *= farg1[i];
////     }
//// 
//// //#pragma omp parallel for
////     for (int i = 0; i < mchunk; ++i)
////     {
////         // res[i] += cos(M_PI * i / (double)N) * ((double *)farg1)[i << 1]
////         //     - sin(M_PI * i / (double)N) * ((double *)farg1)[(i << 1) + 1];
////         res[i] += cos(M_PI * (moff + i) / (double)N) * ((double *)farg1)[2 * i]
////             - sin(M_PI * (moff + i) / (double)N) * ((double *)farg1)[2 * i + 1];
////     }
//// 
////     return;
//// }

//============================================================================//
//    Smoluchowski operator for a-th kernel                                   //
//============================================================================//
void TKernel_TT::gsmol(
    // in: index of kernel to apply
    const int & a,
    // in:
    const double * arg,
    // out: S^(a)(arg)
    double * res
) {
    const int dim = (kers_tt[a])->dimensionality;
    int * ranks = (kers_tt[a])->ranks;
    double ** carriages = (kers_tt[a])->carriages;

    const double dzero = 0.;

    // aux ~ ((d - 2) * R + 2) * R * N / p
    // farg ~
    // fres ~
    double * farg = aux;

    // where to keep current matvec-updated right factor  
    double * tmp = rucarc;

    // printf("prank = %d, mchunk = %d, mld = %d\n", prank, mchunk, mld);
    // fflush(stdout);
    //====================================//
    //    Last carriage partial update    //
    //====================================//
    // tmp[rr] = carriages[dim - 1][rr] x arg
#pragma omp parallel for collapse(2)
    for (int r = 0; r < ranks[dim - 1]; ++r)
    {
        for (int i = 0; i < mchunk; ++i)
        {
            tmp[
                i + ((offs[a][dim - 2] + r) % ranks[dim - 1]) * mchunk
            ]
            = carriages[dim - 1][
                r % ranks[dim - 1] + (moff + i) * ranks[dim - 1]
            ] * arg[i];
        }
    }

    for (int d = dim - 2; d >= 0; --d)
    {
        //=========================================//
        //    Next left carriage partial update    //
        //=========================================//
        // lucar[lr][rr] = carriages[d][lr][rr] x arg
#pragma omp parallel for collapse(2)
        for (int r = 0; r < loads[a][d]; ++r)
        {
            for (int i = 0; i < mchunk; ++i)
            {
                lucar[i + r * mchunk]
                    = carriages[d][r + (moff + i) * loads[a][d]] * arg[i];
            }
        }

    // printf("prank = %d, START\n", prank);
    // fflush(stdout);

////////////////////////////////////////////////////////////////////////////////
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        //========================================//
        //    Convolution along mode dimension    //
        //========================================//
        // conv[lr][rr] = lucar[lr][rr] * rucarc[rr]
#pragma omp parallel for
        for (int r = 0; r < loads[a][d]; ++r)
        {
            int addr = (r % ranks[d + 1]) * ranks[d] + r / ranks[d + 1];

            MPI_Barrier(MPI_COMM_WORLD);
            this->conv(
                omp_get_thread_num(),
                lucar + r * mchunk,
                rucarc + (r % ranks[d + 1]) * mchunk,
                conv + addr * mchunk
            );
        }
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        //=================================================//
        //    Sum of left carriage along mode dimension    //
        //=================================================//
        // aux = lucar · [1]
        gemv(
            CblasRowMajor, CblasNoTrans,
            loads[a][d], mchunk, dones[0],
            lucar, mchunk,
            dones, 1,
            dzero, aux, 1
        );

        if (psize > 1)
        {
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Allreduce(
                MPI_IN_PLACE, aux, loads[a][d],
                MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD
            );
        }

        //==================================================//
        //    Partial product along right rank dimension    //
        //==================================================//
        // lucar = tmp · aux^T
        gemm(
            CblasColMajor, CblasNoTrans, CblasNoTrans,
            mchunk, ranks[d], ranks[d + 1], dones[0],
            tmp, mchunk,
            aux, ranks[d + 1],
            dzero, lucar, mchunk
        );

        // rucarc = [0]
        // rucarp = [0]
        copy(2 * ranks[d] * mchunk, &dzero, 0, rucarc, 1); 

        // rucarp = lucar
        copy(ranks[d] * mchunk, lucar, 1, rucarp, 1);

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        //======================================================//
        //    Sum of convolutions along right rank dimension    //
        //======================================================//
        // rucarc = conv · [1]
        gemv(
            CblasColMajor, CblasNoTrans,
            ranks[d] * mchunk, ranks[d + 1],
            dones[0], conv, ranks[d] * mchunk,
            dones, 1,
            dzero, rucarc, 1
        );
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        if (d)
        {
            tmp = rucarp;
            rucarp = rucarc + ranks[d - 1] * mchunk;
        }
    }

    //=======================//
    //    Result finalize    //
    //=======================//
	// Exchange of mode chunks boundaries 
    MPI_Request mpi_request;
    MPI_Status mpi_status;

    MPI_Barrier(MPI_COMM_WORLD);
	if (prank != psize - 1)
    {
		MPI_Isend(
            rucarc + (mchunk - dim + 1), dim - 1, MPI_DOUBLE,
            prank + 1, prank, MPI_COMM_WORLD, &mpi_request
        );
    }

	if (prank)
    {
		MPI_Recv(
            res, dim - 1, MPI_DOUBLE,
            prank - 1, prank - 1, MPI_COMM_WORLD, &mpi_status
        );
    }

    // res = 0
    if (!prank)
    {
        copy(dim - 1, &dzero, 0, res, 1); 
    }

    // res = rucarc >> dim - 1
    copy(mchunk - dim + 1, rucarc, 1, res + dim - 1, 1);
    /// check ///copy(mchunk, &dzero, 0, res, 1);
    /// check ///copy(mchunk, rucarc, 1, res, 1);

    // res -= (a + 2) * rucarp
    axpy(mchunk, -(double)((A == 1)? D: a + 2), rucarp, 1, res, 1);
    scal(mchunk, mult[a], res, 1);

    MPI_Barrier(MPI_COMM_WORLD);
    return;
}

//============================================================================//
//    Outcome part of Smoluchowski operator                                   //
//============================================================================//
void gmatvec(
    // in: index of kernel to apply
    const int & a,
    // in: 
    const double * arg,
    // out: outcome part of S^(a)(arg)
    double * res
)
{
    const int dim = (kers_tt[a])->dimensionality;
    int * ranks = (kers_tt[a])->ranks;
    double ** carriages = (kers_tt[a])->carriages;

    const double dzero = 0.;

    return;
}

//============================================================================//
//    Smoluchowski operator                                                   //
//============================================================================//
void TKernel_TT::compute(
    // in:
    const double * arg,
    // out: S(arg)
    double * res
) {
    double * tmp = aux + ((L + R) << 1) * mchunk + L;

    this->gsmol(0, arg, res);

    /// debug ///this->conv(0, arg, arg, res);

    for (int a = 1; a < A; ++a)
    {
        this->gsmol(a, arg, tmp);
        axpy(mchunk, 1., tmp, 1, res, 1);
    }

    status = DftiComputeBackward(descs[h], fimg, farg1);

    return;
}

//============================================================================//
//    Gather mode chunks                                                      //
//============================================================================//
void TKernel_TT::collect(
    // alt:
    double * arg
) {
    int * pmchunks = NULL; 
    int * pmoffs = NULL;

    if (!prank)
    {
        pmchunks = (int *)malloc(psize * sizeof(int)); 
        pmoffs = (int *)malloc(psize * sizeof(int));
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gather(
        &mchunk, 1, MPI_INT,
        pmchunks, 1, MPI_INT, 0, MPI_COMM_WORLD 
    );

    MPI_Gather(
        &moff, 1, MPI_INT,
        pmoffs, 1, MPI_INT, 0, MPI_COMM_WORLD 
    );

    if (!prank)
    {
        pmchunks[0] = 0;
    }

    MPI_Gatherv(
        (!prank)? MPI_IN_PLACE: arg, mchunk, MPI_DOUBLE,
        arg, pmchunks, pmoffs,
        MPI_DOUBLE, 0, MPI_COMM_WORLD
    );

    if (!prank)
    {
        free(pmchunks);
        free(pmoffs);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    return;
}
