#ifndef JACOBI_PRECONDITIONER_HPP
#define JACOBI_PRECONDITIONER_HPP

#include "../../../libtt/parallel_cross_omp.h"
#include "band_matrix.hpp"
#include <iostream>
#include <vector>

namespace Jacobi {

template <typename K>
BandMatrix makeBand(int N, int p, K kernel, TCross_Parallel_v1 &kernel_cross,
                    double lambda, double *n) {
  if (p < 1)
    p = 1;
  double *diag = kernel_cross.matvec(n, 'f');
  std::vector<double> band((N - 1) * p);
  scal(N - 1, -(1 + lambda), diag + 1, 1);
  copy(N - 1, diag + 1, 1, band.data(), p);

  for (int k = 1; k < p; ++k) {
    for (int j = 0; j < N - 1 - k; ++j) {
      band[j * p + k] = n[k - 1] * kernel(j + 1, k - 1);
    }
  }

  return BandMatrix(band.data(), N - 1, p);
}

class Preconditioner {
private:
  size_t n, rank;
  std::vector<double> X, Y, C;
  double D;
  std::vector<lapack_int> ipiv;
  BandMatrix band;

public:
  template <typename K>
  Preconditioner(size_t N, size_t R, K kernel, TCross_Parallel_v1 &kernel_cross,
                 double lambda, double *nvec, int p)
      : n(N), rank(R), X((R + 2) * N), Y((R + 2) * N), C((R + 2) * (R + 2)),
        D(0.0), ipiv(R + 2),
        band(Jacobi::makeBand(N, p, kernel, kernel_cross, lambda, nvec)) {
    std::vector<double> indexed_n(n);
    double *indexed_n_p = indexed_n.data();
    indexed_n.front() = 0.0;
    for (int k = 1; k < n; ++k)
      indexed_n[k] = (k + 1) * nvec[k];

    std::vector<double> cropped_n(nvec, nvec + n);
    double *cropped_n_p = cropped_n.data();
    cropped_n.front() = 0.0;

    double *indexed_n_prod = kernel_cross.matvec(indexed_n_p);
    double *n_prod = kernel_cross.matvec(cropped_n_p);

    Y[0] = 0.0;

    for (int k = 1; k < n; ++k) {
      double v = (lambda * (k + 1) - 1) * kernel(0, k);
      Y[k] = v * nvec[0];
      Y[0] += v * nvec[k];
      n_prod[k] *= k + 1;
    }

    axpy(n - 1, lambda, indexed_n_prod + 1, 1, Y.data() + 1, 1);
    axpy(n - 1, lambda, n_prod + 1, 1, Y.data() + 1, 1);
    std::free(indexed_n_prod);
    std::free(n_prod);

    Y[0] -= 2 * kernel(0, 0) * nvec[0];

    if (std::abs(Y[0] - 1.0) > 0.5) {
      D = 1.0;
      Y[0] -= 1.0;
    } else {
      D = -1.0;
      Y[0] += 1.0;
    }

    X[0] = 1;
    // done with first row

    Y[n] = 1;

    for (int k = 1; k < n; ++k) {
      X[k + n] = (nvec[k - 1] * kernel(0, k - 1));
    }

    for (size_t i = 0; i < rank; ++i) {
      X[n * (i + 2)] = 0.0;
      vdMul(n - 1, nvec + 1, kernel_cross.export_U() + n * i + 1,
            X.data() + n * (i + 2) + 1);
    }

    cblas_dscal(n * rank, -(1 + lambda), X.data() + 2 * n, 1);
    cblas_dcopy(n * rank, kernel_cross.export_V(), 1, Y.data() + 2 * n, 1);

    for (size_t i = 0; i < rank + 2; ++i) {
      X[i * n] *= D;
      band.apply(X.data() + i * n + 1, true);
    }

    for (size_t j = 0; j < rank + 2; ++j) {
      for (size_t i = 0; i < rank + 2; ++i) {
        {
          if (i == j)
            C[i + j * (rank + 2)] = 1.0;
          else
            C[i + j * (rank + 2)] = 0.0;
        }
      }
    }

    cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, rank + 2, rank + 2, n,
                1.0, Y.data(), n, X.data(), n, 1.0, C.data(), rank + 2);
    LAPACKE_dgetrf(LAPACK_COL_MAJOR, rank + 2, rank + 2, C.data(), rank + 2,
                   ipiv.data());

    rank += 2;
  }

  void apply(double *x) const;
};
}

#endif /* JACOBI_PRECONDITIONER_HPP */

