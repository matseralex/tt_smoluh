#include "mpi.h"
#include "mkl.h"
#include <stddef.h>
#include "cross.h"
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include "blas.h"
#include <omp.h>
#include "mkl_cdft.h" 
struct Send_Info
{
   int number;
   double volume;
};



struct TCross_Parallel_v1_Parameters
{
    double tolerance;
    int maximal_iterations_number, number_of_checked_elements, max_rank, rank_increase;
    bool memory_strategy;
    MPI_Comm communicator;
    TCross_Parallel_v1_Parameters():tolerance(((double) 0.0)), maximal_iterations_number(((int) 1)), number_of_checked_elements(((int) 1)), max_rank(((int) 1)), rank_increase(((int) 1)), memory_strategy(true), communicator(MPI_COMM_WORLD){}
};

struct TCross_Parallel_v1_Work_Data{
    bool *I, *J;
    double *current_row, *current_column, *vec;
    int omp_column_threads_num, omp_row_threads_num, *omp_column_start, *omp_column_num, *omp_row_start, *omp_row_num;
    TVolume max_volume;
    double global_max;
    TMatrix * matrix;
    TDifferenceMatrix work_matrix;
    TCross_Parallel_v1_Parameters  parameters;
    TCross_Parallel_v1_Work_Data(TMatrix *, TMatrix *);
    ~TCross_Parallel_v1_Work_Data();
};

class TCross_Parallel_v1: public TCross_Base<TCross_Parallel_v1_Work_Data, TCross_Parallel_v1_Parameters>
{
    private:
        double *U, *V, *C, *hat_A_inv, *RT, tolerance, norm;
        int * rows_numbers, * columns_numbers;
        void Prepare_Data(TCross_Parallel_v1_Work_Data &, const TCross_Parallel_v1_Parameters &);
        void Search_Max_Volume(TCross_Parallel_v1_Work_Data &);
        bool Stopping_Criteria(TCross_Parallel_v1_Work_Data &);
        void Update_Cross(TCross_Parallel_v1_Work_Data &);
        void get_diff_column(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
        void get_diff_row(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
        int row_comm_size, column_comm_size, processor_rank, mpi_column_start, mpi_column_num, mpi_row_start, mpi_row_num, mpi_avg_column, mpi_avg_row;
        MPI_Comm row_comm, column_comm;
        DFTI_DESCRIPTOR_DM_HANDLE desc;
        double _Complex *Vbuff;
        double _Complex *Ubuff;
        double _Complex *Voutbuff;
        double _Complex *Uoutbuff;
        double _Complex *work;
        int FFTv, FFTs;
 
    public:
        double value(const int &, const int &);
        int get_row_number(const int &) const;
        int get_column_number(const int &) const;
        TCross_Parallel_v1();
        ~TCross_Parallel_v1();
        const double *export_C(){return C;}
        const double *export_hat_A_inv(){return hat_A_inv;}
        const double *export_RT(){return RT;}
	double * matvec(double *&);
	double * smol_conv(double *&);
	double * smol_conv_discrete(double *&);
        void destroy_DFTI_desc();
        void init_DFTI_desc();


};

using namespace std;
/*template <class double>
double abs(const double & data)
{
    return (data > ((double) 0.0)) ? data : -data;
}*/
