      subroutine plot_2d_contours_of_rn_2d(Nx,Ny,rn_2d,
     &v_x,v_y,time)
c-----------------------------------------------------------------
c     Plots ten contours of n(v_x,v_y)=const_i, i=1:n_contour
c     Now n_contour=10, It was set n_contour=10 inside this subroutine.
c-----------------------------------------------------------------
      implicit none
           
c-----input 
      integer
     & Nx,Ny !dimensions of v_x,v_y arrays
      real*8    
     &rn_2d(0:Nx,0:Ny),      ! n(vx,vy) 2D concentation
     &v_x(0:Nx),v_y(0:Ny),   !v x,vy arrays
     &time                

c-----locals
      integer i,j

c-----for pgplot 
      integer n_contour
      parameter(n_contour=10) !is the number of contours

      integer n_param                          !number of input parameters
      character*1, dimension(1) :: name_param  !names  of the input parameters
      real, dimension(1) :: param             !values of the input parameters    

      real, dimension(1:Nx+1,1:Ny+1) :: f_real_2d
      real, dimension(1:Nx+1) :: x_axis          ! x mesh 
      real, dimension(1:Ny+1) :: y_axis          ! y mesh

      real contour_n_2d(n_contour)

c      write(*,*)'in mk_graph time',time

      n_param=1
      name_param(1)='t'
      param(1)=time     
c-----create real arrays

      do i=0,Nx
         x_axis(i+1)=v_x(i)
c         write(*,*)'i,x_axis(i+1)',i,x_axis(i+1)
      enddo

      do j=0,Ny
         y_axis(j+1)=v_y(j)
c         write(*,*)'j,y_axis(j+1)',j,y_axis(j+1)
      enddo

c-----shift indexes in 2D array for contour subroutine
      do i=0,Nx
         do j=0,Ny
         f_real_2d(i+1,j+1)=rn_2d(i,j)
c         write(*,*)'i,j, f_real_2d(i+1,j+1)',i,j,f_real_2d(i+1,j+1)
         enddo     
      enddo

      call contour2d(nx+1,ny+1,f_real_2d,x_axis,y_axis,
     .'n', 'vx','vy',
     .n_contour,contour_n_2d,
     .'name_param',param,n_param )

      return
      end


      subroutine plot_distribution_particle_number_mass_on_volume
     &(v_x_0,v_y_0,Nx,Ny,rn_2d,v_x,v_y,time,name,
     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
c----------------------------------------------------------------------
c     Calculate and plot
c     sectional distributions versus volume from given concentration 
c     array  n(vx,vy,t) = rn_2d(0:Nx,0:Ny) (at special output volume mesh) 
c     at given time.
c
c     particle_numb_l=integral(d_vx[0-infinity])integral(d_vy[0-infinity]
c                       (eps_l(vx,vy)n(vx,vy),t)}
c
c     component_mass_l,1=integral(d_vx[0-infinity])integral(d_vy[0-infinity]
c                       (eps_l(vx,vy)n(vx,vy),t)vx}
c
c     component_mass_l,2=integral(d_vx[0-infinity])integral(d_vy[0-infinity]
c                       (eps_l(vx,vy)n(vx,vy),t)vy}
c
c     The input argument name ='numerical' if rn_2d were obtained numerically
c                             ='analytical' if rn_2d were obtained analytically
c                             ='initial' in this case rn_2d is initial distribution
c                                        For initial distribution this subroutine will
c                                        calculate sectional distributions 
c                                        1)using numerical integration of initial n(vx,vy)
c                                         from array rn_2d and 
c                                        2)using analytical integration of the analytical
c                                          initial distribution   

c     Output volume mesh  vol_plot_ar(1:n_vol_plot) 
c     was set inside this subtroutine.
c     The mesh is close to the article
c     G.Palaniswaamy, S.K. Loyalka, Direct simulation
c     Monte-Calo aerosol dynamics: coagulation and collisional sampling,
c     Nuclear Technology, Vol. 156, Oct. 2006, pp.20-38
c
      implicit none
      
c-----input 
      integer
     & Nx,Ny,                !dimensions of v_x,v_y arrays
     & n_vol_plot            !dimention of ouput Loyalka volme mesh

      real*8
     &v_x_0,v_y_0,           !parameters for initial concentartions        
     &rn_2d(0:Nx,0:Ny),      !array of n concentration at v_x,v_y mesh
     &v_x(0:Nx),v_y(0:Ny),   !vx,vy mesh 
     &time,
     &vol_plot_ar(0:n_vol_plot)  !Loyalka output volume mesh

      character*(*) name     
c-----output
      real*8
     &component_mass_ar(1:n_vol_plot,1:2), !sectional distributions: 
                                           ! components volumes (masses) 
     &particle_numb_ar(1:n_vol_plot)       !sectional distributions: 
                                           !particles numbers

c-----locals     
      real*8 :: step_x,step_y
      integer i,j,l,k

      integer n_param                    !number of parameters at figure
      character*(20) name_param(4)       !names  of parameters at figure
      real param(4)                      !values of parameters at figure 
      real*8, dimension(1:n_vol_plot) :: x_ar_l,y_ar_l

c-----for Drawing Markers:
      integer n_marker        ! number of points to be marked,
      parameter  (n_marker=1)   
      real  x_marker(n_marker),y_marker(n_marker)
      integer n_symbol_marker

c      write(*,*)'in plot_distribution_particle_number_mass_on_volume'
c      write(*,*)'v_x_0,v_y_0',v_x_0,v_y_0
c      write(*,*)'name=',name

      step_x=v_x(2)-v_x(1)
      step_y=v_y(2)-v_y(1)

c      write(*,*)'step_x,step_y',step_x,step_y

c-----numerical sectional distributions obtained from rn_2d
      do l=1,n_vol_plot 
         particle_numb_ar(l)=0.d0
         component_mass_ar(l,1)=0.d0 
         component_mass_ar(l,2)=0.d0
         do i=0,Nx
            do j=0,Ny
               if ((vol_plot_ar(l-1).lt.(v_x(i)+v_y(j))).and.
     &             (vol_plot_ar(l).ge.(v_x(i)+v_y(j)))) then

                    particle_numb_ar(l)=particle_numb_ar(l)+
     &                                  rn_2d(i,j)
                    component_mass_ar(l,1)=component_mass_ar(l,1)+
     &                                     rn_2d(i,j)*v_x(i)
                    component_mass_ar(l,2)=component_mass_ar(l,2)+
     &                                     rn_2d(i,j)*v_y(j)
                endif
            enddo
          enddo
          particle_numb_ar(l) = particle_numb_ar(l)*step_x*step_y
          component_mass_ar(l,1)= component_mass_ar(l,1)*step_x*step_y
          component_mass_ar(l,2)= component_mass_ar(l,2)*step_x*step_y

      enddo
c-------------------------------------------------------------
      n_param=4
      name_param(1)='v_x_0'
      name_param(2)='v_y_0'
      name_param(3)='time'
      name_param(4)=name

      param(1)=v_x_0
      param(2)=v_y_0
      param(3)=time
      param(4)=0.d0
c-----particle number
 
      do l=1,n_vol_plot
         x_ar_l(l)=vol_plot_ar(l)
         y_ar_l(l)=particle_numb_ar(l)
      enddo


c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_vol_plot/2)
      y_marker(1)=y_ar_l(n_vol_plot/2)

c      write(*,*)'bef 1 plot1dt_marker_param'

      call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_vol_plot,2,
     & n_marker,x_marker,y_marker,n_symbol_marker,    
     &'linlin',0.d0,0.d0,
     &'particle number',
     &'volume','particle number',
     &n_param,name_param,param)

c      write(*,*)'after plot1dt_marker_param'

      if(name.eq.'initial') then
c-------initial particle number using analytical integration
        do l=1,n_vol_plot
           x_ar_l(l)=vol_plot_ar(l)
           if(dabs(v_x_0-v_y_0).lt.1.d-12)then
c------------at v_x_0=v_y_0=1 rN_0=1
c            particle_numb_ar(l)=rN_0/(v_x_0*v_y_0)*
             particle_numb_ar(l)=1.d0/(v_x_0*v_y_0)*
     &       (dexp(-vol_plot_ar(l-1))-dexp(-vol_plot_ar(l)))*
     &       (vol_plot_ar(l-1)+1.d0)-
     &       (vol_plot_ar(l)-vol_plot_ar(l-1))*dexp(-vol_plot_ar(l))
           else
c------------at v_x_0ne.v_y_0=1 rN_0=1
c            particle_numb_ar(l)=rN_0/(v_x_0*v_y_0)*
             particle_numb_ar(l)=1.d0/(v_x_0*v_y_0)*
     &       (v_x_0*v_y_0)/(v_x_0-v_y_0)*
     &       ((v_y_0*dexp(-vol_plot_ar(l-1)/v_x_0)-
     &        v_x_0*dexp(-vol_plot_ar(l)/v_x_0)) +
     &        v_y_0*(dexp(-vol_plot_ar(l-1)/v_y_0)-
     &             dexp(-vol_plot_ar(l)/v_y_0))+
     &        v_x_0-v_y_0)
            endif
        enddo

        do l=1,n_vol_plot
           y_ar_l(l)=particle_numb_ar(l)
        enddo
       
c-------with marker
        n_symbol_marker=9
        x_marker(1)=x_ar_l(n_vol_plot/2)
        y_marker(1)=y_ar_l(n_vol_plot/2)

c        write(*,*)'bef 2 plot1dt_marker_param'

        call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_vol_plot,2,
     &  n_marker,x_marker,y_marker,n_symbol_marker,    
     &  'linlin',0.d0,0.d0,
     &  'analytical integr particle number',
     &  'volume',' particle number',
     &  n_param,name_param,param)
      endif ! (name.eq.'initial')
 
c-----plot mass component 1
      do l=1,n_vol_plot
         y_ar_l(l)=component_mass_ar(l,1)
      enddo

c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_vol_plot/2)
      y_marker(1)=y_ar_l(n_vol_plot/2)

c        write(*,*)'bef 2 plot1dt_marker_param'

      call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_vol_plot,2,
     & n_marker,x_marker,y_marker,n_symbol_marker,    
     &'linlin',0.d0,0.d0,
     &'component mass 1',
     &'volume','component mass',
     &n_param,name_param,param)

c-----plot mass component 2 
      do l=1,n_vol_plot
         y_ar_l(l)=component_mass_ar(l,2)
      enddo

c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_vol_plot/2)
      y_marker(1)=y_ar_l(n_vol_plot/2)

c        write(*,*)'bef 3 plot1dt_marker_param'

      call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_vol_plot,2,
     & n_marker,x_marker,y_marker,n_symbol_marker,    
     &'linlin',0.d0,0.d0,
     &'component mass 2',
     &'volume','component mass',
     &n_param,name_param,param)

c      write(*,*)'after 3 plot1dt_marker_param'

      return
      end


                
      subroutine plot_Ny_1d_curves_y_ji_xj
     &(Nx,Ny,x_1d_ar,y_2d_ar,name,
     &n_param,name_param,param)
c-----plots several(Ny)  1D curves Y(x)
c     x(1:Nx)
c     y(1:nx,1,Ny)

      implicit none     
c     implicit double precision (a-h,o-z)

c-----input
      integer :: Ny !number of curves
      integer :: Nx !number of points at each curve

      real*8, dimension(1:Nx) :: x_1d_ar
      real*8, dimension(1:Nx,1:Ny) :: y_2d_ar

      character*(*) name 

      integer  n_param                 !is the number of parameters
      character*(*) name_param(n_param) !are the names of parameters
      real param(*)                    !are the values of parameters

      REAL RILIN

      character*72 text_param             
      character*72 text
      character *7 ll

c-----locals
      integer i,j
      real xmax,xmin,ymax,ymin
      real, dimension(1:Nx) :: y_plot_ar
      real, dimension(1:Nx) :: x_plot_ar

c      character*(20) xtitle,ytitle 

      integer xtitlelen,titlelen,PGOPEN
      CHARACTER filename*40, line*1000, xtitle*100, titlestr*200
C Color index:
      INTEGER BLACK, WHITE, RED, GREEN, BLUE, CYAN, MAGENT, YELLOW
      PARAMETER (BLACK=0)
      PARAMETER (WHITE=1)
      PARAMETER (RED=2)
      PARAMETER (GREEN=3)
      PARAMETER (BLUE=4)
      PARAMETER (CYAN=5)
      PARAMETER (MAGENT=6)
      PARAMETER (YELLOW=7)
C Line style:
      INTEGER FULL, DASHED, DOTDSH, DOTTED, FANCY
      PARAMETER (FULL=1)
      PARAMETER (DASHED=2)
      PARAMETER (DOTDSH=3)
      PARAMETER (DOTTED=4)
      PARAMETER (FANCY=5)
C Character font:
      INTEGER NORMAL, ROMAN, ITALIC, SCRIPT
      PARAMETER (NORMAL=1)
      PARAMETER (ROMAN=2)
      PARAMETER (ITALIC=3)
      PARAMETER (SCRIPT=4)
C Fill-area style:
      INTEGER SOLID, HOLLOW
      PARAMETER (SOLID=1)
      PARAMETER (HOLLOW=2)
c     Conversion to real function for PGPLOT
      REAL RBOUND
c-----open PGplot
c     call plotinit

c------------------------------------------------------------------
c     calulate minimal and maximal frequencies: xmin,xmax 
c------------------------------------------------------------------
      xmin=x_1d_ar(1) 
      xmax=x_1d_ar(1)
      do j=1,Nx
         if (x_1d_ar(j).lt.xmin) xmin=x_1d_ar(j)
         if (x_1d_ar(j).gt.xmax) xmax=x_1d_ar(j)
      enddo
c------------------------------------------------------------------
c     calulate minimal and maximal frequencies: ymin,ymax 
c------------------------------------------------------------------
      ymin=y_2d_ar(1,1) 
      ymax=y_2d_ar(1,1)

      do i=1, Ny                    
         do j=1,Nx
            if (y_2d_ar(j,i).lt.ymin) ymin=y_2d_ar(j,i)
            if (y_2d_ar(j,i).gt.ymax) ymax=y_2d_ar(j,i)
         enddo
      enddo
cc120315
c      CALL PGPAGE
c
c      set viewport(normalized device coordinates 0<...<1)
c      CALL PGSVP(xleft,xright,ybot.ytop) all arguments are real
c       
c      CALL PGSVP(.2,.8,.4,0.8)
c
c      CALL PGSWIN(rbound(xmin),rbound(xmax),
c     +            rbound(ymin),rbound(ymax))
c
c      draw labeled frame around viewport
c      CALL PGBOX(XOPT, XTICK, NXSUB, YOPT,YTICK, NYSUB)
c      Character(*) XOPT, YOPT
c      REAL XTICK,YTICK
c      INTEGER  NXSUB, NYSUB
c      xopt(*) string of options for X axis of plot. 
c              Options are single letters
c      xtick   world coordinate interval between major marks on X axis
c              If xtick=0 the interval is choosen by BGBOX.
c              So that there will be at least 3 major tick marks along the axis
c      NXSUB   the number of subintervals to devide the major coordinate
c              interval into. IF xtick=0 or NXSUB=0 the number
c              is shoosen in PGBOX
c      xopt    A draw axisa 
c              B draw bottom (X) or left (Y) edge of frame
c              C draw top (X_ or right (Y) edge of frame
c              N write Numeric labes in the conventional location
c                bellow the viewport (X) or to the left of the viewport (Y)
c              S draw minor tick marks (subticks) 
c              T draw major tick marksat the major coordinates interval
c    
c      CALL PGBOX('BCNST', 0.0, 0, 'BCNST', 0.0, 0)

      CALL PGSLW(4)                  !set line width
cSAP120615
      CALL PGSCH(1.5)                !set charexter height
c      CALL PGSCH(1.0)                !set charexter height
cc      CALL PGSCH(2.1)
cSAP120615
c      CALL PGENV(xmin,xmax,ymin,ymax,0,1)
      CALL PGENV(xmin,xmax,ymin,ymax,0,0)

c        print *,'Past PGENV '
C*** Sets limit for plot.
c      xtitle='N trajetory length'
      xtitle='volume'
      xtitlelen=LEN_TRIM(xtitle)
c      titlestr=''
      titlestr=name
      titlelen=LEN_TRIM(titlestr)

c      CALL PGLAB(xtitle(1:xtitlelen),'X2 Y2'  
      CALL PGLAB(xtitle(1:xtitlelen),''
     &          ,titlestr(1:titlelen))
        
     
C***  note on screen, BLACK=0 means background color

      DO j=1,Nx
         x_plot_ar(j) = x_1d_ar(j)
      ENDDO

      DO i=1,Ny
           DO j=1,Nx
              y_plot_ar(j)= y_2d_ar(j,i)
           ENDDO

           select case(i)
             case (1)
C***  WHITE=1 means default foreground color   
              
               CALL PGSLS(1)
               CALL PGSCI(1)
               CALL PGSLW(6)
               CALL PGLINE(Nx,x_plot_ar,y_plot_ar)
             case (2)
               CALL PGSLS(1)
               CALL PGSLS(2)
               CALL PGSCI(BLUE) 
               CALL PGLINE(Nx,x_plot_ar,y_plot_ar)
             case (3)
               CALL PGSLS(3)
               CALL PGSCI(RED)
               CALL PGLINE(Nx,x_plot_ar,y_plot_ar)
             case (4)
               CALL PGSLS(4)
               CALL PGSCI(GREEN)              
               CALL PGLINE(Nx,x_plot_ar,y_plot_ar)
           end select  
         
      ENDDO

        
c        CALL PGSLS(4)
c        CALL PGSCI(GREEN)
c        newx(1)=raxis
c        newx(2)=raxis
c        newy(1)=ymin
c        newy(2)=ymax
c        CALL PGLINE(2,newx,newy)

C** Now add labels   :BEED TO ADJUST FOR .not.ARIES (BH)
c        CALL PGSCI(WHITE)
c        CALL PGTEXT(4.0,31.0,'f\dce\u')
c        CALL PGTEXT(3.8,70.0,'2 f\dce\u')
c        CALL PGTEXT(3.8,110.0,'3 f\dce\u')
c        CALL PGSCI(RED)
c        CALL PGTEXT(1.4,45.0,'f\dpe\u')
c        CALL PGSCI(BLUE)
c        CALL PGTEXT(2.0,165.0,'f\duh\u')
cc        CALL PGSCI(GREEN) 

        CALL PGSCI(1)
        CALL PGSLS(1)
cc        CALL PGSCI(BLACK)
c        CALL PGTEXT(4.75,10.0,'R\dm\u')


 570  format(1X,A,1pe10.3)  
      RILIN=5.
      RILIN=2.
      do i=1,n_param
        rilin=rilin+1
        write(*,*)'i,rilin',
     &             i,rilin
        write(*,*)'i,name_param(i)',
     &             i,name_param(i)
        write(*,*)'i,param(i)',
     &             i,param(i)
        
        write(text,570)name_param(i),param(i)

        write(*,*)'text',text

        CALL PGMTXT('B',RILIN,-0.2,0.0,text)    

      enddo

 580  format(1X,A)  

      rilin=rilin+1
      write(text,580)'numerical - black'
      CALL PGMTXT('B',RILIN,-0.2,0.0,text)    

      rilin=rilin+1
      write(text,580)'analytical - blue'
      CALL PGMTXT('B',RILIN,-0.2,0.0,text)    

c        call PGEND

c-------------------------------------------------------

      return
      end

      subroutine 
     &plot_num_anal_distribution_particle_number_mass_on_volume 
     &(v_x_0,v_y_0,Nx,Ny,rn_2d_numer,rn_2d_anal,v_x,v_y,time,
     & n_vol_plot,vol_plot_ar,particle_numb_ar,component_mass_ar)
c---------------------------------------------------------------------
c     Plot sectional distributions 
c     (from numerical n=rn_2d_numer and analytical n=rn_2d_anal)
c     versus volume 
c     (at special output volume mesh) at given time :
c
c     particle_numb_l=integral(d_vx[0-infinity])integral(d_vy[0-infinity]
c                       (eps_l(vx,vy)n(vx,vy),t)}
c
c     component_mass_l,1=integral(d_vx[0-infinity])integral(d_vy[0-infinity]
c                       (eps_l(vx,vy)n(vx,vy),t)vx}
c
c     component_mass_l,2=integral(d_vx[0-infinity])integral(d_vy[0-infinity]
c                       (eps_l(vx,vy)n(vx,vy),t)vy}

c     Output volume mesh  vol_plot_ar(1:n_vol_plot) 
c     was set inside this subtroutine.
c     The mesh is close to the article
c     G.Palaniswaamy, S.K. Loyalka, Direct simulation
c     Monte-Calo aerosol dynamics: coagulation and collisional sampling,
c     Nuclear Technology, Vol. 156, Oct. 2006, pp.20-38
c

      implicit none
      
c-----input 
      integer 
     &Nx,Ny,                 !dimensions of v_x,v_y arrays
     & n_vol_plot            !dimention of ouput Loyalka volme mesh
      real*8
     &v_x_0,v_y_0,            !parameters for initial concentartions 
     &rn_2d_numer(0:Nx,0:Ny), !array of n concentration at v_x,v_y mesh
     &rn_2d_anal(0:Nx,0:Ny),  !array of n concentration at v_x,v_y mesh
     &v_x(0:Nx),v_y(0:Ny),    !vx,vy mesh 
     &time, 
     &vol_plot_ar(0:n_vol_plot)  !Loyalka output volume mesh
c-----output
      real*8, dimension(1:n_vol_plot,1:2) :: component_mass_ar
      real*8, dimension(0:n_vol_plot) :: particle_numb_ar
c-----local           
      real*8 :: step_x,step_y
      integer i,j,l,k
      
      real*8, dimension(1:n_vol_plot) :: x_ar_l,y_ar_l

c-----for Drawing Markers:
      integer n_marker        ! number of points to be marked,
      parameter  (n_marker=1)   
      real  x_marker(n_marker),y_marker(n_marker)
      integer n_symbol_marker

c-----for two curves at one plot
      integer N_plots
      parameter (N_plots=2)

      integer  n_param                 !is the number of parameters
      parameter (n_param=1)    
      character*4 name_param(n_param) !are the names of parameters
      real param(n_param)                    !are the values of parameters

      real*8, dimension(1:n_vol_plot,1:N_plots) :: 
     &y_2d_particle_numb_ar,
     &y_2d_component_mass1_ar,
     &y_2d_component_mass2_ar


      step_x=v_x(2)-v_x(1)
      step_y=v_y(2)-v_y(1)

c      write(*,*)'step_x,step_y',step_x,step_y
 
      do l=0,n_vol_plot
         write(*,*)'l, vol_plot_ar(l)',l, vol_plot_ar(l)
      enddo


c-----analytical distribution rn_2d_anal
      do l=1,n_vol_plot 
         particle_numb_ar(l)=0.d0
         component_mass_ar(l,1)=0.d0 
         component_mass_ar(l,2)=0.d0
         do i=0,Nx
            do j=0,Ny
               if ((vol_plot_ar(l-1).lt.(v_x(i)+v_y(j))).and.
     &             (vol_plot_ar(l).ge.(v_x(i)+v_y(j)))) then

                    particle_numb_ar(l)=particle_numb_ar(l)+
     &                                  rn_2d_anal(i,j)
                    component_mass_ar(l,1)=component_mass_ar(l,1)+
     &                                     rn_2d_anal(i,j)*v_x(i)
                    component_mass_ar(l,2)=component_mass_ar(l,2)+
     &                                     rn_2d_anal(i,j)*v_y(j)
                endif
            enddo
          enddo

          particle_numb_ar(l) = particle_numb_ar(l)*step_x*step_y
          component_mass_ar(l,1)= component_mass_ar(l,1)*step_x*step_y
          component_mass_ar(l,2)= component_mass_ar(l,2)*step_x*step_y

          y_2d_particle_numb_ar(l,2)= particle_numb_ar(l)
          y_2d_component_mass1_ar(l,2)= component_mass_ar(l,1)
          y_2d_component_mass2_ar(l,2)= component_mass_ar(l,2)
      enddo
   
c-----numerical distribution rn_2d_numer
      do l=1,n_vol_plot 
         particle_numb_ar(l)=0.d0
         component_mass_ar(l,1)=0.d0 
         component_mass_ar(l,2)=0.d0
         do i=0,Nx
            do j=0,Ny
               if ((vol_plot_ar(l-1).lt.(v_x(i)+v_y(j))).and.
     &             (vol_plot_ar(l).ge.(v_x(i)+v_y(j)))) then

                    particle_numb_ar(l)=particle_numb_ar(l)+
     &                                  rn_2d_numer(i,j)
                    component_mass_ar(l,1)=component_mass_ar(l,1)+
     &                                     rn_2d_numer(i,j)*v_x(i)
                    component_mass_ar(l,2)=component_mass_ar(l,2)+
     &                                     rn_2d_numer(i,j)*v_y(j)
                endif
            enddo
          enddo

          particle_numb_ar(l) = particle_numb_ar(l)*step_x*step_y
          component_mass_ar(l,1)= component_mass_ar(l,1)*step_x*step_y
          component_mass_ar(l,2)= component_mass_ar(l,2)*step_x*step_y

          y_2d_particle_numb_ar(l,1)= particle_numb_ar(l)
          y_2d_component_mass1_ar(l,1)= component_mass_ar(l,1)
          y_2d_component_mass2_ar(l,1)= component_mass_ar(l,2)
      enddo

      name_param(1)='time'
      param(1)=time

      call plot_Ny_1d_curves_y_ji_xj
     &(n_vol_plot,N_plots,vol_plot_ar,y_2d_particle_numb_ar,
     &'particle_number',n_param,name_param,param)
     
      call plot_Ny_1d_curves_y_ji_xj
     &(n_vol_plot,N_plots,vol_plot_ar, y_2d_component_mass1_ar,
     &'component_mass1',n_param,name_param,param)

      call plot_Ny_1d_curves_y_ji_xj
     &(n_vol_plot,N_plots,vol_plot_ar, y_2d_component_mass2_ar,
     &'component_mass2',n_param,name_param,param)
      return
      end



      subroutine plot_input_data  
      use One_NmlMod

      implicit integer (i-n), real*8 (a-h,o-z)
     
      character*512 t_
  
      save
      REAL RILIN
      
      CALL PGPAGE
      write(t_,2001)
      RILIN=0.
      CALL PGMTXT('T', -RILIN,0.,0.,t_)      
      RILIN=RILIN+2.

      write(t_,13000) t_max
13000 format("t_max = ",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13001) tau
13001 format("tau = ",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13002) v_x_max
13002 format("v_x_max = ",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13003) v_y_max
13003 format("v_y_max = ",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13004) Nx
13004 format("Nx = ",i5)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13005) Ny
13005 format("Ny = ",i5)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13006) v_x_0
13006 format("v_x_0=",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13007) v_y_0
13007 format("v_y_0=",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

      write(t_,13008) rK_0
13008 format("rK_0 =",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)


      write(t_,13009) rN_0
13009 format("rN_0 =",e12.3)
      RILIN=RILIN+1.
      CALL PGMTXT('T',-RILIN,0.,0.,t_)

c      write(*,100) line 
c100  format(a100)
c      CALL PGTEXT(0.0,0,0,text)

 2001 format("INPUT data FROM coagulation.dat file")
 
      return
      end

      subroutine create_vol_plot_aray
c-------------------------------------------------------------------
c     Calulates  Output volume mesh  vol_plot_ar(1:n_vol_plot) 
c     was set inside this subtroutine.
c     The mesh is close to the article
c     G.Palaniswaamy, S.K. Loyalka, Direct simulation
c     Monte-Calo aerosol dynamics: coagulation and collisional sampling,
c     Nuclear Technology, Vol. 156, Oct. 2006, pp.20-38
c
c     n_vol_plot=15
c-----------------------------------------------------------------------  
      use  One_NmlMod 
      use One_NoNmlMod

      implicit none

c-----input from in One_NmlMod:
c     integer n_vol_plot !number of mesh points
c-----output will be in One_NoNmlMod:
c     real*8  vol_plot_ar(0:n_vol_plot)

c-----locals
      integer l
      real*8 q,p

      vol_plot_ar(0)=0.d0

      write(*,*)'mk_graph.f in create_vol_plot_aray n_vol_plot',
     &                                              n_vol_plot

      vol_plot_ar(1)=(v_x_0+v_y_0)/8.d0
      p=1.d0/(n_vol_plot-1)
      q=((v_x_max+v_y_max)/vol_plot_ar(1))**p
      write(*,*)'mk_graph.f in create_vol_plot_aray 1 q=',q
      q=1.3d0
      write(*,*)'mk_graph.f in create_vol_plot_aray 2 q=',q
      do l=2,n_vol_plot
         vol_plot_ar(l)=q*vol_plot_ar(l-1)
c         vol_plot_ar(l)=1.3d0*vol_plot_ar(l-1)
      enddo

      do l=0,n_vol_plot
         write(*,*)'l, vol_plot_ar(l)',l, vol_plot_ar(l)
      enddo
      return
      end

      subroutine plot_diference_numer_analyt_solutions
      use One_NmlMod
      use One_NoNmlMod
      implicit none

c-----locals
      integer i
      real*8, dimension(1:n_timesteps) ::   x_ar_l, y_ar_l

      integer n_param                    !number of parameters at figure
      character*(20) name_param(1)       !names  of parameters at figure
      real param(1)                      !values of parameters at figure 
    
c-----for Drawing Markers:
      integer n_marker        ! number of points to be marked,
      parameter  (n_marker=1)   
      real  x_marker(n_marker),y_marker(n_marker)
      integer n_symbol_marker

      name_param(1)='tau'
      param(1)=tau

      do i=1,n_timesteps
         x_ar_l(i)=i
      enddo

c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_timesteps/2)
      y_marker(1)=y_ar_l(n_timesteps/2)
c----------------------------------------------------------------
c     difnorm_ar
c-----------------------------------------------------------------
      do i=1,n_timesteps
         y_ar_l(i)=difnorm_ar(i)
      enddo

c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_timesteps/2)
      y_marker(1)=y_ar_l(n_timesteps/2)

      call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_timesteps,2,
     & n_marker,x_marker,y_marker,n_symbol_marker,    
     &'linlin',0.d0,0.d0,
     &'difnorm',
     &'time steps','difnorm',
     &n_param,name_param,param)
c----------------------------------------------------------------
c     difc_ar
c-----------------------------------------------------------------
      do i=1,n_timesteps
         y_ar_l(i)=difc_ar(i)
      enddo

c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_timesteps/2)
      y_marker(1)=y_ar_l(n_timesteps/2)

      call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_timesteps,2,
     & n_marker,x_marker,y_marker,n_symbol_marker,    
     &'linlin',0.d0,0.d0,
     &'difc',
     &'time steps','difnorm',
     &n_param,name_param,param)


c----------------------------------------------------------------
c     difrc_ar
c-----------------------------------------------------------------
      do i=1,n_timesteps
         y_ar_l(i)=difrc_ar(i)
      enddo

c-----with marker
      n_symbol_marker=9
      x_marker(1)=x_ar_l(n_timesteps/2)
      y_marker(1)=y_ar_l(n_timesteps/2)

      call plot1dt_marker_param(x_ar_l,y_ar_l,0,0,n_timesteps,2,
     & n_marker,x_marker,y_marker,n_symbol_marker,    
     &'linlin',0.d0,0.d0,
     &'difrc',
     &'time steps','difnorm',
     &n_param,name_param,param)

      return
      end


      subroutine write_density_at_time(density_time,n_plots)
c-----writes data
c     density_time(1:n_plots,1:4)
c     into ASCI output file: 
c     'density_time'.dat
c---------------------------------------------
      implicit none
     
c-----input
      integer :: n_plots
      real*8, dimension(0:N_plots,1:4) :: density_time  

c-----locas
      character*9 text 

      character outdat*20
      integer i,i_unit,j

c      write(*,*)'in write_data_at_time n_plots=',n_plots

c      do i=0,n_plots
c         write(*,*)'i,time,density_MC,density_analitical',
c     &   j,density_time(i,2),density_time(i,3),
c     &   density_time(i,4)
c      enddo

c      write(text,1)time
c1     format(1pe9.3)
c      write(*,*)'text=',text
      
c      outdat=text//'_time.dat'
c      write(*,*)' outdat=', outdat

      outdat='density_time.dat'
      i_unit=1
      open (unit=i_unit,file=outdat)

      do i=0,n_plots
         j=density_time(i,1)
         write(i_unit,3)
     &   j,density_time(i,2),density_time(i,3),
     &   density_time(i,4)
      enddo

2     format (2(1pe16.6))
3     format (1(i4),3(1pe16.6))

      close(i_unit)
      return
      end
      

