#include "../../libtt/tensor.h"
#include "../../libtt/tensor_train.h"
class StartCondition: public TTensor
{
	public:
		static double *weights;
		static double *knots;
		double operator[](int *);
		StartCondition (const int &, int* &);
		StartCondition();
};

class FirstIntegral: public TTensor
{
//class for discretization of integrand in the first integral operator
	public:
		static double *weights;
		static double *knots;
		static int* average_ranks;
		static int number_of_computations;
		TTensorTrain *to_integrate;
		int *point;
		double operator[](int *);
		FirstIntegral (const int &, int* &);
		FirstIntegral();
		void change_point(int *);
		void set_to_integrate(TTensorTrain *);
		//void ranks_counter();
};

class SecondIntegral: public TTensor
{
//class for discretization of integrand in the second integral operator
	public:
		static double *weights;	
		static double *knots;
		static int* average_ranks;
		static int number_of_computations;
		int *point;
		TTensorTrain *to_integrate;
		double operator[](int*);
		SecondIntegral(const int &, int * &);
		SecondIntegral();
		void change_point(int*);
		void set_to_integrate(TTensorTrain *);
		//void ranks_counter();
};

class ForVolumeCheck: public TTensor
{
// the result of tt_n.dot(for_volume_check) is in pow (1/mesh_step, dimensionality) times bigger than real result
// thus I multiply it by pow(mesh_step, dimensionality) in main.cpp-file.And Only for 2-dimensional equation
	public:
		static double *knots;
		ForVolumeCheck();
		ForVolumeCheck(const int & , int * &);
		double operator[](int*);
};

class Weights: public TTensorTrain
{
	protected:
		TTensorTrainParameters parameters;

	public:
		void change_point(int*);
		Weights();
		Weights(const TTensorTrain &, const double &);
		Weights(const TTensorTrain &tens, const double *c);
		void set_parameters(double tolerance, int iterations_number);

};

class Steps: public TTensor
{
	protected:
		TTensorTrainParameters parameters;

	public:
		static long int number_of_computations;
		static double *weights;
		static double *knots;
		static double mesh_step;
		static double step_time;

		TTensorTrain *previous, *half_step;
		char is_half_step;
		double operator[](int *);
		Steps (const int &, int* &);
		Steps();
		//void change_point(int *);
		void set_trains(TTensorTrain *, TTensorTrain *,char is_half_step);
		void set_parameters(double tolerance, int iterations_number);
};


class TDiscreteAnalitycal:public TTensor 
{
	
	public:
		TDiscreteAnalitycal();
		TDiscreteAnalitycal(const int &, int* &);

		static double *knots;
		double t;
		double operator [](int *);

};

class TTrapecialWeights:public TTensorTrain
{
      public:
	TTrapecialWeights(int d, const int *m, double vol):TTensorTrain()
	{
		dimensionality = d;
                carriages = (double **) malloc(dimensionality * sizeof(double*));
                modes_sizes = (int *) malloc(dimensionality * sizeof(int));
                ranks = (int *) malloc((dimensionality + 1) * sizeof(int));
                ranks[0] = 1;
		for (int i = 0; i < dimensionality; i++)
                {
                    ranks[i + 1] = 1;
                    modes_sizes[i] = m[i] + 1;
                    carriages[i] = (double *) malloc (modes_sizes[i] * sizeof(double));
		    
/*		    carriages[i][0]= vol/3.0;
		    
		    for (int j=1;j<modes_sizes[i]-1;j++)
		    {//Simpson's weights
			if (j%2==1)
			    carriages[i][j]=4.0*vol/3.0;
   			else
			    carriages[i][j]=2.0*vol/3.0;
		    }
		    carriages[i][modes_sizes[i]-1]=vol/3.0;
*/                  
		    carriages[i][0] = vol/ 2;
		    for (int j = 1; j < modes_sizes[i] - 1; j++)
                    {//trapecial weights
                        carriages[i][j] = vol;
                    }
		    carriages[i][modes_sizes[i] - 1] = vol/ 2;
		    
/*		    for (int j=0; j < modes_sizes[i];j++)
			{//central parallelepipeds weights
				if (j%2==0)
					carriages[i][j]=2*vol;
				else
					carriages[i][j]=0;
			}*/	    		    
                }
	};
};
