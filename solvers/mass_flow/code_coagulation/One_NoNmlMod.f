      MODULE One_NoNmlMod
c---------------------------------------------------------------
c     Contains description of global data 
c     which are not given in any namesits 
c-------------------------------------------------------------- 
      real*8 
     &step_x,step_y                    !steps of volume mesh

      integer n_timesteps               !number of time steps

c      real*8, pointer ::
c     &v_x(:),                                  !volume mesh(0:Nx)
c     &v_y(:),                                  !volume mesh(0:Ny)
                                               !particle concentation(0:Nx,0:Ny)
c     &rn_2d_t(:,:),                            !at the next (top) time step
c     &rn_2d_b(:,:),                            !at the privios (bottom) time step 
c     &rn_2d_c(:,:),                            !at the middlle (center) time step
c     &rn_2d_analyt(:,:),                       !partcile concentatio analytica
                                               !for test
c---------------------------------------------------------------------------
c     for sectional output distributions
c--------------------------------------------------------------------- 
c     &vol_plot_ar(:),             !Loyalka output volume mesh(0:n_vol_plot
c     &component_mass_ar(:,:),     !(1:n_vol_plot,1:2) 
c     &particle_numb_ar(:),         !(1:n_vol_plot)
c-----------------------------------------------------------------------
c     for difference between nymerical and analytical n
c-----------------------------------------------------------------------
c     &difnorm_ar(:),   !norm of difference between numerical and
                       !analytical solutions                 (1: n_timesteps)
c     &difc_ar(:),      !error in C norm (maximal difference) (1: n_timesteps) 
c     &difrc_ar(:)      !relative error in norm C             (1: n_timesteps)


      real*8, allocatable, dimension(:) ::
     &v_x,                                  !volume mesh(0:Nx)
     &v_y                                  !volume mesh(0:Ny)

      real*8, allocatable, dimension(:,:) ::
     &rn_2d_t,                            !!particle concentation(0:Nx,0:Ny at the next (top) time step
     &rn_2d_b,                            !at the privios (bottom) time step 
     &rn_2d_c,                            !at the middlle (center) time step
     &rn_2d_wx,                           !array od distribution created from w_x
     &rn_2d_analyt,                       !partcile concentatio analytica for test
     &component_mass_ar                   !(1:n_vol_plot,1:2) 
c---------------------------------------------------------------------------
c     for sectional output distributions
c--------------------------------------------------------------------- 
      real*8, allocatable, dimension(:) ::
     &vol_plot_ar,             !Loyalka output volume mesh(0:n_vol_plot 
     &particle_numb_ar,         !(1:n_vol_plot)
c-----------------------------------------------------------------------
c     for difference between nymerical and analytical n
c-----------------------------------------------------------------------
     &difnorm_ar,   !norm of difference between numerical and
                    !analytical solutions                 (1: n_timesteps)
     &difc_ar,      !error in C norm (maximal difference) (1: n_timesteps) 
     &difrc_ar      !relative error in norm C             (1: n_timesteps)


c-------------------------------------------------------------------------------
c     for Monte Carlo
c--------------------------------------------------------------------------
      real*8, allocatable, dimension(:) :: 
     &v0_vector,    !(1:N_species) parameters of initial
                    ! distributions normalized to v_ch
     &v_particle_max_vector, !minimal and maximal volumes normalized to v_ch
     &v_particle_min_vector

      real*8, allocatable, dimension(:,:) :: 
     &y_ar                 !volumes of particle (1:N_species,1:N_particle)

      real*8, allocatable, dimension(:,:) ::
     &fn_2d_mesh,fn_2d_analyt, !2d distribution of n at volume mesh (1:Nx,1:Ny)
     &fq_2d_mesh,fq_2d_analyt  !2d distribution of q at volume mesh (1:Nx.1:Ny)

      real*8, allocatable, dimension(:) ::      
     &v_c_x,         ! 1st species volume mesh (1:Nx) for plotting
                     ! in 2d case 
     &v_c_y          ! 2nd species volume mesh (1:Ny) for plotting
                     ! in 2d case 
c      real*8, allocatable, dimension(:,:) :: dk_coag_ar     

      integer :: n_plots                       ! t_max/h_plot

      real*8, allocatable, dimension(:,:)   :: density_time ! to write density at output times (1:n_plots,1:4)        

      END MODULE One_NoNmlMod
  
  
