#include"./anderson_lib.h"

void print_matrix(char *desc, double *a, int m, int n) 
{
    printf( "\n %s\n", desc );
    for (int i = 0; i < m; i++) 
    {
	for (int j = 0; j < n; j++) 
        {
	    printf( " %.5f", a[n * i + j] );
	}
	printf( "\n" );
    }
}

void init(double *u, int n, double u0) 
    {
    for (int i = 0; i < n; i++) 
    {
        u[i] = u0;
    }
}

double calculate_eps(double *u, double *g, int n) 
{
    double eps = 0;
    double *tmp = (double*) malloc (n * sizeof(double));
    cblas_dcopy(n, g, 1, tmp, 1);
    cblas_daxpy(n, -1, u, 1, tmp, 1);
    eps = cblas_dnrm2(n, tmp, 1);
    free(tmp);
    return eps;
}

void swap_vec(double **a, int m) 
{
    double *tmp = a[m];
    for (int i = m; i > 0; i--) 
    {
        a[i] = a[i - 1];
    }
    a[0] = tmp;
    tmp = NULL;
}

void print_vec(char *desc, double *a, int m) 
{
    printf("\n %s\n", desc);
    for (int i = 0; i < m; i++) 
    {
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;
}

void free_vec(double **a, int m)
{
    for (int i = 0; i < m; i++) 
    {
        free(a[i]);
    }
    free(a);
}

void init_a(double *a, int n, int m, double **u, double** g) 
{
    for (int j = 0; j < m; j++) 
    {
	for (int i = 0; i < n; i++) 
        {
	    a[j * n + i] = ((g[j][i] - u[j + 1][i]) -
			    (g[j + 1][i] - u[j+ 2][i]));
	}
    }
}

void init_b(double *b, int n, double** u, double** g) 
{
    for (int i = 0; i < n; i++) 
    {
	b[i] = (g[0][i] - u[1][i]);
    }
}

double find_rank(double *mat_a, double *mat_b, int n, int m, double *x, double& cond, double cond_param)
{
    // input
    int nrhs = 1, lda = n;
    int ldb = std::max(n, m);
    double rcond = cond_param;
    double* work;
    int lwork;
    double *a, *b;
    int info, rank = m + 1;
    double *s;

    bool find = false;
    while (!find)
    {
	ldb = std::max(n, m);
	lwork = 4 * (n + m);
	work = (double*) malloc(lwork * sizeof(double));

	a = (double*) malloc(n * m * sizeof(double));
	for (int j = 0; j < m; j++) 
        {
	    for (int i = 0; i < n; i++) 
            {
		a[j * n + i] = mat_a[j * n + i];
	    }
	}
	b = (double*) malloc(ldb * sizeof(double));
	for (int i = 0; i < ldb; i++) 
        {
	    b[i] = mat_b[i];
	}
	s = (double*) malloc(std::max(n, m) * sizeof(double));

	dgelss(&n, &m, &nrhs, a, &lda, b, &ldb, s, &rcond, &rank,
	       work, &lwork, &info);
	cond = s[0] / s[std::min(m, n) - 1];

	if (rank < m) 
        {
	    std::cout << "cond(A) too big " <<  cond << ", reducing mAA to ";
	    std::cout << m - 1 << std::endl;
	    m--;
	    find = false;
	}
        else 
        {
	    find = true;
	}
	for (int i = 0; i < m; i++) 
        {
	    x[i] = b[i];
	}

	free(work);
	free(a);
	free(b);
	free(s);
    }

    return rank;
}


double calculate_next(double **u, double **g, int n,
		      int mmax, int iter, double& cond, double cond_param, int& mcur) 
{
    if (mcur < mmax) 
    {
	mcur++;
    }
    int m = std::min(mcur, iter);
    //    std::cout << "niter = " << iter << " mAnderson = " << mcur << std::endl;
    int ldb = std::max(n, m);

    double *a, *b, *x;
    a = (double*) malloc(n * m * sizeof(double));
    init_a(a, n, m, u, g);
    b = (double*) malloc(ldb * sizeof(double));
    init_b(b, n, u, g);
    x = (double*) malloc(m * sizeof(double));

    double cond_num;
    int rank = find_rank(a, b, n, m, x, cond_num, cond_param);
    cond = cond_num;
    if (iter == 0) 
    {
	cond = 0;
    }
    m = rank;
    mcur = m;

    for (int i = 0; i < m; i++) 
    {
	b[i] = x[i];
    }
    int m1 = m;
    free(x);
    double *delta;
    delta = (double *) malloc (m1 * sizeof(double));

    for( int i = 0; i < m1; i++ ) 
    {
	    delta[i] = b[i];
    }

    for (int i = 0; i < n; i++) 
    {
	double sum = 0;
	for (int j = 0; j < m1; j++) 
        {
	    sum += delta[j] * (g[j][i] - g[j + 1][i]);
	}
	u[0][i] = g[0][i] - sum;
    }

    double *alpha;
    alpha = (double *) malloc( (m1 + 1) * sizeof(double));
    alpha[0] = 1 - delta[0];
    for (int i = 1; i < m1; i++) 
    {
        alpha[i] = delta[i - 1] - delta[i];
    }
    if (m1 > 0) 
    {
        alpha[m1] = delta[m1 - 1];
    }

    double norm = 0;
    for (int i = 0; i < m1 + 1; i++)
    {
        norm += std::abs(alpha[i]);
    }
    free(a);
    free(b);
    free(delta);
    free(alpha);
    return norm;
}
/////////////////////////////////////////////////////////////////////////////////////////////
double *anderson_acceleration(int n, int max_iter, int mmax, double eps, double cond_param, double *init_vals, void (calculate_g(double *, double *, int)), double *final_residual_norm, double *runtime)
{
// 
// int n -- n equations
// max_iter -- maximal number of iterations
// mmax -- maximal number of vectors for mixing
// eps  -- residual norm stopping criteria
// cond_param -- max condition number for linear least squares problem (if exceeds, history becomes shorter)
// init_vals -- vector with n initial values for iterations
// calculate_g -- nonlinear operator for fixed-point problem; G(x_in, x_out, n); result is in x_out
// final_residual_norm -- resulting euclidian residual norm
// runtime -- runtime in seconds
    int mcur = -1;
    double **u;
    u = (double **) malloc ((mmax + 2) * sizeof(double *));
    for (int i = 0; i < mmax + 2; i++) 
    {
        u[i] = (double *) malloc(n * sizeof(double));
        init(u[i], n, 0);
    }

    for (int i = 0; i < n; i++)
	u[1][i] = init_vals[i];

    double **g;
    g = (double **) malloc ((mmax + 1) * sizeof(double *));
    for (int i = 0; i < mmax + 1; i++) 
    {
	g[i] = (double *) malloc(n * sizeof(double));
	init(g[i], n, 0);
    }

    int iter = 1;
    double res_norm = 1;
    double alpha_norm = 0;
    double matrix_cond = 0;

    struct timeval start, end;
    double r_time = 0.0;
    gettimeofday(&start, NULL);
       
    while (iter <= max_iter && res_norm > eps) 
    {
	calculate_g(u[1], g[0], n);
	alpha_norm = calculate_next(u, g, n, mmax, iter, matrix_cond, cond_param, mcur);

	res_norm = calculate_eps(u[1], g[0], n);
	swap_vec(u, mmax + 1);
	swap_vec(g, mmax);
	printf("iter = %d res = %E coef = %.3e cond = %.3e\n",
	       iter, res_norm, alpha_norm, matrix_cond);
	iter++;
    }
    gettimeofday(&end, NULL);
    
    final_residual_norm[0] = res_norm;
    r_time = end.tv_sec - start.tv_sec +
	((double) (end.tv_usec - start.tv_usec)) / 1000000;
    runtime[0] = r_time;

    double *result = (double *) malloc(n * sizeof(double));
    for (int i = 0; i < n; i++)
        result[i] = u[0][i];
    
    
    free_vec(u, mmax + 2);
    free_vec(g, mmax + 1);
    return result;
}
