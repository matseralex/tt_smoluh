d = 11%% 2**s
tol = 1e-6
timestep = 1e-4
ranksfile = 'ranks2048.dat'
fid_ranks = fopen(ranksfile, 'wt')

for i = 1:2000
    file = sprintf('TEST2048pics/sol_%04d.dat', i);
    fid = fopen(file, 'r');
    A = fscanf(fid, '%lf %lf\n');
    A = A(2:2:end) ;
    A = reshape(A, 2 * ones(d,1));
    TT = TTSVD(A, tol, 0);
    for j =1:(d-1)
        r(j) = size(TT{j}, 3);
    end;
    t = (i-1) * 1000 * timestep;
    
    %disp(usefuldata)
    
    fprintf(fid_ranks, "%g\t", t)
    fprintf(fid_ranks, "%d ", max(r))
    for j = 1:(d-1)
        fprintf(fid_ranks, "%d ", r(j))
    end
    fprintf(fid_ranks, "\n")
    %fprintf(fid_ranks, "%d \n ", r)
    fclose(fid);
    B = TT2Full(TT);
    %size(B)
    %2**d
    B = reshape(B, 2**d, 1);
    A = reshape(A, 2**d, 1);
    printf("%g ", t);
    printf(" TTrelerr %g\n ", norm(A - B) )
end;
fclose(fid_ranks)
