using namespace std;

#include <iostream>
#include <cstdio>
#include <string.h>
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <mkl.h>
#include <omp.h>
#include <ctime>
#include "../../tensor_train.h"

#define alpha 0.75l
#define beta -0.75l
double K(double v, double u)
{
//    return pow( (pow(u, 1.0/3.0) + pow(v, 1.0/3.0)), 2.0l) * sqrt(1.0 / u + 1.0 / v);
    return pow(u, alpha) * pow(v, beta) + pow(u, beta) * pow(v, alpha);
//    return 1.0;
}

class Kernel: public TTensor
{
    public:

        double operator[](const int *);
        Kernel(const int &d , int *);
        Kernel();
        void set_h(double );
        double get_h();
    private:
        double h;
};

Kernel::Kernel(const int &d, int *m): TTensor(d, m)
{
}

Kernel::Kernel(): TTensor()
{
}
double Kernel::get_h()
{
    return h;
}

void Kernel::set_h(double _h)
{
    h=_h;
}

double Kernel::operator[](const int *p)
{
    int d = this->get_dimensionality();
    //cout << d;
    double u = 5e-1, v = 5e-1;
    for (int i = 0; i < d /2; i++)
    {
        u += p[i];
        v += p[d / 2 + i];
    }
    u *= h;
    v *= h;
//    cout << " u =" << u << " v =  " << v << "K(u, v) = " << K(u,v) << endl;
    return K(u, v);
}

int main(int argc, char** argv)
{
    int d = 4;
    int N = 1000;
    double h = 1e-2;
    double tolerance = 1e-6;

    if(argc < 4)
    {
        printf("Args: d N h\n");
        exit(1);
    }

    sscanf(argv[1], "%d", &d);
    sscanf(argv[2], "%d", &N);
    sscanf(argv[3], "%lf", &h);
    int *m = new int[d];
    //printf("finished scanf\n");

    for (int i = 0; i<d; i++)
        m[i] = N;

    Kernel kernel(d, m);
    kernel.set_h(h);
    
    TTensorTrain tt_kernel;

    TTensorTrainParameters parameters;
    parameters.tolerance = tolerance;
    parameters.maximal_iterations_number = 0;

    tt_kernel.Approximate(&kernel, parameters);
//    tt_kernel.SVDCompress(tolerance);
    for (int i = 0; i <= d; i++)
        printf("rank %d = %d\n ", i, tt_kernel.get_rank(i));
    
    delete[] m;
    return 0;
}
