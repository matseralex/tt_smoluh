#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<ctime>
#include<complex>

#include"../include/gsmol_cp_blas.h"
#include"../include/gsmol_cp_utility.h"

//============================================================================//
//    Main                                                                    //
//============================================================================//
int main(
    int argc,
    char ** argv
) {
    if (argc < 8)
    {
        printf(
            "================================================================================\n"
        );

        printf(
            "Required arguments:\n (1) kernel_name\n (2) collisions_type\n (3) dimensionality\n (4) mode_size\n"
        );

        printf(
            " (5) number_iterations\n (6) dt\n (7) path_to_numerical\nOptional arguments:\n (8) filename_analytical\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: kernel_name == {const, sumsqrt, genprod}\n"
        );

        printf(
            "Tips: kernel_name := const, otherwise\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: collisions_type == all   --   all collisions till dimensionality\n"
        );

        printf(
            "Tips: collisions_type != all   --   only dimensionality-ary collisions\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: path_to_numerical == <path>/\n"
        );

        printf(
            "================================================================================\n"
        );

        printf(
            "Tips: 'filename_analytical' file to be generated if argument (8) is present\n"
        );

        printf(
            "================================================================================\n"
        );

        return 1;
    }

    int D;
    int N;
    double dt;
    int T;

    // argv parsing
    sscanf(argv[3], "%d", &D);
    sscanf(argv[4], "%d", &N);
    sscanf(argv[5], "%d", &T);
    sscanf(argv[6], "%lf", &dt);

    if (D < 2 || N < 1 || T < 1)
        return 1;

    int tmp;
    int time = (int)rint(dt * T);
    char filename[256];

    sprintf(
        filename, "%ss_%s_%d-%s_%d_%d_%s_%d",
        argv[7], argv[1], D,
        (strcmp(argv[2], "all"))? "only": "all",
        N, time, argv[6], T
    );

    // numerical solution
    double * numsol = (double *)malloc(N * sizeof(double));
    double mnum = 0.; 

    //====================================================//
    //    Read numerical solution & compute its moment    //
    //====================================================//
    FILE * fres = fopen(filename, "r");

    for (int i = 0; i < N; ++i)
    {
        fscanf(fres, "%d %lf", &tmp, numsol + i);
        mnum += numsol[i];
    }

    fclose(fres);

    //====================================================//
    //    Analytical solution & its moment computation    //
    //====================================================//
    double * ansol = (double *)calloc(N, sizeof(double));
    double man = trivial_compute(time, D, N, ansol);

    //===========================================//
    //    Write analytical solution > argv[8]    //
    //===========================================//
    if (argc > 8)
    {
        fres = fopen(argv[8], "w");

        for (int i = 0; i < N; ++i)
            fprintf(fres, "%d %.15f\n", i, ansol[i]);

        fclose(fres);
    }

    //==================//
    //    Comparison    //
    //==================//
    printf("Relative difference = %.14f\n", (man - mnum) / man);
    printf("Euclid metrics = %.14f\n", euclid_metrics(N, ansol, numsol));

    //=========================//
    //    Deallocate memory    //
    //=========================//
    free(ansol);
    free(numsol);

    return 0;
}
 
