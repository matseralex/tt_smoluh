#include <stdio.h>
#include "parallel_cross.h"
#include <gsl/gsl_sf_bessel.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include <sys/time.h>
#include <omp.h>
using namespace std;

double K(const int & u, const int &v)
{
//	return 1.0;
//	return u + v + 1.0;
	double u1 = (double)u + 1.0;
	double v1 = (double)v + 1.0;
//	return 2 * (pow (u1*v1, 1.0l/3.0) + 1.0);

//	return u * v;
//	return (pow(u1 * v1 , 1.0l/12.0));
//	double u1=u * h;
//	double v1=v * h;
//	return pow(u1, 0.75l) * pow(v1, -0.75l) + pow (u1, -0.75l) * pow (v1, 0.75l);
//	return pow(u1, 0.95l) * pow(v1, -0.95l) + pow (u1, -0.95l) * pow (v1, 0.95l);
//	
        return pow(pow(u1, 1.0 / 3.0) + pow(v1, 1.0 / 3.0), 2.0) * sqrt((1.0 / u1 + 1.0 / v1));
//    return 1.0;
}

class TKernel: public TMatrix {

        public:
		int kernel_type ;
                TKernel (const int &, const int &);
                double value (const int & , const int &);

};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{
        
}

double TKernel::value(const int &i, const int &j)
{
	if (this -> kernel_type == 1)
	        return  K(i , j);
	if (this -> kernel_type == 2)
		return K(i + 1, j + 1);
	else
		return K(i, j);
}

int main(int argc, char ** argv)
{
	//printf("You are ridiculous\n");
	int  N;
	int p, np;
	double start, end;
	if (argc < 2)
	{
		printf("Need option: N\n");
		return 1;
	}
	sscanf(argv[1], "%d", &N);
	
	MPI_Init(&argc, &argv);
	
        TCross_Parallel_v1_Parameters parameters;

        parameters.tolerance=1e-6;
        parameters.maximal_iterations_number = 0;

       	TKernel kernel(N, N);
	kernel.kernel_type = 1;
        
        TCross_Parallel_v1 crossed_kernel;


        crossed_kernel.Approximate(&kernel, parameters);
	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	if ( p == 0)
		printf("approximated successfully\n");

	double *x = (double *) malloc( (N / np) * sizeof(double));
	double *result;
	for (int i = 0; i < N / np; i++)
		x[i] = 1.0;


	
	start = MPI_Wtime();
	for ( int i = 0; i < 100; i++)
	{
		result = crossed_kernel.matvec(x);
		free(result);
		MPI_Barrier(MPI_COMM_WORLD);
	}

	result = crossed_kernel.matvec(x);
	end = MPI_Wtime();
	
	if (p == 0)
	{
		
		printf("rank = %d , np = %d\n", crossed_kernel.get_rank(), np);
		printf("100 matvec time = %lf\n", end - start);
		printf("chunksize = %d\n",  N / np);
		//for (int i = 0; i < N / np; i ++)
		//	printf("result[%d] = %lf\n", i, result[i]);

	}	
		
	free(result);
	start = MPI_Wtime();
	result = crossed_kernel.smol_conv_discrete(x);
	MPI_Barrier(MPI_COMM_WORLD);
	end = MPI_Wtime();
	if (p == 1)
	{
		printf("rank = %d , np = %d\n", crossed_kernel.get_rank(), np);
		printf(" smol conv time = %lf\n", end - start);
		//printf("chunksize = %d\n",  N / np);
		//for (int i = 0; i < N / np; i ++)
		//	printf("result[%d] = %lf\n", i + N / np, result[i]);

	}
	
	//if (p == 0)
	//	for (int j = 0; j < N / np; j++)
	//		printf("res[%d] = %lf\n", j + p * N/np, result[j]);

	//free(result);

	MPI_Finalize();
}
