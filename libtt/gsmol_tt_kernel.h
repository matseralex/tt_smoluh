#ifndef GSMOL_TT_KERNEL_H
#define GSMOL_TT_KERNEL_H

#include <mkl/mkl.h>
#include <mkl/mkl_lapacke.h>
#include "tensor_train.h"

//============================================================================//
//    Class for Generalized Smoluchowski operator                             //
//============================================================================//
class TKernel_TT
{

    private:

        // kernels maximal dimension
        int D;

        // quantity of kernels
        int A;

        // kernels mode size
        int N;
        
        // fourier size
        int size;

        // maximal rank
        int R;

        // multipliers in front of kernels
        // factorial coefficient * weight
        double * mult;

        // kernels offsets 
        int * koffs;

        // carriage offsets
        int ** coffs;

        // factor loads
        int ** loads;

        // total kernel size
        int K;

        // kernels in TT-format
        TTensorTrain ** kers_tt;

        // matrix composed of TT-kernels 
        double * kdata;

        // quantity of OMP threads
        int H; 

        // OMP parallel kernel bounds
        int * kbounds;

        // OMP parallel mode bounds
        int * mbounds;

        // DFTI descriptor forward handlers
        DFTI_DESCRIPTOR_HANDLE * fdescs;

        // DFTI descriptor backward handlers
        DFTI_DESCRIPTOR_HANDLE bdesc;

        // auxiliary memory capacity
        int X;

        // auxiliary memory
        double * aux;

        // fourier image
        complex<double> * faux;

        // ranks of kernels after approximation
        int * ranks_appr;

        // evaluated time of approximation
        double t_appr;

        // evaluated time of compression
        double t_compress;

        //============================================//
        //    Income part of Smoluchowski operator    //
        //============================================//
        void gconv(
            // in:
            const double * arg,
            // out: income part of S(arg)
            double * res
        );

        //=============================================//
        //    Outcome part of Smoluchowski operator    //
        //=============================================//
        void gmatvec(
            // in:
            const double * arg,
            // out: outcome part of S(arg)
            double * res
        );

    public:

        TKernel_TT();

        TKernel_TT(
            const char * kname,
            // in: type of collisions
            const char * ktype,
            // in: kernels maximal dimension
            const int & kdim,
            // in: quantity of equations
            const int & kmode,
            // in: weights in front of kernels
            const double * kw,
            // in: tolerance of approximation
            const double & ktol
        );

        virtual ~TKernel_TT();

        // return: evaluated time of approximation
        double get_t_appr(
            void        
        ) { return t_appr; }

        // return: evaluated time of compression
        double get_t_compress(
            void        
        ) { return t_compress; }

        // return: d-th rank of last kernel before compression
        int get_rank_appr(
            // in: index of kernel
            int & d
        ) { return ranks_appr[d]; }

        // return: d-th rank of last kernel 
        int get_rank(
            // in: index of kernel
            int & d
        ) { return (kers_tt[A - 1])->get_rank(d); }

        //=============================//
        //    Smoluchowski operator    //
        //=============================//
        void compute(
            // in:
            const double * arg,
            // out: S(arg)
            double * res
        );

};

#endif // GSMOL_TT_KERNEL_H

