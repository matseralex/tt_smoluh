#ifndef JACOBI_H
#define JACOBI_H

#include "../../libtt/parallel_cross_omp.h"
#include "jacobi/preconditioner.hpp"
#include <cassert>
#include <cstring>
#include <iostream>
#include <mkl.h>
#include <mkl_rci.h>
#include <vector>

template <typename K>
void apply_jacobi(int N, K kernel, TCross_Parallel_v1 &kernel_cross,
                  double lambda, const double *n, double *in, double *out) {
  std::vector<double> first_row(N - 1);

  std::vector<double> indexed_n(N);
  double *indexed_n_p = indexed_n.data();
  indexed_n.front() = 0.0;
  for (int k = 1; k < N; ++k)
    indexed_n[k] = (k + 1) * n[k];

  std::vector<double> cropped_n(n, n + N);
  double *cropped_n_p = cropped_n.data();
  cropped_n.front() = 0.0;

  double *indexed_n_prod = kernel_cross.matvec(indexed_n_p);
  double *n_prod = kernel_cross.matvec(cropped_n_p);

  double a = 0.0;

  // this should probably be unified with first column below eventually
  for (int k = 1; k < N; ++k) {
    double v = (lambda * (k + 1) - 1) * kernel(0, k);
    first_row[k - 1] = v * n[0];
    a += v * n[k];
    n_prod[k] *= k + 1;
  }

  axpy(N - 1, lambda, indexed_n_prod + 1, 1, first_row.data(), 1);
  axpy(N - 1, lambda, n_prod + 1, 1, first_row.data(), 1);
  std::free(indexed_n_prod);
  std::free(n_prod);

  a -= 2 * kernel(0, 0) * n[0];

  out[0] = a * in[0] + dot_u(N - 1, first_row.data(), 1, in + 1, 1);

  // first column
  for (int k = 1; k < N; ++k) {
    out[k] = in[0] * (n[k - 1] * kernel(0, k - 1));
  }

  // the (N-1)x(N-1) block
  double *low_rank_part = kernel_cross.matvec(in);
  for (int k = 1; k < N; ++k)
    low_rank_part[k] *= n[k];

  axpy(N - 1, -(1 + lambda), low_rank_part + 1, 1, out + 1, 1);
  std::free(low_rank_part);

  int R = kernel_cross.get_rank();
  std::vector<VSLConvTaskPtr> tasks(R);
  int status =
      vsldConvNewTask1D(&tasks[0], VSL_CONV_MODE_AUTO, N - 1, N - 1, N - 1);
  assert(status == VSL_STATUS_OK);

  for (int i = 1; i < R; ++i) {
    vslConvCopyTask(&tasks.at(i), tasks.front());
  }

  std::vector<double> convolution_results(R * (N - 1)),
      convolution_vectors_y(R * (N - 1)), convolution_vectors_x(R * (N - 1));
#pragma omp parallel for
  for (int i = 0; i < R; ++i) {
    convolution_vectors_y[i * (N - 1)] =
        -(1 + lambda) * dot_u(N, kernel_cross.export_V() + i * N, 1, n, 1);
    for (int j = 1; j < N - 1; ++j) {
      convolution_vectors_y[i * (N - 1) + j] =
          n[j - 1] * kernel_cross.export_V()[i * N + j - 1];
    }

    for (int j = 0; j < N - 1; ++j)
      convolution_vectors_x[i * (N - 1) + j] =
          in[j + 1] * kernel_cross.export_U()[i * N + j + 1];

    vsldConvExec1D(tasks.at(i), convolution_vectors_x.data() + i * (N - 1), 1,
                   convolution_vectors_y.data() + i * (N - 1), 1,
                   convolution_results.data() + i * (N - 1), 1);
  }

  for (int i = 0; i < R; ++i) {
    vslConvDeleteTask(&tasks.at(i));
  }

  for (int i = 0; i < R; ++i) {
    for (int j = 0; j < N - 1; ++j) {
      out[j + 1] += convolution_results[i * (N - 1) + j];
    }
  }
}

template <typename K>
int solve_jacobi_approx(MKL_INT N, K kernel, TCross_Parallel_v1 &kernel_cross,
                        double lambda, double *n, double *rhs, double *x, int p,
                        int iters, int norestart_iters) {
  MKL_INT ipar[128] = {};
  double dpar[128] = {};
  MKL_INT RCI_request = 0;
  std::vector<double> tmp((2 * norestart_iters + 1) * N +
                          norestart_iters * ((norestart_iters + 9) / 2 + 1));
  double *tmp_p = tmp.data();

  Jacobi::Preconditioner prec{
      N, kernel_cross.get_rank(), kernel, kernel_cross, lambda, n, p};

  dfgmres_init(&N, x, rhs, &RCI_request, ipar, dpar, tmp_p);
  assert(RCI_request == 0);

  // Note: all indices shifted by 1 compared to docs; that is because C uses
  // 0-based indexing, while the docs are for Fortran's 1-based
  // indexing.
  dpar[2] = nrm2(N, rhs, 1);             // initial residue
  dpar[3] = dpar[0] * dpar[2] + dpar[1]; // residue target
  ipar[8] = 1;              // check residue for termination within FGMRES
  ipar[9] = 0;              // no user-supplied stopping test
  ipar[10] = p > 0 ? 1 : 0; // use preconditioning
  ipar[11] = 1;             // check new vector for being zero within FGMRES
  ipar[4] = iters;
  ipar[14] = std::min(norestart_iters, iters);
  dfgmres_check(&N, x, rhs, &RCI_request, ipar, dpar, tmp_p);
  assert(RCI_request == 0);

  bool done = false;
  while (!done) {
    dfgmres(&N, x, rhs, &RCI_request, ipar, dpar, tmp_p);
    switch (RCI_request) {
    case -1: // Max iterations reached
    case 0:
      done = true;
      break;
    case 1:
      // multiply by the matrix
      apply_jacobi(N, kernel, kernel_cross, lambda, n, tmp_p + ipar[21] - 1,
                   tmp_p + ipar[22] - 1);
      break;
    case 3:
      // apply the preconditioner
      copy(N, tmp_p + ipar[21] - 1, 1, tmp_p + ipar[22] - 1, 1);
      if (p > 0)
        prec.apply(tmp_p + ipar[22] - 1);
      break;
    default:
      std::cerr << RCI_request << std::endl;
      assert(false);
    }
  }

  MKL_INT itercount = 0;
  dfgmres_get(&N, x, rhs, &RCI_request, ipar, dpar, tmp_p, &itercount);
  assert(RCI_request == 0);
  return itercount;
}

template <typename K>
void smoluchowski(int N, K kernel, TCross_Parallel_v1 &kernel_cross,
                  double lambda, double J, double *in, double *out) {
  double *L1_res_vec, *L2_res_vec;
  double *workspace_for_mono, *L3_for_mono_vec;
  double L3_for_mono, L4_for_mono;

  workspace_for_mono = (double *)malloc(N * sizeof(double));
  workspace_for_mono[0] = 0.0;
  // fast computations of L1, L2, L3 and L4
  L1_res_vec = kernel_cross.smol_conv_discrete(in);
  L2_res_vec = kernel_cross.matvec(in);
  L3_for_mono = 0.0;
  L4_for_mono = 0.0;
  for (int i = 1; i < N; i++) {
    workspace_for_mono[i] = (i + 1) * in[i];
    L4_for_mono += kernel(0, i) * in[i] * (i + 1);
  }

  L3_for_mono_vec = kernel_cross.matvec(workspace_for_mono);
  for (int i = 1; i < N; i++)
    L3_for_mono += in[i] * L3_for_mono_vec[i];

  L4_for_mono *= lambda * in[0];

  for (int i = 0; i < N; i++) {
    // fast
    if (i > 0)
      out[i] = (L1_res_vec[i] * 0.5 - (1.0 + lambda) * in[i] * L2_res_vec[i]);
    else
      out[i] =
          (-in[i] * L2_res_vec[i] + L3_for_mono * lambda + L4_for_mono + J);
    // There's no good reason to do cutoff in the operator itself.
    // if (out[i] < 0.0)
    //     out[i] = 0.0;
  }

  free(L1_res_vec);
  L1_res_vec = NULL;
  free(L2_res_vec);
  L2_res_vec = NULL;
  free(L3_for_mono_vec);
  L3_for_mono_vec = NULL;
  free(workspace_for_mono);
  workspace_for_mono = NULL;
}

template <typename K>
int newton_smoluchowski(int N, K kernel, TCross_Parallel_v1 &kernel_cross,
                        double lambda, double J, int p, int max_newton_iter,
                        double *n, std::ostream &residues,
                        std::ostream &iter_counts, double newton_eps = 1e-10,
                        int fgmres_iters = 80,
                        int fgmres_norestart_iters = 80) {
  std::vector<double> op_res(N), shift(N);

  int iter = 1;
  for (; iter <= max_newton_iter; ++iter) {
    smoluchowski(N, kernel, kernel_cross, lambda, J, n, op_res.data());
    int itercount = solve_jacobi_approx(N, kernel, kernel_cross, lambda, n,
                                        op_res.data(), shift.data(), p,
                                        fgmres_iters, fgmres_norestart_iters);
    iter_counts << itercount << std::endl;
    axpy(N, -1.0, shift.data(), 1, n, 1);
    for (int i = 0; i < N; ++i)
      if (n[i] < 0)
        n[i] = 0;

    double op_nrm = nrm2(N, op_res.data(), 1);
    residues << op_nrm << std::endl;
    if (op_nrm < newton_eps)
      break;
  }

  return iter;
}

#endif /* JACOBI_H */
