      subroutine read_all_namelists
c-----------------------------------------------------------
c     reads all namelists from coagulation.dat input file
c-----------------------------------------------------------
c     declaration and storage of all namelists variables
c-----------------------------------
      
      use One_NmlMod
      use One_NoNmlMod 
      
c---------------------------------------------------------------
c     list of all namelists
c--------------------------------------------------------------
      use NamelistMod     
c--------------------------------------------------------------
      implicit none

c-----local
      integer
     &kode,icheck,i_unit,j
   
      i_unit=1 

      open(i_unit,file = 'coagulation.dat',status='old',iostat=kode)
 
      write(*,*)'in read_all_namelists after
     & open file=coagulation.dat kode',kode
       
      if (kode.ne.0) then
         write(*,*)' read_all_namelists:'
         write(*,*)' Neither coagulation.dat present'
         stop
      endif

      rewind(unit=i_unit)
      read(i_unit,physical_nml,iostat=kode)
      write(*,*)'in read_all_namelists after read ,physica_nml kode=',
     &kode

      call check_read(kode,'physical_nml')
      write(*,*)'print data from /physical_nml/ obtained from input'
      write(*,physical_nml)

      rewind(unit=i_unit)
      read(i_unit,numericle_nml,iostat=kode)
      write(*,*)'in read_all_namelist_nmls after read numericle kode=',
     &kode

      call check_read(kode,'numericle_nml')
      write(*,*)'print data from /numericle_nml/ obtained from input'
      write(*,numericle_nml)

c------------------------------------------------------------
c     allocate arrys in One_NoNmlMod
c------------------------------------------------------------
      write(*,*)'in read_input before ainalloc_arrays_One_NoNmlMod'
      call ainalloc_arrays_One_NoNmlMod
      write(*,*)'in read_input after ainalloc_arrays_One_NoNmlMod'
c----------------------------------------------------------------
      read(i_unit,species_v0_vector_nml,iostat=kode)
   
      write(*,*)'N_species ', N_species
      do j=1,N_species
         write(*,*)'j',j
         write(*,*)'prof(j)',prof(j)
         v0_vector=prof(j)
         write(*,*)'v0_vector(j)',v0_vector(j)         
      enddo

      read(i_unit,species_v_particle_min_vector_nml,iostat=kode)
      call check_read(kode,'species_v_particle_min_vector_nml')
      do j=1,N_species
         v_particle_min_vector(j)=prof(j)

         write(*,*)'j,v_particle_min_vector(j)',
     &              j,v_particle_min_vector(j)
      enddo


      read(i_unit,species_v_particle_max_vector_nml,iostat=kode)
      call check_read(kode,'species_v_particle_max_vector_nml')
      do j=1,N_species
         v_particle_max_vector(j)=prof(j)

         write(*,*)'j,v_particle_max_vector(j)',
     &              j,v_particle_max_vector(j)
      enddo
      
c      stop 'in read_input'     

      return
      end



c     ****************CHECK_READ********************************
      subroutine check_read(iostat,name_of_namelist)
c     check the sign of iostat from operator read
c     iostat < 0   the end of input file was detected
c     iostat=0     reading has complited succefully
c     iostat>0     an error has occurred
c
c     input
      integer iostat
      character*(*) name_of_namelist

c      write(*,*)'in check_read name_of_namelist = '
c     &,name_of_namelist,'  iostat = ', iostat      

      if (iostat.gt.0) then
         write(*,1)name_of_namelist,name_of_namelist
 1       format('check_read has found the positive value of iostat',/,
     .   'The error has occurred in reading namelist = ',A,/,
     .   'Check input data in ',A,' in the file laser.in')
         stop
      endif

      if (iostat.lt.0) then
         write(*,*)
         write(*,*)'**************************************************'
         write(*,2) name_of_namelist,name_of_namelist,name_of_namelist
 2       format('check_read has found the negative value of iostat',/,
     .   'The end of input in the given namelist ',A,' was detected.',/,
     .   'It can be that namelist ',A,' is absent in laser.dat file',/,
     .   'Check data in ',A,' in the file laser.dat')
         write(*,*)'**************************************************'
      endif

      return
      end

      subroutine default_in
c--------------------------------------------------------------------------
c     It creates default input data in coagulation.dat file.
c     The results of the work  are in One_NmlMod
c-----------------------------------------------------------------------      
      use One_NmlMod
      use One_noNmlMod
      implicit none
      integer j
c--------------------------------------------------      
c      namelist /physical_nml/
c     &v_x_0,v_y_0,                 !Overage initial volumes
c     &rK_0,                        !Kernel
c     &rN_0                         !Number of particles
c      namelist /numericle_nml/ 
c     &t_max,                       !maximal time
c     &tau,                         !time step
c     &v_x_max,v_y_max,             !Maximum particle volumes
c     &Nx,                          !number of volume mesh points
c     &Ny,                          !number of volume mesh points
c     &mnemonic                     !it is used for naming the data output
c                                   !files: mnemonic.txt
c/physical_nml/
c---------------------------------------------------
!      v_x_0,v_y_0,                 !Overage initial volumes
!
!      rK_0,                        !Kernel
!
!      rN_0                         !Number of particles    
!
!     N_species,                    !number of species
!     i_coag_type                  !type of kernel 1 const, 2 brounian
!--------------------------------------------------
c&physical_nml    
      v_x_0=1.d0
      v_y_0=1.d0  
      rK_0=1.d0
      rN_0=1.d0 
      N_species=2
      i_coag_type=1
c&end 
      write(*,*)'in default_in after set data for /physical_nml/'    
!-------------------------------------------------------------------------
!/numerical_nml/ namelist   	 
!-------------------------------------------------------------
! t_max,                       !maximal time
!--------------------0-----------------------------------------
! tau,                         !time step
!-------------------------------------------------------------
! v_x_max,v_y_max,             !Maximum particle volumes
!-------------------------------------------------------------
! h_plot,                      !time step for plots-\
!-----------------------------------------------------------
! Nx                           !number of volume mesh points
! Ny                           !number of volume mesh points
!---------------------------------------------------------------
!n_vol_plot                    !number of mesh points 
!                              !for sectional distributions (0:n_vol_plot)
!------------------------------------------------------------------
! mnemonic                     !it is used for naming the data output files:
!                              ! mnemonic.txt
!-------------------------------------------------------------
! N_particle                   !number of MC particles
!------------------------------------------------------------
c&numericle_nml  
      t_max=1.d0 
      tau=0.1d0 
      v_x_max=10.d0
      v_y_max=10.d0
      h_plot=0.1 d0
      Nx=100                         
      Ny=100
      n_vol_plot=15
      mnemonic='coagulation'   
      N_particle=10000   
c&end
      write(*,*)'in default_in after set data for /numericle_nml/'
      write(*,*)'Nx',Nx,'Ny',Ny

c------------------------------------------------------------------
c     namelist /species_v0_vector_nml
c-----------------------------------------------------------------
c     prof(j)=v0_vector_m3(j)  
c&species_v0_vector_nml
      write(*,*) 'in default_in N_species',N_species
      do j=1,N_species
         prof(j)=1.d0                       
      enddo     
      write(*,*)'v0 prof',prof
c&end 

c------------------------------------------------------------------
c     namelist /species_v_particle_min_vector_nml/
c-----------------------------------------------------------------
c     prof(j)=v_particle_min_vector_m3(j)
c&   /species_v_particle_min_vector_nml/
      do j=1,N_species
         prof(j)=0.01d0                       
      enddo     
c&end 

c------------------------------------------------------------------
c     namelist /species_v_particle_max_vector_nml/
c-----------------------------------------------------------------
c     prof(j)=v_particle_max_vector_m3(j)
c&   /species_v_particle_max_vector_nml/
      do j=1,N_species
         prof(j)=15.d0                       
      enddo     
c&end 


      return
      end
