#include <mkl/mkl.h>
#include "../tensor.h"
#include "../tensor_train.h"
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
double J = 1.0;

double K(const int & u, const int &v)
{
    double u1 = ((double) u) + 1.;
    double v1 = ((double) v) + 1.;
    return pow(u1/v1, 0.3l) + pow(v1/u1, 0.3l);
    //return 2.0;

	return pow(pow(u1,1.0l/3)+pow(v1, 1.0l/3),2)*sqrt((1.0l/u1 + 1.0l/v1));
}

int *int_to_qtt_pos(int i, int d)
{
// d -- dimensionality for 2^d array
// i -- general linear position in 2^d array from 0 to (2^d -1)
    int *ip;
    ip = (int *) malloc(d * sizeof(int));
//    printf("i = %d \n", i);
    for (int i1 = 0; i1 < d; i1++)
    {
        ip[i1] = ((i >> i1) % 2);
//        printf("%d ", ip[i1]);
    }
//    printf("\n");
    return ip;
}


// Declare target functions for TT-cross 

// Function 1
class ApplyOperator : public TTensor {
    public:
        TTensorTrain *tt_densities;
        double time_step;
        double operator[](int *point)
        {
            double s = 0;
            int ind = 0;
            for (int i = 0; i < this->get_dimensionality(); i++)
                ind =ind + (point[i] << i); 
            
            return (*(this->tt_densities))[point] + (this->time_step) * SmolOper(ind);
        }
        double SmolOper(const int &i); 
        ApplyOperator(): TTensor() {
        }
        ApplyOperator(const int &d , int * &m):TTensor(d,m) {
        }
};

class StartCond : public TTensor {
    public:

        double operator [] (int *point)
        {
            double s = 0;
            int ind = 0;
            for (int i = 0; i < this->get_dimensionality(); i++)
                ind = ind + (point[i] << i);
            
            return pow(ind + 1.0, -2.5l);
        }
        StartCond(): TTensor(){
        }
        StartCond(const int &d, int *&m):TTensor(d, m){
        }
};


double ApplyOperator::SmolOper(const int &i)
{
    double l1 = 0.0;
    int *ip, *ip1;// qtt version of i 
    TTensorTrain *n = this->tt_densities;
    int d = n->get_dimensionality();
    for (int i1 = 0; i1 < i ; i1++)
    {// compute convolution part of aggregation
        ip = int_to_qtt_pos(i-i1 - 1, d);
        ip1 = int_to_qtt_pos(i1, d);
        l1 += (*(n))[ip1] * ((*n))[ip] * K((i - i1) , i1);
        free(ip);
        free(ip1);
    }

    double l2 = 0;
    int N = 1 << d;
    ip = int_to_qtt_pos(i, d);
    for (int i1 = 0; i1 < N; i1++)
    {// compute matvec part of aggregation
        ip1 = int_to_qtt_pos(i1, d);
        l2 += (*(n))[ip1] * K(i, i1);
        free(ip1);
    }
    l2 *=((*n))[ip];
    free(ip);

    if (i > 0)
        return (l1/2. - l2) ;
    else if (i == 0)
        return (l1/2. - l2 + J);
    else if (i < 0)
    {
        printf("some crap occasion\n");
        exit(1);
    }
}

int main(int argc, char **argv)
{
    int i, d; 
    int N_steps = 1;
    if (argc < 4)
    {
        printf("params: d N_steps, time_step\n");
        exit(1);
    }
    double time_step; 
    sscanf(argv[1], "%d", &d);
    sscanf(argv[2], "%d", &N_steps);
    sscanf(argv[3], "%lf", &time_step);
    int N = 1 << d; // 2**d
    struct timeval start, end;
    printf("N = %d\n", N);
    int *mode_sizes = (int *) malloc(d * sizeof(int));
    for (i = 0; i < d; i++)
        mode_sizes[i] = 2;
	StartCond start_cond(d, mode_sizes);           // declare first tensor for approximation

    mode_sizes = (int *) malloc(d * sizeof(int));
    for (i = 0; i < d; i++)
        mode_sizes[i] = 2;
    ApplyOperator smol_operator(d, mode_sizes);
	TTensorTrainParameters parameters;               
	parameters.tolerance = 1e-6;                     
	parameters.maximal_iterations_number = 0;        
    /*
	TTensorTrain tt1, tt2;                       
	tt1.Approximate(&start_cond, parameters);    

	printf("Approximated tt1\n");            //                                                 
	printf("tt1\n");                                 //
	for (i = 0; i < d; i++)                      //
            printf("rank tt1 = %d\n", tt1.get_rank(i));  // print its tt-ranks 
	printf("\n\n\ntt2\n\n");                         //

    smol_operator.tt_densities = &tt1;
    smol_operator.time_step = 0.01;
    tt2.Approximate(&smol_operator, parameters); 
	printf("Approximated tt2\n");            //                                                 
	printf("tt1\n");                                 //
	for (i = 0; i < d; i++)                      //
            printf("rank tt1 = %d\n", tt1.get_rank(i));  // print its tt-ranks 
	printf("\n\n\ntt2\n\n");                         //

	for (i = 0; i < d; i++)                      //
            printf("rank tt2 = %d\n", tt2.get_rank(i));  //
    
    int *pp;
    for (i = 0; i < N; i++)
    {
        pp = int_to_qtt_pos(i, d); 
        printf("tt2[%d] = %lf sin(%d*1e-3) = %lf\n ", i, tt2[pp], i, sin(i*0.001));
        free(pp);
    }
    */
    TTensorTrain tt_start, cur, next;

	parameters.tolerance = 1e-6;  
    parameters.stop_rank = 2;
	parameters.maximal_iterations_number = 0;

    tt_start.Approximate(&start_cond, parameters);

    
    for (i = 0; i < d; i++)                      
        printf("rank start = %d\n", tt_start.get_rank(i));  

    smol_operator.tt_densities = &(tt_start);
    smol_operator.time_step = time_step;

    int maxrank =0 ; 

    printf("Entering main loop\n");
    gettimeofday(&start, NULL);
    for (int t = 0; t < N_steps; t++)
    {    
        next.Approximate(&smol_operator, parameters);
        cur = next;
        /*
        for (i = 0; i < d; i++)                      
            printf("rank next = %d\n", next.get_rank(i));  

        int *pp;
        for (i = 0; i < N; i++)
        {
            pp = int_to_qtt_pos(i, d); 
            printf("next[%d] = %lf\n ", i, next[pp]);
            free(pp);
        }
        */
        maxrank = 0;
        for (i = 0; i < d; i++)
        {
             if (maxrank < cur.get_rank(i))
                 maxrank = cur.get_rank(i);
        }

        printf("time step %d, maxrank = %d\n", t, maxrank);
        smol_operator.tt_densities = &(cur);
        /*printf("-------------------------------------\n");
        printf("-------------------------------------\n");*/
    }
    gettimeofday(&end, NULL);

	double r_time = end.tv_sec - start.tv_sec + ((double)(end.tv_usec - start.tv_usec)) / 1000000;

    for (i = 0; i < d; i++)                      
        printf("rank final = %d\n", cur.get_rank(i));  
    printf("runtime = %lf sec\n", r_time);
    int *pp;
    FILE *data_final = fopen("TT_final.log", "w");
    for (i = 0; i < N; i++)
    {
        pp = int_to_qtt_pos(i, d); 
        fprintf(data_final, "%d %lf\n ", i+1, cur[pp]);
        free(pp);
    }
    fclose(data_final);
    return 0;                                        //
}
