#include <stdio.h>
#include <math.h>
#include <mkl.h>

double analytical_solution(const double v, const double t)
{

	return v / ( (1.0 + t / 2.0) * (1.0 + t / 2.0) ) * exp(-v / (1.0 + t /2.0));
}

int main (int argc, char ** argv)
{
	FILE * coag_file;
	coag_file = fopen ("1.000E+01_time.dat", "r+");
	double t ;
	int i, n;
	fscanf (coag_file, "%d", &n);
	fscanf (coag_file, "%d", &n);
	printf("n = %d\n", n);
	
//	scanf("%d", &trash);
//	printf("%d", trash);
	fscanf (coag_file, "%lf", &t);

	double *analytical_values = (double *) malloc(n * sizeof(double));
	double *numerical_values = (double *) malloc(n * sizeof(double));
	double *numerical_values_concentration = (double *) malloc(n * sizeof(double));
	double *nodes = (double *) malloc(n * sizeof(double));

	//scan values, nodes and compute analytical solution
	for (i = 0; i < n; i++)
	{
		fscanf(coag_file, "%lf ", &nodes[i]);
		//printf("%lf", node);
		analytical_values[i] = analytical_solution(nodes[i], t);
		fscanf(coag_file, "%lf\n", &numerical_values[i]);
		numerical_values_concentration[i] = numerical_values[i] / nodes[i];
//		printf("%lf %lf %lf\n", nodes[i], analytical_values[i], numerical_values[i]);
	}

	double relative_error = 0;//in  L_2 norm
	double absolute_error = 0;//in  L_2 norm
	double solution_norm = 0; // L_2 norm
	double concentration_solution_norm = 0;
	double absolute_concentration_error = 0; // in L_2 norm
	double relative_concentration_error = 0; // in L_2 norm
	
	//trapezoids quadrature
	for (i = 1; i<n; i++)
	{
		if ((i == 1) || (i == (n - 1)))
		{
			solution_norm += 0.5 * analytical_values[i]  * analytical_values[i]  * (nodes[i] - nodes[i-1]);
			concentration_solution_norm += 0.5 * analytical_values[i]  * analytical_values[i]  * (nodes[i] - nodes[i-1]) / ( nodes[i] * nodes[i] );
			absolute_error += 0.5 * (analytical_values[i] - numerical_values[i]) * (analytical_values[i] - numerical_values[i]) * (nodes[i] - nodes[i-1]);
			absolute_concentration_error += 0.5 * (analytical_values[i] / nodes[i] - numerical_values_concentration[i]) *  (analytical_values[i] / nodes[i] - numerical_values_concentration[i]) * ( nodes[i] - nodes[i-1]);
		}
		else
		{
			solution_norm += analytical_values[i]  * analytical_values[i]  * (nodes[i] - nodes[i-1]);
			concentration_solution_norm +=  analytical_values[i]  * analytical_values[i]  * (nodes[i] - nodes[i-1]) / ( nodes[i] * nodes[i] );
			absolute_error += (analytical_values[i] - numerical_values[i]) * (analytical_values[i] - numerical_values[i]) * (nodes[i] - nodes[i-1]);
			absolute_concentration_error += (analytical_values[i] / nodes[i] - numerical_values_concentration[i]) *  (analytical_values[i] / nodes[i] - numerical_values_concentration[i]) * ( nodes[i] - nodes[i-1]);
		}
	}

	relative_error = sqrt (absolute_error / solution_norm);
	relative_concentration_error = sqrt (absolute_concentration_error / concentration_solution_norm);
	absolute_error = sqrt (absolute_error);
/*
	cblas_daxpy(n, -1.0, analytical_values, 1, numerical_values, 1);
	printf("abs_error = %f \n",  cblas_dnrm2(n, numerical_values, 1) );

	double relative_error = cblas_dnrm2(n, numerical_values, 1) / cblas_dnrm2(n, analytical_values, 1);
*/
	printf("relative error in L_2 ( v * n (v) )= %lf \n", relative_error);
	printf("absolute error in L_2 (v * n (v) )= %lf \n", absolute_error);

	printf("relative error in L_2 ( n(v) ) = %lf \n", relative_concentration_error);
	printf("absolute concentration error in L_2 ( n (v) )= %lf \n", absolute_concentration_error);

	printf(" t = %lf\n", t);
	fclose(coag_file);

	return 0;

}
